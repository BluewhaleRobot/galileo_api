#!/bin/bash
# @Author: randoms
# @Date:   2017-08-04
# @Email:  randoms@randoms.me
# @Project: galileo
# @Last modified by:   randoms
# @Last modified time: 2017-08-04
# @Copyright: © 2015-2017 Blue Whale Technology all rights reserved



DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR
cd ..
source ./venv/bin/activate
./venv/bin/python src/fake_server.py $@
