#!/usr/bin/python3
# @Author: randoms
# @Date:   2017-08-04
# @Email:  randoms@randoms.me
# @Project: galileo
# @Last modified by:   randoms
# @Last modified time: 2017-08-07
# @Copyright: © 2015-2017 Blue Whale Technology all rights reserved

import time
import random
import rospy
import actionlib

from galileo_msg.msg import\
SaveMapAction, NavigationAction, NavigationActionFeedback


class SaveMapServer:
    def __init__(self):
        self.server = actionlib.SimpleActionServer(
        '/galileo/map/save', SaveMapAction, self.execute, False)
        self.server.start()

    def execute(self, goal):
        # Do lots of awesome groundbreaking robot stuff here
        time.sleep(2)
        self.server.set_succeeded()

class MoveActionServer:

    def __init__(self):
        self.server = actionlib.SimpleActionServer(
            '/galileo/navigation/target_point',
            NavigationAction, self.execute, False
        )
        self.server.start()

    def execute(self, goal):
        timecount = 0
        while timecount < 2:
            if self.server.is_preempt_requested():
                self.server.set_aborted()
                return
            time.sleep(0.1)
            timecount += 0.1
            feedback = NavigationActionFeedback().feedback
            feedback.distance = random.uniform(0, 1)
            self.server.publish_feedback(feedback)
        self.server.set_succeeded()



if __name__ == '__main__':
    rospy.init_node('fake_server')
    SaveMapServer()
    MoveActionServer()
    rospy.spin()
