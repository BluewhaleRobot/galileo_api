#!/bin/bash

# @Author: randoms
# @Date:   2017-08-01
# @Email:  randoms@randoms.me
# @Project: galileo
# @Last modified by:   randoms
# @Last modified time: 2017-08-31
# @Copyright: © 2015-2017 Blue Whale Technology all rights reserved

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR
source ./venv/bin/activate
# django test has dependencies on current working directory
cd galileo
../venv/bin/python ./manage.py $@
