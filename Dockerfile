FROM xiaoqiang_fake:1.0
RUN apt-fast update && apt-fast upgrade -y

RUN su xiaoqiang
RUN export LC_ALL=en_US.UTF-8
# RUN bwupdate fake
# # 生成随机ID
# RUN SHARPLINK_LOG=$(openssl rand -hex 38)
# RUN SHARPLINK_LOG=${SHARPLINK_LOG^^}
# RUN mkdir -p /home/xiaoqiang/Documents/ros/devel/lib/sharplink
# RUN rm /home/xiaoqiang/Documents/ros/devel/lib/sharplink/server.log
# RUN echo "Time: 1571620952371, ID: ${SHARPLINK_LOG}" > /home/xiaoqiang/Documents/ros/devel/lib/sharplink/server.log
# CMD ["roslaunch", "startup", "fake.launch"]