#!/usr/bin/env python
#encoding=utf-8


"""apiv1 tests"""
import shutil
import json
import os
import time
import socket
import requests
from apiv1.models import TOKEN_DB, RECHARGE_DB
from django.test import TestCase, Client
from galileo.settings import BASE_DIR, BROADCAST_PORT, SERVICE_PORT
from utils.utils import get_my_id


class Apiv1Test(TestCase):
    """Apiv1 test class"""

    def setUp(self):
        TOKEN_DB.drop()
        RECHARGE_DB.drop()
        shutil.rmtree(os.path.join(BASE_DIR, "private"), ignore_errors=True)

    def test_get_token(self):
        self.setUp()
        cli = Client()
        # test cert not found
        res = cli.get("/api/v1/token", {
            "username": "testnocert",
            "password": "1234567",
            "device_id": "E0C3FB9E4C63FE5E9E00A30AA5EE77567BF06F592C858E60CF2"
            + "C341DBEA1B778512CEB95B8CF",
        })
        self.assertEqual(res.status_code, 404)
        # tesk id not match
        res = cli.get("/api/v1/token", {
            "username": "testsuccess",
            "password": "1234567",
            "device_id": "E0C3FB9E4C63FE5E9E00A30AA5EE77567BF06F592C85"
            + "8E60CF2C341DBEA1B778512CEB95B8CF",
        })
        self.assertEqual(res.status_code, 404)
        res = cli.get("/api/v1/token", {
            "username": "testsuccess",
            "password": "1234567",
            "device_id": "B3BBA749EFD5BEF6E176D07AD844655AC371F5E3C8082"
            + "2E46914D274156FE8028F8E1C49C077",
        })
        self.assertEqual(res.status_code, 200)

    def test_token_required(self):
        self.setUp()
        cli = Client()
        res = cli.get("/api/v1/token", {
            "username": "testsuccess",
            "password": "1234567",
            "device_id": "B3BBA749EFD5BEF6E176D07AD844655AC371F5E3C" +
            "80822E46914D274156FE8028F8E1C49C077",
        })
        self.assertEqual(res.status_code, 200)
        token = json.loads(res.content.decode("utf-8"))["token"]
        res = cli.get("/api/v1/system/status")
        self.assertEqual(res.status_code, 403)
        res = cli.get("/api/v1/system/status", {"token": token})
        self.assertEqual(res.status_code, 200)

    def test_get_info(self):
        self.setUp()
        cli = Client()
        res = cli.get("/api/v1/token", {
            "username": "testsuccess",
            "password": "1234567",
            "device_id": "B3BBA749EFD5BEF6E176D07AD844655AC371F5E3C80"
            + "822E46914D274156FE8028F8E1C49C077",
        })
        self.assertEqual(res.status_code, 200)
        token = json.loads(res.content.decode("utf-8"))["token"]
        res = cli.get("/api/v1/system/info", {
            "token": token,
        })
        res = json.loads(res.content.decode("utf-8"))
        time.sleep(2)
        res = cli.get("/api/v1/system/info", {
            "token": token,
        })
        res = json.loads(res.content.decode("utf-8"))
        self.assertEqual(True, "battery" in res)
        self.assertEqual(True, "camera_rgb" in res)
        self.assertEqual(True, "odom" in res)
        self.assertEqual(True, "camera_depth" in res)
        self.assertEqual(True, "imu" in res)
        self.assertNotEqual(0, res["imu"])

    def test_get_status(self):
        self.setUp()
        cli = Client()
        res = cli.get("/api/v1/token", {
            "username": "testsuccess",
            "password": "1234567",
            "device_id": "B3BBA749EFD5BEF6E176D07AD844655AC371F5E3C808"
            + "22E46914D274156FE8028F8E1C49C077",
        })
        token = json.loads(res.content.decode("utf-8"))["token"]
        self.assertEqual(res.status_code, 200)
        # test get without token
        res = cli.get("/api/v1/system/status")
        self.assertEqual(res.status_code, 403)
        res = cli.get("/api/v1/system/status", {
            "token": token,
        })
        self.assertEqual(res.status_code, 200)
        res = json.loads(res.content.decode("utf-8"))
        self.assertEqual(res["status"], "Free")

    def test_start_stop_map(self):
        self.setUp()
        cli = Client()
        res = cli.get("/api/v1/token", {
            "username": "testsuccess",
            "password": "1234567",
            "device_id": "B3BBA749EFD5BEF6E176D07AD844655AC371F5E3C80822"
            + "E46914D274156FE8028F8E1C49C077",
        })
        self.assertEqual(res.status_code, 200)
        token = json.loads(res.content.decode("utf-8"))["token"]
        res = cli.get("/api/v1/system/status", {
            "token": token,
        })
        res = json.loads(res.content.decode("utf-8"))
        self.assertEqual(res["status"], "Free")
        # start map
        res = cli.get("/api/v1/map/start", {
            "token": token,
        })
        self.assertEqual(res.status_code, 200)
        self.assertEqual(json.loads(
            res.content.decode("utf-8"))["result"], True)
        # check status
        res = cli.get("/api/v1/system/status", {
            "token": token,
        })
        self.assertEqual(json.loads(res.content.decode("utf-8"))["status"],
                         "Mapping")
        # test start again
        res = cli.get("/api/v1/map/start", {
            "token": token,
        })
        self.assertEqual(res.status_code, 400)
        # stop mapping
        res = cli.get("/api/v1/map/stop", {"token": token})
        self.assertEqual(res.status_code, 200)
        self.assertEqual(json.loads(
            res.content.decode("utf-8"))["result"], True)
        # stop again
        res = cli.get("/api/v1/map/stop", {"token": token})
        self.assertEqual(res.status_code, 400)
        self.assertEqual(json.loads(
            res.content.decode("utf-8"))["result"], False)

    def test_broadcast(self):
        self.setUp()
        address = ('0.0.0.0', BROADCAST_PORT)
        server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server.bind(address)
        data, dummy = server.recvfrom(1024)
        server.close()
        res = json.loads(data.decode("utf-8"))
        self.assertEqual(res["id"], get_my_id())
        self.assertEqual(res["port"], SERVICE_PORT)

    def test_save_current_map(self):
        # start map server
        cli = Client()
        res = cli.get("/api/v1/token", {
            "username": "testsuccess",
            "password": "1234567",
            "device_id": "B3BBA749EFD5BEF6E176D07AD844655AC371F5E3C80822"
            + "E46914D274156FE8028F8E1C49C077",
        })
        self.assertEqual(res.status_code, 200)
        token = json.loads(res.content.decode("utf-8"))["token"]
        time.sleep(2)
        # start map
        res = cli.get("/api/v1/map/start", {
            "token": token,
            "package": "ORB_SLAM2",
            "launch": "start.launch",
        })
        self.assertEqual(res.status_code, 200)
        # save map
        map_name = "map" + str(int(time.time()))
        res = cli.post("/api/v1/map/current_map_image", json.dumps({
            "token": token,
            "name": map_name
        }, indent=4), content_type="application/json")
        self.assertEqual(res.status_code, 200)
        # save map again
        res = cli.post("/api/v1/map/current_map_image", json.dumps({
            "token": token,
            "name": map_name
        }, indent=4), content_type="application/json")
        self.assertEqual(res.status_code, 400)
        res = cli.get("/api/v1/map/stop", {
            "token": token,
        })
        self.assertEqual(res.status_code, 200)

    def test_navigation(self):
        self.setUp()
        cli = Client()
        res = cli.get("/api/v1/token", {
            "username": "testsuccess",
            "password": "1234567",
            "device_id": "B3BBA749EFD5BEF6E176D07AD844655AC371F5E3C80822"
            + "E46914D274156FE8028F8E1C49C077",
        })
        self.assertEqual(res.status_code, 200)
        token = json.loads(res.content.decode("utf-8"))["token"]
        time.sleep(2)
        # test start navigation
        # start with wrong params
        res = cli.get("/api/v1/navigation/start", {
            "token": token,
            "map": "newmap5",
        })
        self.assertEqual(res.status_code, 400)
        # start without token
        res = cli.get("/api/v1/navigation/start", {
            "map": "newmap5",
            "path": "path1",
        })
        self.assertEqual(res.status_code, 403)
        # start with none exist path
        path_name = "@test_path" + str(int(time.time()))
        map_name = "@test_map" + str(int(time.time()))
        res = cli.get("/api/v1/navigation/start", {
            "token": token,
            "path": path_name,
            "map": map_name,
        })
        self.assertEqual(res.status_code, 404)
        
        map_name = "newmap5"
        path_name = "path1"

        # start success
        res = cli.get("/api/v1/navigation/start", {
            "token": token,
            "map": map_name,
            "path": path_name,
        })
        self.assertEqual(res.status_code, 200)
        # start again
        res = cli.get("/api/v1/navigation/start", {
            "token": token,
            "map": map_name,
            "path": path_name,
        })
        self.assertEqual(res.status_code, 400)
        # stop navigation
        res = cli.get("/api/v1/navigation/stop", {
            "token": token,
        })
        self.assertEqual(res.status_code, 200)
        # stop again
        res = cli.get("/api/v1/navigation/stop", {
            "token": token,
        })
        self.assertEqual(res.status_code, 400)

        # start navigation
        res = cli.get("/api/v1/navigation/start", {
            "token": token,
            "map": map_name,
            "path": path_name,
        })
        self.assertEqual(res.status_code, 200)
        # get pose
        res = cli.get("/api/v1/navigation/pose", {
            "token": token
        })
        self.assertEqual(res.status_code, 200)
        time.sleep(1)
        res = cli.get("/api/v1/navigation/pose", {
            "token": token
        })
        pose = json.loads(res.content.decode("utf-8"))
        self.assertEqual(pose["x"], 0)
        # stop navigation
        res = cli.get("/api/v1/navigation/stop", {
            "token": token,
        })


    #     # get target point
    #     res = cli.get("/api/v1/navigation/target_point", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     self.assertEqual(True, "x" in json.loads(res.content.decode("utf-8")))

    #     # set target point
    #     res = cli.post("/api/v1/navigation/target_point", json.dumps({
    #         "x": 1,
    #         "y": 1,
    #         "angle": 1,
    #         "token": token,
    #     }, indent=4), content_type="application/json")
    #     self.assertEqual(res.status_code, 200)

    #     time.sleep(1)
    #     res = cli.get("/api/v1/navigation/target_point", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     target_json = json.loads(res.content.decode("utf-8"))
    #     self.assertEqual(target_json["x"], 1)
    #     self.assertEqual(target_json["status"], "ACTIVE")

    #     time.sleep(2)
    #     res = cli.get("/api/v1/navigation/target_point", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     target_json = json.loads(res.content.decode("utf-8"))
    #     self.assertEqual(target_json["status"], "SUCCEEDED")

    #     # cancel_goal
    #     res = cli.post("/api/v1/navigation/target_point", json.dumps({
    #         "x": 2,
    #         "y": 2,
    #         "angle": 2,
    #         "token": token,
    #     }, indent=4), content_type="application/json")
    #     self.assertEqual(res.status_code, 200)
    #     time.sleep(0.2)
    #     res = cli.delete("/api/v1/navigation/target_point", json.dumps({
    #         "token": token,
    #     }, indent=4), content_type="application/json")
    #     self.assertEqual(res.status_code, 200)
    #     time.sleep(2)
    #     res = cli.get("/api/v1/navigation/target_point", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     target_json = json.loads(res.content.decode("utf-8"))
    #     self.assertEqual(target_json["status"], "ABORTED")

    #     # get current path
    #     res = cli.get("/api/v1/navigation/current_path", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)

    #     # test navigation task loop
    #     res = cli.get("/api/v1/navigation/target_points", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     points_list = json.loads(res.content.decode("utf-8"))
    #     self.assertEqual(len(points_list), 0)

    #     # add target points
    #     res = cli.put("/api/v1/navigation/target_points", json.dumps({
    #         "token": token,
    #         "target_points": [{
    #             "x": 1,
    #             "y": 1,
    #             "angle": 1,
    #         }, {
    #             "x": 2,
    #             "y": 2,
    #             "angle": 2,
    #         }, {
    #             "x": 3,
    #             "y": 3,
    #             "angle": 3,
    #         }]
    #     }, indent=4), content_type="application/json")
    #     self.assertEqual(res.status_code, 200)

    #     # check execute target list
    #     time.sleep(1)
    #     res = cli.get("/api/v1/navigation/target_point", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     target_json = json.loads(res.content.decode("utf-8"))
    #     self.assertEqual(target_json["x"], 1)
    #     time.sleep(2)
    #     res = cli.get("/api/v1/navigation/target_point", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     target_json = json.loads(res.content.decode("utf-8"))
    #     self.assertEqual(target_json["x"], 2)
    #     time.sleep(2)
    #     res = cli.get("/api/v1/navigation/target_point", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     target_json = json.loads(res.content.decode("utf-8"))
    #     self.assertEqual(target_json["x"], 3)
    #     time.sleep(2)
    #     res = cli.get("/api/v1/navigation/target_point", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     target_json = json.loads(res.content.decode("utf-8"))
    #     self.assertEqual(target_json["x"], 1)
    #     # send new goal
    #     res = cli.post("/api/v1/navigation/target_point", json.dumps({
    #         "x": 1.1,
    #         "y": 1.1,
    #         "angle": 1.1,
    #         "token": token,
    #     }, indent=4), content_type="application/json")
    #     self.assertEqual(res.status_code, 200)

    #     time.sleep(1)
    #     res = cli.get("/api/v1/navigation/target_point", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     target_json = json.loads(res.content.decode("utf-8"))
    #     self.assertEqual(target_json["x"], 1.1)
    #     # check execute loop stoped
    #     time.sleep(2)
    #     res = cli.get("/api/v1/navigation/target_point", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     target_json = json.loads(res.content.decode("utf-8"))
    #     self.assertEqual(target_json["x"], 1.1)

    #     # test get distance
    #     res = cli.get("/api/v1/navigation/target_distance", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)

    #     res = cli.put("/api/v1/navigation/target_points", json.dumps({
    #         "token": token,
    #         "target_points": [{
    #             "x": 1,
    #             "y": 1,
    #             "angle": 1,
    #         }, {
    #             "x": 2,
    #             "y": 2,
    #             "angle": 2,
    #         }, {
    #             "x": 3,
    #             "y": 3,
    #             "angle": 3,
    #         }]
    #     }, indent=4), content_type="application/json")
    #     self.assertEqual(res.status_code, 200)
    #     time.sleep(1)
    #     res = cli.get("/api/v1/navigation/target_point", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     target_json = json.loads(res.content.decode("utf-8"))
    #     self.assertEqual(target_json["x"], 1)
    #     time.sleep(2)
    #     res = cli.get("/api/v1/navigation/target_point", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     target_json = json.loads(res.content.decode("utf-8"))
    #     self.assertEqual(target_json["x"], 2)
    #     time.sleep(2)
    #     res = cli.get("/api/v1/navigation/target_point", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     target_json = json.loads(res.content.decode("utf-8"))
    #     self.assertEqual(target_json["x"], 3)
    #     # pause
    #     res = cli.get("/api/v1/navigation/pause", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     time.sleep(2)
    #     res = cli.get("/api/v1/navigation/target_point", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     target_json = json.loads(res.content.decode("utf-8"))
    #     self.assertEqual(target_json["x"], 3)
    #     # resume
    #     res = cli.get("/api/v1/navigation/resume", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)

    #     time.sleep(2)
    #     res = cli.get("/api/v1/navigation/target_point", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     target_json = json.loads(res.content.decode("utf-8"))
    #     self.assertEqual(target_json["x"], 1)

    #     self.delete_map(map_name)
    #     res = cli.get("/api/v1/navigation/stop", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)

    def test_tasks(self):
        self.setUp()
        # create task
        cli = Client()
        res = cli.get("/api/v1/token", {
            "username": "testsuccess",
            "password": "1234567",
            "device_id": "B3BBA749EFD5BEF6E176D07AD844655AC371F5E3C80822"
            + "E46914D274156FE8028F8E1C49C077",
        })
        self.assertEqual(res.status_code, 200)
        token = json.loads(res.content.decode("utf-8"))["token"]
        # create without token
        map_name = "newmap5"
        path_name = "path1"
        res = cli.post("/api/v1/navigation/task", json.dumps({
            "map_name": map_name,
            "path_name": path_name,
        }, indent=4), content_type="application/json")
        self.assertEqual(res.status_code, 403)
        # create without map name
        res = cli.post("/api/v1/navigation/task", json.dumps({
            "token": token,
            "path_name": path_name,
        }, indent=4), content_type="application/json")
        self.assertEqual(res.status_code, 400)
        # create with path not exist
        res = cli.post("/api/v1/navigation/task", json.dumps({
            "token": token,
            "name": "navigation",
            "map_name": map_name,
            "path_name": path_name + "1"
        }, indent=4), content_type="application/json")
        self.assertEqual(res.status_code, 400)
        # create success
        res = cli.post("/api/v1/navigation/task", json.dumps({
            "token": token,
            "map_name": map_name,
            "path_name": path_name,
            "name": "navigation"
        }, indent=4), content_type="application/json")
        self.assertEqual(res.status_code, 200)
        task = json.loads(res.content.decode("utf-8"))
        # create again
        res = cli.post("/api/v1/navigation/task", json.dumps({
            "token": token,
            "map_name": map_name,
            "path_name": path_name,
            "name": "navigation"
        }, indent=4), content_type="application/json")
        self.assertEqual(res.status_code, 400)
        # add action
        res = cli.post("/api/v1/navigation/action", json.dumps({
            "token": token,
            "type": "sleep_action",
            "map_name": map_name,
            "path_name": path_name,
            "task_id": task["id"],
            "wait_time": 10
        }, indent=4), content_type="application/json")
        self.assertEqual(res.status_code, 200)
        action = json.loads(res.content.decode("utf-8"))
        self.assertEqual(action["type"], "sleep_action")
        self.assertEqual(action["wait_time"], 10)
        # get action
        res = cli.get("/api/v1/navigation/action", {
            "token": token,
            "id": action["id"]
        })
        self.assertEqual(res.status_code, 200)
        self.assertEqual(json.loads(res.content.decode("utf-8"))["id"], action["id"])
        # edit action
        res = cli.put("/api/v1/navigation/action", json.dumps({
            "token": token,
            "id": action["id"],
            "wait_time": 20,
        }, indent=4), content_type="application/json")
        self.assertEqual(res.status_code, 200)
        action = json.loads(res.content.decode("utf-8"))
        self.assertEqual(action["wait_time"], 20)
        # delete action
        res = cli.delete("/api/v1/navigation/action?token={token}&id={id}"
            .format(
                token=token,
                id=action["id"]
            ))
        self.assertEqual(res.status_code, 200)
        # get again
        res = cli.get("/api/v1/navigation/action", {
            "token": token,
            "id": action["id"]
        })
        self.assertEqual(res.status_code, 404)
        # add sub task
        res = cli.post("/api/v1/navigation/task", json.dumps({
            "token": token,
            "map_name": map_name,
            "path_name": path_name,
            "name": "navigation1",
        }, indent=4), content_type="application/json")
        self.assertEqual(res.status_code, 200)
        new_task = json.loads(res.content.decode("utf-8"))
        res = cli.put("/api/v1/navigation/task", json.dumps({
            "token": token,
            "map_name": map_name,
            "path_name": path_name,
            "id": new_task["id"],
            "sub_tasks": [task]
        }, indent=4), content_type="application/json")
        self.assertEqual(res.status_code, 200)
        edited_task = json.loads(res.content.decode("utf-8"))
        self.assertEqual(new_task["id"], edited_task["id"])
        self.assertEqual(edited_task["sub_tasks"][0]["id"], task["id"])
        # delete task
        res = cli.delete("/api/v1/navigation/task?token={token}&id={id}&map_name={map_name}&path_name={path_name}".format(
            token=token,
            id=task["id"],
            map_name=map_name,
            path_name=path_name,
        ))
        self.assertEqual(res.status_code, 200)
        res = cli.get("/api/v1/navigation/task?token={token}&task_name={task_name}&map_name={map_name}&path_name={path_name}".format(
            token=token,
            task_name=task["name"],
            map_name=map_name,
            path_name=path_name,
        ))
        self.assertEqual(res.status_code, 404)
        res = cli.get("/api/v1/navigation/task?token={token}&task_name={task_name}&map_name={map_name}&path_name={path_name}".format(
            token=token,
            task_name=edited_task["name"],
            map_name=map_name,
            path_name=path_name,
        ))
        self.assertEqual(res.status_code, 200)
        edited_task = json.loads(res.content.decode("utf-8"))
        self.assertEqual(len(edited_task["sub_tasks"]), 0)
        res = cli.delete("/api/v1/navigation/task?token={token}&id={id}&map_name={map_name}&path_name={path_name}".format(
            token=token,
            id=edited_task["id"],
            map_name=map_name,
            path_name=path_name,
        ))
        self.assertEqual(res.status_code, 200)


    # def test_recharge(self):
    #     self.setUp()
    #     cli = Client()
    #     res = cli.get("/api/v1/token", {
    #         "username": "testsuccess",
    #         "password": "1234567",
    #         "device_id": "B3BBA749EFD5BEF6E176D07AD844655AC371F5E3C80822"
    #         + "E46914D274156FE8028F8E1C49C077",
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     token = json.loads(res.content.decode("utf-8"))["token"]

    #     path_name = "@test_path" + str(int(time.time()))
    #     map_name = "@test_map" + str(int(time.time()))
    #     res = cli.get("/api/v1/recharge/start", {
    #         "token": token,
    #         "package": "ORB_SLAM2",
    #         "launch": "start.launch",
    #         "path": path_name,
    #         "map": map_name,
    #     })
    #     self.assertEqual(res.status_code, 404)
    #     # create map and path
    #     self.create_path(path_name, map_name)

    #     # start without set recharge location
    #     res = cli.get("/api/v1/recharge/start", {
    #         "token": token,
    #         "package": "ORB_SLAM2",
    #         "launch": "start.launch",
    #         "map": map_name,
    #         "path": path_name,
    #     })
    #     self.assertEqual(res.status_code, 400)
    #     # set recharge locations
    #     res = cli.get("/api/v1/recharge/location", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     recharge_locations = json.loads(res.content.decode("utf-8"))
    #     self.assertEqual(len(recharge_locations), 0)
    #     res = cli.put("/api/v1/recharge/location", json.dumps({
    #         "locations": [
    #             {
    #                 "x": 0,
    #                 "y": 0,
    #                 "angle": 0,
    #             },
    #             {
    #                 "x": 1,
    #                 "y": 1,
    #                 "angle": 1,
    #             },
    #             {
    #                 "x": 2,
    #                 "y": 2,
    #                 "angle": 2,
    #             },
    #         ],
    #         "token": token
    #     }, indent=4), content_type="application/json")
    #     self.assertEqual(res.status_code, 200)
    #     res = cli.get("/api/v1/recharge/location", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     recharge_locations = json.loads(res.content.decode("utf-8"))
    #     self.assertEqual(len(recharge_locations), 3)

    #     # start success
    #     res = cli.get("/api/v1/recharge/start", {
    #         "token": token,
    #         "package": "ORB_SLAM2",
    #         "launch": "start.launch",
    #         "map": map_name,
    #         "path": path_name,
    #     })
    #     self.assertEqual(res.status_code, 200)

    #     time.sleep(5)
    #     res = cli.get("/api/v1/recharge/current_target", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)

    #     res = cli.get("/api/v1/recharge/stop", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     self.delete_map(map_name)

    # def test_get_waypoints(self):
    #     self.setUp()
    #     cli = Client()
    #     # 获取token
    #     res = cli.get("/api/v1/token", {
    #         "username": "testsuccess",
    #         "password": "1234567",
    #         "device_id": "B3BBA749EFD5BEF6E176D07AD844655AC371F5E3C80822"
    #         + "E46914D274156FE8028F8E1C49C077",
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     token = json.loads(res.content.decode("utf-8"))["token"]
    #     # 创建测试地图
    #     path_name = "@test_path" + str(int(time.time()))
    #     map_name = "@test_map" + str(int(time.time()))
    #     self.create_path(path_name, map_name)
    #     # 进入导航模式
    #     # start success
    #     res = cli.get("/api/v1/navigation/start", {
    #         "token": token,
    #         "package": "ORB_SLAM2",
    #         "launch": "start.launch",
    #         "map": map_name,
    #         "path": path_name,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     # 获取规划好后的路径，检验路径正确性
    #     # 等待进入tracking状态
    #     res = None
    #     count = 0
    #     while res is None and count < 20:
    #         res = cli.get("/api/v1/navigation/pose", {
    #             "token": token,
    #         })
    #         count += 1
    #         time.sleep(1)

    #     res = cli.get("/api/v1/navigation/current_waypoints", {
    #         "token": token,
    #     })
    #     self.assertEqual(res.status_code, 200)
    #     correct_res = [
    #         {
    #             "y": 13.918901670341002,
    #             "x": -12.025503194655958,
    #             "id": 12,
    #         },
    #         {
    #             "y": 9.01461593420583,
    #             "x": -5.489322117268717,
    #             "id": 15,
    #         },
    #         {
    #             "y": 13.950089298167333,
    #             "x": 0.35806103635197317,
    #             "id": 13,
    #         },
    #         {
    #             "y": 23.06456574418455,
    #             "x": -0.07176501155695192,
    #             "id": 16,
    #         }
    #     ]
    #     res = json.loads(res.content.decode("utf-8"))
    #     for index in range(0, len(correct_res)):
    #         self.assertEqual(
    #             str(correct_res[index]["id"]), str(res[index]["id"]))
    #     # stop Navigating
    #     res = cli.get("/api/v1/navigation/stop", {
    #         "token": token,
    #     })
    #     # delete test map and path
    #     self.delete_map(map_name)
