#!/usr/bin/env python
#encoding=utf-8

import ssl
import os
import json
import hashlib
import requests
import rospy
from apiv1.models import TOKEN_DB
from galileo.settings import BASE_DIR, TEST, USER_CONFIG
from utils.utils import get_my_id, verify
from django.http import HttpResponse
import base.core


def token_required(function):
    """檢查token"""
    def wrap(request, *args, **kwargs):
        """decorator wrap"""
        if USER_CONFIG["no_token_check"]:
            return function(request, *args, **kwargs)
        # 不检查本地的请求
        if request.META.get('REMOTE_ADDR') == "127.0.0.1" \
            and "HTTP_X_REAL_IP"  not in request.META:
            return function(request, *args, **kwargs)
        req_data = {}
        try:
            req_data = json.loads(request.body.decode("utf-8"))
        except Exception:
            pass
        if "token" not in request.GET and "token" not in req_data:
            return HttpResponse(json.dumps({
                "result": False,
                "description": "token is required."
            }, indent=4), content_type="application/json", status=403)
        token = ""
        if "token" in request.GET:
            token = request.GET["token"]
        if "token" in req_data:
            token = req_data["token"]
        if token == "":
            return HttpResponse(json.dumps({
                "result": False,
                "description": "token cannot be blank"
            }, indent=4), content_type="application/json", status=403)
        token_record = TOKEN_DB.find_one({"token": token})
        if token_record is None:
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "invalid token",
            }, indent=4), content_type="application/json", status=400)
        return function(request, *args, **kwargs)
    return wrap


def allowed_status(keys):
    """allowed status decorator"""
    def outer_wrap(function):
        """allowed status decorator out wrap"""
        def wrap(request, *args, **kwargs):
            """allowed status decorator inner wrap"""
            with base.core.STATUS_LOCK:
                if base.core.SYS_STATUS == "Busy":
                    return HttpResponse(json.dumps({
                        "result": False,
                        "description": "Device is busy",
                    }, indent=4), content_type="application/json", status=400)
                if base.core.SYS_STATUS not in keys:
                    return HttpResponse(json.dumps({
                        "result": False,
                        "description": "device must in {states} to use this api"
                                       .format(states=",".join(keys))
                    }, indent=4), content_type="application/json", status=400)
            return function(request, *args, **kwargs)
        return wrap
    return outer_wrap
