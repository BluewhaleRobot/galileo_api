#!/usr/bin/env python
# encoding=utf-8

"""controller for navigation"""
import subprocess
import json
import os
import io
import time
import yaml
import requests
import threading
import math
from PIL import Image
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.dispatch import receiver
from utils.decorator import required_keys, allowed_methods
from utils.utils import stop_process, md5sum
from apiv1.decorator import token_required, allowed_status
from galileo_serial_server.msg import GalileoStatus, GalileoNativeCmds
import base.core
from base.services import stop_udp
from base.ros_connector import get_topic_data, set_topic_data
from base.management.commands import SHUTDOWN_SINGAL
from base.actions.NavAction import NavAction
from base.actions.SleepAction import SleepAction
import rospy
import tf
import requests
import yaml
from galileo.settings import TIMEOUT, TEST, BASE_DIR, MAP_PATH, MAP_WORKSPACE_PATH
from std_msgs.msg import Bool
from nav_msgs.msg import Odometry
from geometry_msgs.msg import PoseStamped
from base.actions.action_utils import ActionUtils
from base.tasks.Task import Task
from base.actions.Action import Action
from django.http import FileResponse
from galileo_msg.srv import RoadInit, RoadInitRequest
from base.core import SCHEDULE_MANAGER, SCHEDULE_MANAGER_LOCK
from django.test import Client

CURRENT_PATH = ""
CURRENT_MAP = ""

@csrf_exempt
@allowed_status(["Free"])
@token_required
@allowed_methods(["GET"])
def start(req):
    """start navigation"""
    global CURRENT_PATH, CURRENT_MAP
    with base.core.STATUS_LOCK:
        base.core.SYS_STATUS = "Busy"
    req_data = req.GET
    if "map" in req_data and req_data["map"] != "" and ( "path" not in req_data or req_data["path"] == "" ):
        if not os.path.exists(os.path.join(MAP_PATH, req_data["map"])):
            with base.core.STATUS_LOCK:
                base.core.SYS_STATUS = "Free"
            return HttpResponse(json.dumps({
                "result": False,
                "description": "target map does not exist",
            }, indent=4), content_type="application/json", status=404)
        # 切换当前地图至目标地图
        res = subprocess.Popen("cp -rfp {map_path}/{map_name}/* {map_workspace}/ ".format(map_path=MAP_PATH, map_name=req_data["map"].encode("utf-8"), map_workspace=MAP_WORKSPACE_PATH),
                            universal_newlines=True,
                            shell=True,
                            stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
                            )
        if res.wait() != 0:
            rospy.logerr(res.stdout.read())
            with base.core.STATUS_LOCK:
                base.core.SYS_STATUS = "Free"
            return HttpResponse(json.dumps({
                "result": False,
                "description": "switch map failed",
            }, indent=4), content_type="application/json", status=500)
        # 开启导航指令
        galileo_cmds = GalileoNativeCmds()
        galileo_cmds.data = 'm' + chr(0x00)
        galileo_cmds.length = len(galileo_cmds.data)
        set_topic_data('/galileo/cmds', GalileoNativeCmds, galileo_cmds)
        timecount = 0
        while timecount < TIMEOUT:
            time.sleep(1)
            timecount += 1
            data = get_topic_data("/galileo/status", GalileoStatus)
            if data is None:
                continue
            if data is not None and "hz" in data and data["hz"] != 0:
                if data['data'].navStatus == 1:
                    break
        if timecount >= TIMEOUT:
            # 发送关闭导航指令
            galileo_cmds.data = 'm' + chr(0x04)
            galileo_cmds.length = len(galileo_cmds.data)
            galileo_cmds.header.stamp = rospy.Time.now()
            set_topic_data('/galileo/cmds', GalileoNativeCmds, galileo_cmds)
            with base.core.STATUS_LOCK:
                base.core.SYS_STATUS = "Free"
            return HttpResponse(json.dumps({
                "result": False,
                "description": "start navigation timeout",
            }, indent=4), content_type="application/json", status=500)

        with base.core.STATUS_LOCK:
            base.core.SYS_STATUS = "Navigating"
        map_name = ""
        if os.path.exists(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")):
            with open(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")) as map_name_file:
                map_name = map_name_file.read().strip()                
        CURRENT_MAP = map_name
        return HttpResponse(json.dumps({
            "result": True
        }, indent=4), content_type="application/json")


    if "map" in req.GET and "path" in req.GET:
        # 检查目标地图和路径是否存在
        if not os.path.exists(os.path.join(MAP_PATH, req_data["map"])):
            with base.core.STATUS_LOCK:
                base.core.SYS_STATUS = "Free"
            return HttpResponse(json.dumps({
                "result": False,
                "description": "target map does not exist",
            }, indent=4), content_type="application/json", status=404)
        if not os.path.exists(os.path.join(MAP_PATH, req_data["map"], "path_{path_name}.csv".format(path_name=req_data["path"].encode("utf-8")).decode("utf-8"))):
            with base.core.STATUS_LOCK:
                base.core.SYS_STATUS = "Free"
            return HttpResponse(json.dumps({
                "result": False,
                "description": "target path does not exist",
            }, indent=4), content_type="application/json", status=404)
        # 检查当前地图和路径是否对应
        map_name = ""
        if os.path.exists(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")):
            with open(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")) as map_name_file:
                map_name = map_name_file.read().strip()
        # 切换当前地图至目标地图
        res = subprocess.Popen("cp -rfp {map_path}/{map_name}/* {map_workspace}/ ".format(map_path=MAP_PATH, map_name=req_data["map"].encode("utf-8"), map_workspace=MAP_WORKSPACE_PATH),
                            universal_newlines=True,
                            shell=True,
                            stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
                            )
        if res.wait() != 0:
            rospy.logerr(res.stdout.read())
            with base.core.STATUS_LOCK:
                base.core.SYS_STATUS = "Free"
            return HttpResponse(json.dumps({
                "result": False,
                "description": "switch map failed",
            }, indent=4), content_type="application/json", status=500)
        # 切换至目标路径
        res = subprocess.Popen("cp -rf {map_workspace}/path_{path_name}.csv {map_workspace}/path.csv".format(path_name=req_data["path"].encode("utf-8"), map_workspace=MAP_WORKSPACE_PATH),
                            universal_newlines=True,
                            shell=True,
                            stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
                            )
        result_1 = res.wait()
        # 复制旧版导航文件
        res = subprocess.Popen("cp -rf {map_workspace}/nav_{path_name}.csv {map_workspace}/nav.csv".format(path_name=req_data["path"].encode("utf-8"), map_workspace=MAP_WORKSPACE_PATH),
                            universal_newlines=True,
                            shell=True,
                            stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
                            )
        res.wait()
        # 复制新版导航文件
        res = subprocess.Popen("cp -rf {map_workspace}/new_nav_{path_name}.csv {map_workspace}/new_nav.csv".format(path_name=req_data["path"].encode("utf-8"), map_workspace=MAP_WORKSPACE_PATH),
                            universal_newlines=True,
                            shell=True,
                            stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
                            )
        result_3 = res.wait()
        # 删除旧active文件
        res = subprocess.Popen("rm -rf {map_workspace}/*.active".format(map_workspace=MAP_WORKSPACE_PATH),
                            universal_newlines=True,
                            shell=True,
                            stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
                            )
        res.wait()
        # 创建active文件
        res = subprocess.Popen("cp -rf {map_workspace}/path_{path_name}.csv {map_workspace}/path_{path_name}.csv.active".format(path_name=req_data["path"].encode("utf-8"), map_workspace=MAP_WORKSPACE_PATH),
                            universal_newlines=True,
                            shell=True,
                            stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
                            )
        result_5 = res.wait()

        if result_1 != 0 or result_3 != 0 or result_5 != 0:
            rospy.logerr(res.stdout.read())
            with base.core.STATUS_LOCK:
                base.core.SYS_STATUS = "Free"
            return HttpResponse(json.dumps({
                "result": False,
                "description": "switch path failed",
            }, indent=4), content_type="application/json", status=500)
    else:
        # 通过计算path的md5值来判断路径
        if not os.path.exists(os.path.join(MAP_WORKSPACE_PATH, "path.csv")):
            return HttpResponse(json.dumps({
                "result": False,
                "description": "no map and path selected"
            }, indent=4), content_type="application/json", status=400)
        path_md5sum = md5sum(os.path.join(MAP_WORKSPACE_PATH, "path.csv"))
        for file_path in os.listdir(MAP_WORKSPACE_PATH):
            if md5sum(os.path.join(MAP_WORKSPACE_PATH, file_path)) == path_md5sum:
                CURRENT_PATH = file_path[5:-4] # 从文件名提取路径名称

    # 开启导航指令
    galileo_cmds = GalileoNativeCmds()
    galileo_cmds.data = 'm' + chr(0x00)
    galileo_cmds.length = len(galileo_cmds.data)
    set_topic_data('/galileo/cmds', GalileoNativeCmds, galileo_cmds)
    timecount = 0
    while timecount < TIMEOUT:
        time.sleep(1)
        timecount += 1
        data = get_topic_data("/galileo/status", GalileoStatus)
        if data is None:
            continue
        if data is not None and "hz" in data and data["hz"] != 0:
            if data['data'].navStatus == 1:
                break
    if timecount >= TIMEOUT:
        # 发送关闭导航指令
        galileo_cmds.data = 'm' + chr(0x04)
        galileo_cmds.length = len(galileo_cmds.data)
        galileo_cmds.header.stamp = rospy.Time.now()
        set_topic_data('/galileo/cmds', GalileoNativeCmds, galileo_cmds)
        with base.core.STATUS_LOCK:
            base.core.SYS_STATUS = "Free"
        return HttpResponse(json.dumps({
            "result": False,
            "description": "start navigation timeout",
        }, indent=4), content_type="application/json", status=500)

    with base.core.STATUS_LOCK:
        base.core.SYS_STATUS = "Navigating"
    map_name = ""
    if os.path.exists(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")):
        with open(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")) as map_name_file:
            map_name = map_name_file.read().strip()
    if "path" in req_data:
        CURRENT_PATH = req_data["path"]
            
    CURRENT_MAP = map_name
    # set map to ros parameter server
    # rospy.set_param('/galileo/galileo_map', CURRENT_MAP)
    # rospy.set_param('/galileo/galileo_path', CURRENT_PATH)
    if "path" in req_data:
        # 设置.active文件
        # 删除所有.active文件
        res1 = subprocess.Popen("rm -rf {map_workspace}/*.active".format(map_workspace=MAP_WORKSPACE_PATH),
            universal_newlines=True,
            shell=True,
            stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
        )
        if res1.wait() != 0:
            rospy.logerr(res1.stdout.read())
            return HttpResponse(json.dumps({
                "result": False,
                "description": "delete acitve files failed",
            }, indent=4), content_type="application/json", status=500)
        res = subprocess.Popen("cp -rf {map_workspace}/path_{path_name}.csv {map_workspace}/path_{path_name}.csv.active".format(path_name=req_data["path"].encode("utf-8"), map_workspace=MAP_WORKSPACE_PATH),
            universal_newlines=True,
            shell=True,
            stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
        )
        if res.wait() != 0:
            rospy.logerr(res.stdout.read())
            return HttpResponse(json.dumps({
                "result": False,
                "description": "copy active file failed",
            }, indent=4), content_type="application/json", status=500)
    return HttpResponse(json.dumps({
        "result": True
    }, indent=4), content_type="application/json")

@csrf_exempt
@allowed_status(["Free"])
@token_required
@allowed_methods(["GET"])
def start_schedule_mode(req):
    """
    启动调度状态下的导航模式
    """
    with base.core.STATUS_LOCK:
        base.core.SYS_STATUS = "Busy"
    if len(SCHEDULE_MANAGER) == 0:
        with base.core.STATUS_LOCK:
            base.core.SYS_STATUS = "Free"
        return HttpResponse(json.dumps({
            "result": False,
            "description": "no scheduel manager found",
        }, indent=4), content_type="application/json", status=500)
    # 开启导航指令
    galileo_cmds = GalileoNativeCmds()
    galileo_cmds.data = 'm' + chr(0x07)
    galileo_cmds.length = len(galileo_cmds.data)
    set_topic_data('/galileo/cmds', GalileoNativeCmds, galileo_cmds)
    timecount = 0
    while timecount < TIMEOUT:
        time.sleep(1)
        timecount += 1
        data = get_topic_data("/galileo/status", GalileoStatus)
        if data is None:
            continue
        if data is not None and "hz" in data and data["hz"] != 0:
            if data['data'].navStatus == 1:
                break
    if timecount >= TIMEOUT:
        # 发送关闭导航指令
        galileo_cmds.data = 'm' + chr(0x08)
        galileo_cmds.length = len(galileo_cmds.data)
        galileo_cmds.header.stamp = rospy.Time.now()
        set_topic_data('/galileo/cmds', GalileoNativeCmds, galileo_cmds)
        with base.core.STATUS_LOCK:
            base.core.SYS_STATUS = "Free"
        return HttpResponse(json.dumps({
            "result": False,
            "description": "start navigation timeout",
        }, indent=4), content_type="application/json", status=500)

    with base.core.STATUS_LOCK:
        base.core.SYS_STATUS = "Navigating"
    map_name = ""
    if os.path.exists(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")):
        with open(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")) as map_name_file:
            map_name = map_name_file.read().strip()
    # set map to ros parameter server
    rospy.set_param('/galileo/galileo_map', map_name)
    # init road to global planner
    rospy.wait_for_service('/move_base/service/road_init')
    init_road = rospy.ServiceProxy('/move_base/service/road_init', RoadInit)
    layout_res = None
    with SCHEDULE_MANAGER_LOCK:
        try:
            layout_res = requests.get("http://{ip}:{port}/api/v1/layout".format(
                ip=SCHEDULE_MANAGER[0]["ip"],
                port=SCHEDULE_MANAGER[0]["port"],
            ))
        except Exception:
            pass
    if layout_res is None or layout_res.status_code != 200:
        galileo_cmds.data = 'm' + chr(0x08)
        galileo_cmds.length = len(galileo_cmds.data)
        galileo_cmds.header.stamp = rospy.Time.now()
        set_topic_data('/galileo/cmds', GalileoNativeCmds, galileo_cmds)
        with base.core.STATUS_LOCK:
            base.core.SYS_STATUS = "Free"
        return HttpResponse(json.dumps({
            "result": False,
            "description": "failed to get layout data from schedule manager",
        }, indent=4), content_type="application/json", status=500)
    layout_data = json.loads(layout_res.content.decode("utf-8"))["layout"]
    res = init_road(json.dumps(layout_data, indent=4))
    if res.code != 0:
        galileo_cmds.data = 'm' + chr(0x08)
        galileo_cmds.length = len(galileo_cmds.data)
        galileo_cmds.header.stamp = rospy.Time.now()
        set_topic_data('/galileo/cmds', GalileoNativeCmds, galileo_cmds)
        with base.core.STATUS_LOCK:
            base.core.SYS_STATUS = "Free"
        return HttpResponse(json.dumps({
            "result": False,
            "description": res.error_message,
        }, indent=4), content_type="application/json", status=500)
    return HttpResponse(json.dumps({
        "result": True
    }, indent=4), content_type="application/json")

@csrf_exempt
@token_required
@allowed_status(["Navigating"])
@allowed_methods(["GET"])
def stop(_req):
    """stop navigation"""
    with base.core.STATUS_LOCK:
        base.core.SYS_STATUS = "Busy"
    # 发送关闭导航指令
    galileo_cmds = GalileoNativeCmds()
    galileo_cmds.data = 'm' + chr(0x04)
    galileo_cmds.length = len(galileo_cmds.data)
    galileo_cmds.header.stamp = rospy.Time.now()
    set_topic_data('/galileo/cmds', GalileoNativeCmds, galileo_cmds)
    timecount = 0
    while timecount < TIMEOUT:
        time.sleep(1)
        timecount += 1
        data = get_topic_data("/galileo/status", GalileoStatus)
        if data is None:
            continue
        if data is not None and "hz" in data and data["hz"] != 0:
            if data['data'].navStatus == 0:
                break
    if timecount >= TIMEOUT:
        return HttpResponse(json.dumps({
            "result": False,
            "description": "stop navigation timeout"
        }, indent=4), content_type="application/json")
    with base.core.STATUS_LOCK:
        base.core.SYS_STATUS = "Free"
    return HttpResponse(json.dumps({
        "result": True
    }, indent=4), content_type="application/json")

@csrf_exempt
@token_required
@allowed_status(["Navigating"])
@allowed_methods(["GET"])
def stop_schedule_mode(req):
    """
    停止调度状态的导航模式
    """
    with base.core.STATUS_LOCK:
        base.core.SYS_STATUS = "Busy"
    # 发送关闭导航指令
    galileo_cmds = GalileoNativeCmds()
    galileo_cmds.data = 'm' + chr(0x08)
    galileo_cmds.length = len(galileo_cmds.data)
    galileo_cmds.header.stamp = rospy.Time.now()
    set_topic_data('/galileo/cmds', GalileoNativeCmds, galileo_cmds)
    timecount = 0
    while timecount < TIMEOUT:
        time.sleep(1)
        timecount += 1
        data = get_topic_data("/galileo/status", GalileoStatus)
        if data is None:
            continue
        if data is not None and "hz" in data and data["hz"] != 0:
            if data['data'].navStatus == 0:
                break
    if timecount >= TIMEOUT:
        return HttpResponse(json.dumps({
            "result": False,
            "description": "stop navigation timeout"
        }, indent=4), content_type="application/json")
    with base.core.STATUS_LOCK:
        base.core.SYS_STATUS = "Free"
    return HttpResponse(json.dumps({
        "result": True
    }, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def pose(_req):
    """get current pose"""
    data = get_topic_data("/galileo/status", GalileoStatus)
    res = {
        "x": 0,
        "y": 0,
        "angle": 0,
    }
    if data is not None:
        res["x"] = data["data"].currentPosX
        res["y"] = data["data"].currentPosY
        res["angle"] = data["data"].currentAngle
    return HttpResponse(json.dumps(
        res, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
@allowed_status(["Navigating"])
def current_path(req):
    global CURRENT_PATH, CURRENT_MAP
    if CURRENT_PATH == "":
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "path is not set"
        }, indent=4), content_type="application/json", status=400)

    return HttpResponse(json.dumps({
        "map": CURRENT_MAP,
        "path": CURRENT_PATH,
    }, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
@allowed_status(["Navigating"])
def target_distance(req):
    data = get_topic_data("/galileo/status", GalileoStatus)
    if data is not None:
        return HttpResponse(json.dumps({
            "distance": data['data'].targetDistance
        }, indent=4), content_type="application/json")
    else:
        return HttpResponse(json.dumps({
            "status": 'error',
            "description": 'get distance data failed'
        }, indent=4), content_type="application/json", status=400)


@csrf_exempt
@token_required
@allowed_status(["Navigating"])
@allowed_methods(["GET"])
def current_waypoints(req):
    # 获取当前规划路径
    return HttpResponse(json.dumps([], indent=4),
                        content_type="application/json")


@csrf_exempt
@token_required
@allowed_status(["Navigating"])
@allowed_methods(["GET"])
def current_path_info(req):
    global CURRENT_PATH, CURRENT_MAP
    return HttpResponse(json.dumps({
        "path": CURRENT_PATH,
        "map": CURRENT_MAP,
    }, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET", "DELETE"])
def saved_maps(req):
    # 处理删除地图
    if req.method == "DELETE":
        if "name" in req.GET:
            if os.path.exists(os.path.join(MAP_PATH, req.GET["name"])):
                res = subprocess.Popen("rm -rf {map_dir}".format(map_dir=os.path.join(MAP_PATH, req.GET["name"].encode("utf-8"))),
                    universal_newlines=True,
                    shell=True,
                    stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
                )
                if res.wait() != 0:
                    return HttpResponse(json.dumps({
                        "status": "error",
                        "description": "delete map failed"
                    }, indent=4), content_type="application/json", status=400)
            map_name = ""
            if os.path.exists(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")):
                with open(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")) as map_name_file:
                    map_name = map_name_file.read().strip()
            if map_name == req.GET["name"].encode("utf-8"):
                # 删除当前地图，清空slamdb
                res = subprocess.Popen("rm -rf {map_dir}/*".format(map_dir=MAP_WORKSPACE_PATH),
                    universal_newlines=True,
                    shell=True,
                    stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
                )
                if res.wait() != 0:
                    return HttpResponse(json.dumps({
                        "status": "error",
                        "description": "clear slamdb failed"
                    }, indent=4), content_type="application/json", status=400)
            return HttpResponse(json.dumps({
                "status": "ok",
            }, indent=4), content_type="application/json")
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "name is required"
        }, indent=4), content_type="application/json", status=400)
    if "name" in req.GET:
        if not os.path.exists(os.path.join(MAP_PATH, req.GET["name"], "map.yaml")):
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "target map dose not exist"
            }, indent=4), content_type="application/json", status=404)
        map_info = None
        with open(os.path.join(MAP_PATH, req.GET["name"], "map.yaml")) as map_file:
            map_info = yaml.safe_load(map_file)
        if map_info is None:
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "load map info failed"
            }, indent=4), content_type="application/json", status=400)
        res = subprocess.Popen("md5sum {map_file}".format(map_file=os.path.join(MAP_PATH, req.GET["name"].encode("utf-8"), "map.pgm")),
                               universal_newlines=True,
                               shell=True,
                               stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
                               )
        if res.wait() != 0:
            return HttpResponse(json.dumps({
                "status": "error",
                "desciption": "calculate md5sum failed",
            }, indent=4), content_type="application/json", status=400)
        md5sum = res.stdout.read().split(' ')[0]
        return HttpResponse(json.dumps({
            "resolution": map_info["resolution"],
            "origin": map_info["origin"],
            "occupied_thresh": map_info["occupied_thresh"],
            "free_thresh": map_info["free_thresh"],
            "negate": map_info["negate"],
            "name": req.GET["name"],
            "md5sum": md5sum,
        }, indent=4), content_type="application/json")
    else:
        map_list = os.listdir(MAP_PATH)
        map_info_list = []
        for map_name in map_list:
            map_info = None
            if os.path.exists(os.path.join(MAP_PATH, map_name, "map.yaml")):
                with open(os.path.join(MAP_PATH, map_name, "map.yaml")) as map_file:
                    map_info = yaml.safe_load(map_file)
            if map_info is None:
                continue
            res = subprocess.Popen("md5sum {map_file}".format(map_file=os.path.join(MAP_PATH, map_name, "map.pgm")),
                                   universal_newlines=True,
                                   shell=True,
                                   stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
                                   )
            if res.wait() != 0:
                continue
            md5sum = res.stdout.read().split(' ')[0]
            map_info_list.append({
                "resolution": map_info["resolution"],
                "origin": map_info["origin"],
                "occupied_thresh": map_info["occupied_thresh"],
                "free_thresh": map_info["free_thresh"],
                "negate": map_info["negate"],
                "name": map_name,
                "md5sum": md5sum,
            })
        return HttpResponse(json.dumps(map_info_list, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def robot_tracks(req):
    if "map_name" in req.GET:
        track_path = os.path.join(
            MAP_PATH, req.GET["map_name"], "RobotTrajectory.txt")
        if not os.path.exists(track_path):
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "target map not found"
            }, indent=4), content_type="application/json", status=400)
        with open(track_path) as track_file:
            data = track_file.read()
            data = data.split('\n')
            points_list = [
                {
                    "x": float(line.split(' ')[1]),
                    "y": float(line.split(' ')[2]),
                    "z": float(line.split(' ')[3])
                } for line in data if len(line.split(' ')) >= 8]
            return HttpResponse(json.dumps({
                "trajectory": points_list,
            }, indent=4), content_type="application/json")
    else:
        map_list = os.listdir(MAP_PATH)
        track_info_list = []
        for map_name in map_list:
            track_path = os.path.join(
                MAP_PATH, map_name, "RobotTrajectory.txt")
            if not os.path.exists(track_path):
                continue
            with open(track_path) as track_file:
                data = track_file.read()
                data = data.split('\n')
                points_list = [
                    {
                        "x": float(line.split(' ')[1]),
                        "y": float(line.split(' ')[2]),
                        "z": float(line.split(' ')[3])
                    } for line in data if len(line.split(' ')) >= 8]
                track_info_list.append({
                    "map_name": map_name,
                    "track": points_list
                })
        return HttpResponse(json.dumps(track_info_list, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def map_pgm(req):
    if "name" in req.GET:
        map_file_path = os.path.join(MAP_PATH, req.GET["name"], "map.pgm")
        if not os.path.exists(map_file_path):
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "target map not exist"
            }, indent=4), content_type="application/json", status=404)
        res = FileResponse(open(map_file_path, "rb"))
        res['Content-Disposition'] = 'attachment; filename=' + \
            req.GET["name"] + ".pgm"
        res['Content-Type'] = "image/pgm"
        return res
    else:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "name is required",
        }, indent=4), content_type="application/json", status=400)
    
@csrf_exempt
@token_required
@allowed_methods(["GET"])
def map_png(req):
    if "name" in req.GET:
        map_file_path = os.path.join(MAP_PATH, req.GET["name"], "map.pgm")
        if not os.path.exists(map_file_path):
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "target map not exist"
            }, indent=4), content_type="application/json", status=404)
        img = Image.open(map_file_path)
        png_path = os.path.join(MAP_PATH, req.GET["name"], "map.png")
        img.save(png_path)
        res = FileResponse(open(png_path, "rb"))
        res['Content-Disposition'] = 'attachment; filename=' + \
            req.GET["name"] + ".png"
        res['Content-Type'] = "image/png"
        return res
    else:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "name is required",
        }, indent=4), content_type="application/json", status=400)


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def current_map(req):
    # 获取当前 map workspace 内的地图
    if not os.path.exists(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")):
        return HttpResponse(json.dumps({
            "name": ""
        }, indent=4), content_type="application/json")
    map_point_file_path = os.path.join(MAP_WORKSPACE_PATH, "mappoints.bson")
    if not os.path.exists(map_point_file_path):
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "no map found"
        }, indent=4), content_type="application/json", status=404)
    md5 = md5sum(map_point_file_path)
    with open(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")) as map_name_file:
        map_name = map_name_file.read()
        return HttpResponse(json.dumps({
            "name": map_name.strip(),
            "md5sum": md5,
        }, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def upload_map(req):
    """
    上传当前地图至服务器
    """
    if "server_id" not in req.GET:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "server_id is required"
        }, indent=4), content_type="application/json", status=400)
    # 创建上传action
    upload_action = ActionUtils.new_action({
        "type": "upload_map_action",
        "server_id": req.GET["server_id"]
    })
    # 创建任务
    target_task = Task.new_task({
        "name": "upload_map",
    })
    target_task.sub_tasks.append(upload_action)
    threading.Thread(target=target_task.start).start()
    # 返回 task
    return HttpResponse(json.dumps(target_task.to_dict(), indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def download_map(req):
    """
    从调度服务器下载地图
    """
    if not set(["server_id", "map_id"]).issubset(req.GET):
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "server_id and map_id are required"
        }, indent=4), content_type="application/json", status=400)
    
    # 创建下载action
    download_action = ActionUtils.new_action({
        "type": "download_map_action",
        "server_id": req.GET["server_id"],
        "map_id": req.GET["map_id"]
    })
    target_task = Task.new_task({
        "name": "download_map"
    })
    target_task.sub_tasks.append(download_action)
    threading.Thread(target=target_task.start).start()
    return HttpResponse(json.dumps(target_task.to_dict(), indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["POST"])
@allowed_status(["Navigating"])
@required_keys(["x", "y", "theta"])
def start_nav_task(req):
    req_data = {}
    try:
        req_data = json.loads(req.body.decode("utf-8"))
    except Exception:
        pass
    # 创建 nav action
    nav_action = ActionUtils.new_action({
        "type": "nav_action",
        "x": req_data["x"],
        "y": req_data["y"],
        "theta": req_data["theta"],
    })
    # 创建 nav task
    nav_task = Task.new_task({
        "name": "navigation task"
    })
    nav_task.sub_tasks.append(nav_action)
    threading.Thread(target=nav_task.start).start()
    return HttpResponse(json.dumps(nav_task.to_dict(), indent=4), content_type="application/json")


CHARGE_TASK = None

@csrf_exempt
@token_required
@allowed_methods(["GET"])
@allowed_status(["Navigating"])
def go_charge(req):
    # 调用自动充电，机器人先导航至充电桩附近，然后对接充电桩进行充电
    # 检查是否有充电桩文件
    if not os.path.exists(os.path.join(MAP_WORKSPACE_PATH, "dock_station.txt")):
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "charge docker position file not found"
        }, indent=4), content_type="application/json", status=400)
    # 获取充电桩位置
    docker_pos_x = 0
    docker_pos_y = 0
    docker_theta = 0
    (docker_pos_x, docker_pos_y, docker_theta) = load_charge_docker()
    # 检查是否处于执行任务过程中
    data = get_topic_data("/galileo/status", GalileoStatus)
    if data['data'].loopStatus == 1:
        # 关闭自动巡检
        galileo_cmds = GalileoNativeCmds()
        galileo_cmds.data = 'm' + chr(0x06)
        galileo_cmds.length = len(galileo_cmds.data)
        set_topic_data('/galileo/cmds', GalileoNativeCmds, galileo_cmds)
    if data['data'].targetStatus != 0:
        # 取消当前导航任务
        galileo_cmds = GalileoNativeCmds()
        galileo_cmds.data = 'i' + chr(0x02)
        galileo_cmds.length = len(galileo_cmds.data)
        set_topic_data('/galileo/cmds', GalileoNativeCmds, galileo_cmds)
    # 创建导航任务
    nav_action = ActionUtils.new_action({
        "type": "nav_action",
        "x": docker_pos_x,
        "y": docker_pos_y,
        "theta": docker_theta
    })
    charge_action = ActionUtils.new_action({
        "type": "charge_action",
        "x": docker_pos_x,
        "y": docker_pos_y,
        "theta": docker_theta
    })

    target_task = Task.new_task({
        "name": "charge_task"
    })
    target_task.append(nav_action)
    target_task.append(charge_action)
    threading.Thread(target=target_task.start).start()
    global CHARGE_TASK
    CHARGE_TASK = target_task
    return HttpResponse(json.dumps(target_task.to_dict(), indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def stop_charge(req):
    global CHARGE_TASK
    if CHARGE_TASK == None:
        status_data = get_topic_data("/galileo/status", GalileoStatus)
        if status_data is not None and status_data["data"].chargeStatus != 0:
            stop_charge_msg = Bool()
            stop_charge_msg.data = False
            set_topic_data("/bw_auto_dock/EnableCharge", Bool, stop_charge_msg)
            return HttpResponse(json.dumps({
                "status": "ok",
                "description": "charge stoped"
            }, indent=4), content_type="application/json")
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "no charge task found"
        }, content_type="application/json"), status=404)
    CHARGE_TASK.stop()
    return HttpResponse(json.dumps({
        "status": "ok",
        "task": CHARGE_TASK.to_dict()
    }, indent=4), content_type="application/json")

@csrf_exempt
@token_required
@allowed_methods(["GET"])
@allowed_status(["Navigating"])
def move_to_index(req):
    if "index" not in req.GET:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "index is needed to move robot"
        }, indent=4), content_type="application/json", status=400)
    # 载入nav_path
    target_points = load_nav_file()
    index = -1
    try:
        index = int(req.GET["index"])
    except Exception:
        pass
    if index == -1:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "invalid index: " + req.GET["index"]
        }, indent=4), content_type="application/json", status=400)

    if len(target_points) <= index:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "index out of range"
        }, indent=4), content_type="application/json", status=400)
    
    current_point = target_points[index]

    c = Client()
    res = c.post("/api/v1/navigation/start_nav_task", json.dumps({
        "x": current_point[0],
        "y": current_point[1],
        "theta": current_point[2]
    }, indent=4), content_type="application/json")
    if res.status_code != 200:
        print(res.content.decode("utf-8"))
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "start nav action failed"
        }, indent=4), content_type="application/json")
    return HttpResponse(res.content.decode("utf-8"), content_type="application/json")

@csrf_exempt
@token_required
@allowed_methods(["GET"])
@allowed_status(["Navigating"])
def stop_nav_task(req):
    NavAction.stop_all()
    return HttpResponse(json.dumps({
        "status": "OK"
    }, indent=4), content_type="application/json")

@csrf_exempt
@token_required
@allowed_methods(["GET"])
@allowed_status(["Navigating"])
def pause_nav_task(req):
    NavAction.pause_all()
    return HttpResponse(json.dumps({
        "status": "OK"
    }, indent=4), content_type="application/json")

@csrf_exempt
@token_required
@allowed_methods(["GET"])
@allowed_status(["Navigating"])
def resume_nav_tesk(req):
    NavAction.resume_all()
    return HttpResponse(json.dumps({
        "status": "OK"
    }, indent=4), content_type="application/json")

@csrf_exempt
@token_required
@allowed_methods(["GET", "POST"])
def charge_pose(req):
    
    if req.method == "POST":
        charge_msg = Bool()
        charge_msg.data = True
        old_md5 = ""
        if os.path.exists(os.path.join(MAP_WORKSPACE_PATH, "dock_station.txt")):
            old_md5 = md5sum(os.path.join(MAP_WORKSPACE_PATH, "dock_station.txt"))
        set_topic_data('/bw_auto_dock/dockposition_save', Bool, charge_msg)
        timecount = 0
        while timecount < TIMEOUT:
            time.sleep(1)
            timecount += 1
            if os.path.exists(os.path.join(MAP_WORKSPACE_PATH, "dock_station.txt")):
                new_md5 = md5sum(os.path.join(MAP_WORKSPACE_PATH, "dock_station.txt"))
                if new_md5 != old_md5:
                    break
        if timecount >= TIMEOUT:
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "save charge docker position failed"
            }, indent=4), content_type="application/json", status=400)
        
        cp_task = subprocess.Popen("cp -rf {MAP_WORKSPACE_PATH}/dock_station.txt {MAP_PATH}/{map_name}/dock_station.txt"
            .format(
                MAP_WORKSPACE_PATH=MAP_WORKSPACE_PATH,
                MAP_PATH = MAP_PATH,
                map_name=get_current_map_name()
            ), 
            universal_newlines=True,
            shell=True,
            stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
        )
        if cp_task.wait() != 0:
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "copy docker station file failed"
            }, indent=4), content_type="application/json", status=400)

    (pos_x, pos_y, theta) = load_charge_docker()
    if pos_x is None:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "charge position is not avaliable"
        }, indent=4), content_type="application/json", status=400)
    return HttpResponse(json.dumps({
        "x": pos_x,
        "y": pos_y,
        "theta": theta
    }, indent=4), content_type="application/json")
    


LOOP_TASK = None
@csrf_exempt
@token_required
@allowed_methods(["GET", "POST", "DELETE"])
@allowed_status(["Navigating"])
def loop_task(req):
    global LOOP_TASK
    if req.method == "GET":
        if LOOP_TASK is not None:
            LOOP_TASK = Task.load_by_id(LOOP_TASK["id"]).to_dict()
        if LOOP_TASK is None or LOOP_TASK["state"] in["CANCELLED", "ERROR", "COMPLETED"]:
            LOOP_TASK = None
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "no active loop task"
            }, indent=4), content_type="application/json", status=400)
        return HttpResponse(json.dumps(LOOP_TASK, indent=4), content_type="application/json")
    if req.method == "DELETE":
        if LOOP_TASK is None:
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "no active loop task"
            }, indent=4), content_type="application/json", status=400)
        c = Client()
        res = c.get("/api/v1/task/stop?id=" + LOOP_TASK["id"])
        if res.status_code != 200:
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "stop loop task failed"
            }, indent=4), content_type="application/json", status=400)
        LOOP_TASK = None
        return HttpResponse(res.content.decode("utf-8"), content_type="application/json")
    if LOOP_TASK is not None:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "loop task is already running"
        }, indent=4), content_type="application/json", status=400)
    target_points = load_nav_file()
    data = get_topic_data("/galileo/status", GalileoStatus)
    if data is None:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "get current position failed",
        }, indent=4), content_type="application/json", status=400)
    current_pose = [data["data"].currentPosX, data["data"].currentPosY, data["data"].currentAngle]
    back_points = []
    next_target = None
    mini_distance = 0
    for point in target_points:
        theta_delta = math.atan2(
            point[1] - current_pose[1], point[0] - current_pose[0])
        delta_current = abs(theta_delta - current_pose[2])
        if delta_current > math.pi:
            delta_current = abs(2 * math.pi - delta_current)

        rospy.loginfo("delta_current: " + str(delta_current))
        rospy.loginfo("current pose: " + str(current_pose[0]) +
                        " " + str(current_pose[1]))
        rospy.loginfo("point: " + str(point[0]) +
                        " " + str(point[1]))
        rospy.loginfo("theta_current: " + str(current_pose[2]))
        rospy.loginfo("theta_delta: " + str(theta_delta))
        if delta_current > math.pi / 2:
            back_points.append(point)
            continue
        current_distance = pose_distance(current_pose, point)

        if current_distance < mini_distance or mini_distance == 0:
            mini_distance = current_distance
            next_target = point
    mini_distance = 0
    if next_target is None:
        for point in back_points:
            current_distance = pose_distance(current_pose, point)
            if current_distance < mini_distance or mini_distance == 0:
                mini_distance = current_distance
                next_target = point
    start_index = target_points.index(next_target)
    target_points = target_points[start_index:] + target_points[:start_index]
    new_task = {
        "name": "loop task",
        "sub_tasks": [{
            "type": "nav_action",
            "x": point[0],
            "y": point[1],
            "theta": point[2]
        } for point in target_points],
        "loop_flag": True,
    }
    req_data = {}
    try:
        req_data = json.loads(req.body.decode("utf-8"))
    except Exception:
        pass
    if "wait_time" in req_data:
        wait_time = 0
        try:
            wait_time = int(req_data["wait_time"])
        except Exception:
            pass
        nav_tasks = new_task["sub_tasks"]
        sub_tasks = []
        for sub_task in nav_tasks:
            sub_tasks.append(sub_task)
            sub_tasks.append({
                "type": "sleep_action",
                "wait_time": wait_time
            })
        new_task["sub_tasks"] = sub_tasks
    c = Client()
    res = c.post("/api/v1/task", json.dumps(new_task), content_type="application/json")
    if res.status_code != 200:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "create nav task failed: " + res.content.decode("utf-8")
        }, indent=4), content_type="application/json", status=res.status_code)
    
    task_info = json.loads(res.content.decode("utf-8"))
    res = c.get("/api/v1/task/start?id=" + task_info["id"])
    if res.status_code != 200:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "start nav task failed: " + res.content.decode("utf-8")
        }, indent=4), content_type="application/json", status=res.status_code)
    LOOP_TASK = json.loads(res.content.decode("utf-8"))
    return HttpResponse(json.dumps(LOOP_TASK, indent=4),
        content_type="application/json")

def load_charge_docker():
    if not os.path.exists(os.path.join(MAP_WORKSPACE_PATH, "dock_station.txt")):
        return (None, None, None)
    with open(os.path.join(MAP_WORKSPACE_PATH, "dock_station.txt")) as dock_file:
        dock_content = dock_file.read()
        lines = dock_content.split('\n')
        line_data = lines[0].split(' ')
        ref_point1 = {
            "x": float(line_data[0]),
            "y": float(line_data[1]),
        }
        line_data = lines[1].split(' ')
        ref_point2 = {
            "x": float(line_data[0]),
            "y": float(line_data[1]),
        }
        line_data = lines[2].split(' ')
        station_point = {
            "x": float(line_data[0]),
            "y": float(line_data[1]),
        }
        docker_pos_x = (ref_point1["x"] + ref_point2["x"]) / 2
        docker_pos_y = (ref_point1["y"] + ref_point2["y"]) / 2
        docker_theta = math.atan2(station_point["y"] - docker_pos_y, station_point["x"] - docker_pos_x) + math.pi
        if docker_theta > math.pi:
            docker_theta = docker_theta - math.pi * 2
        return (docker_pos_x, docker_pos_y, docker_theta)

def load_nav_file(map_name=None, path_name=None):
    new_nav_points_file = os.path.join(MAP_WORKSPACE_PATH, "new_nav.csv")
    if map_name != None and path_name != None:
        new_nav_points_file = os.path.join(MAP_PATH, map_name, 
            "new_nav_{path}.csv".format(path=path_name))
    
    target_points = []
    plan_mode = 0
    with open(new_nav_points_file, "r") as new_nav_data_file:
        new_nav_data_str = new_nav_data_file.readline()
        while len(new_nav_data_str) != 0:
            pos_x = float(new_nav_data_str.split(" ")[0])
            pos_y = float(new_nav_data_str.split(" ")[1])
            pos_angle = float(new_nav_data_str.split(" ")[3])
            target_points.append([pos_x, pos_y, pos_angle])
            new_nav_data_str = new_nav_data_file.readline()
            if len(new_nav_data_str.split(" ")) == 7:
                plan_mode = int(new_nav_data_str.split(" ")[6])
    if map_name is None:
        rospy.set_param("/NLlinepatrol_planner/ab_direction", plan_mode)
    return target_points

def pose_distance(pose1, pose2):
    return math.sqrt(math.pow((pose1[0] - pose2[0]), 2)
                        + math.pow((pose1[1] - pose2[1]), 2)
                        )

def get_current_map_name():
    if not os.path.exists(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")):
        return None
    with open(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")) as map_name_file:
        map_name = map_name_file.read()
        return map_name.strip()

@csrf_exempt
@token_required
@allowed_methods(["GET", "POST", "DELETE"])
def path(req):
    """
    机器人路径操作相关
    """
    if req.method == "GET":
        # 获取地图所属路径
        if "map_name" in req.GET and "path_name" in req.GET:
            # 获取路径文件内容
            all_paths = get_map_paths(req.GET["map_name"])
            if all_paths is None:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "target map dose not exist"
                }, indent=4), content_type="application/json", status=404)
            if req.GET["path_name"] not in all_paths:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "target path not exist"
                }, indent=4), content_type="application/json", status=404)
            with open(os.path.join(MAP_PATH, req.GET["map_name"], 
                "path_{path}.csv".format(path=req.GET["path_name"].encode("utf-8")))) as path_file:
                return HttpResponse(json.dumps({
                    "map_name": req.GET["map_name"],
                    "path_name": req.GET["path_name"],
                    "path_data": path_file.read()
                }, indent=4), content_type="application/json")
        elif "map_name" in req.GET:
            # 获取对应地图路径
            all_paths = get_map_paths(req.GET["map_name"])
            if all_paths is None:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "target map dose not exist"
                }, indent=4), content_type="application/json", status=404)
                        # 如果是当前地图则把active path移动到第一个
            map_name = ""
            if os.path.exists(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")):
                with open(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")) as map_name_file:
                    map_name = map_name_file.read().strip()
            if map_name == req.GET["map_name"].encode("utf-8"):
                # 找到.active文件
                files = os.listdir(MAP_WORKSPACE_PATH)
                active_path = ""
                for one_file in files:
                    if one_file.endswith(".csv.active") and one_file.startswith("path_"):
                        active_path = one_file[5:-11]
                if active_path in all_paths:
                    all_paths.remove(active_path)
                    all_paths.insert(0, active_path)
            return HttpResponse(json.dumps(all_paths, indent=4),
                content_type="application/json")
        else:
            # 获取所有地图路径
            map_list = os.listdir(MAP_PATH)
            map_paths = {}
            for map_name in map_list:
                if not os.path.isdir(os.path.join(MAP_PATH, map_name)):
                    continue
                map_paths[map_name] = get_map_paths(map_name)
            return HttpResponse(json.dumps(map_paths, indent=4),
                content_type="application/json")
    if req.method == "POST":
        req_data = {}
        try:
            req_data = json.loads(req.body.decode("utf-8"))
        except:
            pass
        if "map_name" not in req_data or "path_name" not in req_data:
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "map_name and path_name are required"
            }, indent=4), content_type="application/json", status=400)
        # 检查地图是否存在
        if not os.path.exists(os.path.join(MAP_PATH, req_data["map_name"])):
            return HttpResponse(json.dumps({
                "status": "eror",
                "description": "target map not exist"
            }, indent=4), content_type="application/json", status=400)
        # 检查路径是否存在
        if os.path.exists(os.path.join(MAP_PATH, req_data["map_name"], \
            "path_{path}.csv".format(path=req_data["path_name"].encode("utf-8")))):
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "target path already exist"
            }, indent=4), content_type="application/json", status=400)
        if "path_data" not in req_data:
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "path_data is required"
            }, indent=4), content_type="application/json", status=400)
        # 保存路径文件
        with open(os.path.join(MAP_PATH, req_data["map_name"], \
            "path_{path}.csv".format(path=req_data["path_name"].encode("utf-8"))), "w+") as path_file:
            path_file.write(req_data["path_data"])
        return HttpResponse(json.dumps({
            "status": "ok",
            "path_data": req_data["path_data"]
        }, indent=4), content_type="application/json")

    if req.method == "DELETE":
        if "map_name" not in req.GET or "path_name" not in req.GET:
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "map_name and path_name are required"
            }, indent=4), content_type="application/json", status=400)
        if not os.path.exists(os.path.join(MAP_PATH, req.GET["map_name"])):
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "target map not exists"
            }, indent=4), content_type="application/json", status=400)
        path_file = os.path.join(MAP_PATH, req.GET["map_name"], 
            "path_{path}.csv".format(path=req.GET["path_name"].encode("utf-8")))
        if not os.path.exists(path_file):
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "target path not exist"
            }, indent=4), content_type="application/json", status=400)
        os.remove(path_file)
        # 同时删除导航点文件
        nav_file = os.path.join(MAP_PATH, req.GET["map_name"],
            "new_nav_{path}.csv".format(path=req.GET["path_name"].encode("utf-8")))
        if os.path.exists(nav_file):
            os.remove(nav_file)
        nav_file = os.path.join(MAP_PATH, req.GET["map_name"],
            "nav_{path}.csv".format(path=req.GET["path_name"].encode("utf-8")))
        if os.path.exists(nav_file):
            os.remove(nav_file)
        return HttpResponse(json.dumps({
            "status": "ok"
        }, indent=4), content_type="application/json")

def get_map_paths(map_name):
    if not os.path.exists(os.path.join(MAP_PATH, map_name)):
        return None
    map_files = os.listdir(os.path.join(MAP_PATH, map_name))
    path_files = [map_file[5:-4] for map_file in map_files \
        if map_file.startswith("path_") and map_file.endswith(".csv")]
    return path_files

@csrf_exempt
@token_required
@allowed_methods(["GET", "POST", "DELETE", "PUT"])
def get_nav_points(req):
    """
    路径导航点相关操作
    """
    if req.method == "GET":
        if "map_name" in req.GET and "path_name" in req.GET:
            # 获取特定路径导航的API
            target_path = os.path.join(MAP_PATH, req.GET["map_name"], 
                "new_nav_{path}.csv".format(path=req.GET["path_name"].encode("utf-8")))
            if not os.path.exists(target_path):
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "target path not found"
                }, indent=4), content_type="application/json", status=404)
            with open(target_path) as path_file:
                return HttpResponse(json.dumps({
                    "map_name": req.GET["map_name"],
                    "path_name": req.GET["path_name"],
                    "path_data": path_file.read()
                }, indent=4), content_type="application/json")
        else:
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "map_name and path_name are required"
            }, indent=4), content_type="application/json", status=400)
    if req.method == "POST" or req.method == "PUT":
        req_data = {}
        try:
            req_data = json.loads(req.body.decode("utf-8"))
        except:
            pass
        if "map_name" not in req_data or "path_name" not in req_data or "path_data" not in req_data:
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "map_name, path_name, path_data are required"
            }, indent=4), content_type="application/json", status=400)
        if not os.path.exists(os.path.join(MAP_PATH, req_data["map_name"])):
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "target map not exist"
            }, indent=4), content_type="application/json", status=400)
        target_path = os.path.join(MAP_PATH, req_data["map_name"], 
            "new_nav_{path}.csv".format(path=req_data["path_name"].encode("utf-8")))
        with open(target_path, "w+") as target_path_file:
            target_path_file.write(req_data["path_data"])
        return HttpResponse(json.dumps({
            "map_name": req_data["map_name"],
            "path_name": req_data["path_name"],
            "path_data": req_data["path_data"]
        }, indent=4), content_type="application/json")

    if req.method == "DELETE":
        if "map_name" not in req.GET or "path_name" not in req.GET:
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "map_name and path_name are required"
            }, indent=4), content_type="application/json", status=400)
        target_path = os.path.join(MAP_PATH, req_data["map_name"], 
            "new_nav_{path}.csv".format(path=req_data["path_name"].encode("utf-8")))
        if not os.path.exists(target_path):
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "target nav file not found"
            }, indent=4), content_type="application/json", status=400)
        os.remove(target_path)
        return HttpResponse(json.dumps({
            "status": "ok"
        }, indent=4), content_type="application/json")
        

@csrf_exempt
@token_required
@allowed_methods(["GET"])
def switch_map(req):
    if "map_name" not in req.GET:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "map_name is required"
        }, indent=4), content_type="application/json", status=400)
    target_map_path = os.path.join(MAP_PATH, req.GET["map_name"])
    if not os.path.exists(target_map_path):
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "target map not exist"
        }, indent=4), content_type="application/json", status=400)
    # 检查当前地图和目标地图是否一致
    map_name = ""
    if os.path.exists(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")):
        with open(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")) as map_name_file:
            map_name = map_name_file.read().strip()
    if map_name ==  req.GET["map_name"].encode("utf-8"):
        return HttpResponse(json.dumps({
            "status": "ok"
        }, indent=4), content_type="application/json")
    # 删除老地图
    res = subprocess.Popen("rm -rf {map_workspace}/* ".format(map_workspace=MAP_WORKSPACE_PATH),
                        universal_newlines=True,
                        shell=True,
                        stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
                        )
    if res.wait() != 0:
        rospy.logerr(res.stdout.read())
        return HttpResponse(json.dumps({
            "result": False,
            "description": "switch map failed",
        }, indent=4), content_type="application/json", status=500)

    res = subprocess.Popen("cp -rfp {map_path}/{map_name}/* {map_workspace}/ ".format(map_path=MAP_PATH, map_name=req.GET["map_name"].encode("utf-8"), map_workspace=MAP_WORKSPACE_PATH),
                        universal_newlines=True,
                        shell=True,
                        stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
                        )
    if res.wait() != 0:
        rospy.logerr(res.stdout.read())
        return HttpResponse(json.dumps({
            "result": False,
            "description": "switch map failed",
        }, indent=4), content_type="application/json", status=500)
    return HttpResponse(json.dumps({
        "status": "ok"
    }, indent=4), content_type="application/json")
