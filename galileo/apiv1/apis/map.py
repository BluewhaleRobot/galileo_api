#!/usr/bin/env python
# encoding=utf-8

"""controller for map"""
import subprocess
import json
import os
import time
from io import BytesIO
import rospy
from PIL import Image
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from nav_msgs.msg import OccupancyGrid, MapMetaData
from galileo_serial_server.msg import GalileoStatus, GalileoNativeCmds
import base.core
from base.ros_connector import get_topic_data, set_topic_data
from apiv1.decorator import token_required, allowed_status
from utils.decorator import required_keys, allowed_methods
from utils.views import proxy_view
from galileo.settings import TIMEOUT, MAP_PATH, MAP_WORKSPACE_PATH



@token_required
@allowed_methods(["GET"])
@allowed_status(["Free"])
def start(req):
    """start mapping process"""
    with base.core.STATUS_LOCK:
        base.core.SYS_STATUS = "Busy"
    # 设置 camera_id 参数，表示使用哪个摄像头进行建图。0为前摄像头 1为后摄像头，-1为默认摄像头
    if "camera_id" in req.GET:
        rospy.set_param("/remote_server/camera_id", int(req.GET["camera_id"]))
    else:
        rospy.set_param("/remote_server/camera_id", -1)
    # 发送开启建图指令
    galileo_cmds = GalileoNativeCmds()
    galileo_cmds.data = 'V' + chr(0x00)
    galileo_cmds.length = len(galileo_cmds.data)
    set_topic_data('/galileo/cmds', GalileoNativeCmds, galileo_cmds)
    timecount = 0
    while timecount < TIMEOUT:
        time.sleep(1)
        timecount += 1
        data = get_topic_data("/galileo/status", GalileoStatus)
        if data is None:
            continue
        if data is not None and "hz" in data and data["hz"] != 0:
            if data['data'].mapStatus == 1:
                break
    if timecount >= TIMEOUT:
        # 发送关闭建图指令
        galileo_cmds.data = 'V' + chr(0x01)
        galileo_cmds.length = len(galileo_cmds.data)
        galileo_cmds.header.stamp = rospy.Time.now()
        set_topic_data('/galileo/cmds', GalileoNativeCmds, galileo_cmds)
        with base.core.STATUS_LOCK:
            base.core.SYS_STATUS = "Free"
        return HttpResponse(json.dumps({
            "result": False,
            "description": "start map failed",
        }, indent=4), content_type="application/json", status=500)
    with base.core.STATUS_LOCK:
        base.core.SYS_STATUS = "Mapping"
    return HttpResponse(json.dumps({
        "result": True,
    }, indent=4), content_type="application/json")


@token_required
@allowed_methods(["GET"])
@allowed_status(["Mapping"])
def stop(_req):
    """stop mapping process"""
    with base.core.STATUS_LOCK:
        base.core.SYS_STATUS = "Busy"
    galileo_cmds = GalileoNativeCmds()
    galileo_cmds.data = 'V' + chr(0x01)
    galileo_cmds.length = len(galileo_cmds.data)
    galileo_cmds.header.stamp = rospy.Time.now()
    set_topic_data('/galileo/cmds', GalileoNativeCmds, galileo_cmds)
    timecount = 0
    while timecount < TIMEOUT:
        time.sleep(1)
        timecount += 1
        data = get_topic_data("/galileo/status", GalileoStatus)
        if data is None:
            continue
        if data is not None and "hz" in data and data["hz"] != 0:
            if data['data'].mapStatus == 0:
                break
    if timecount >= TIMEOUT:
        return HttpResponse(json.dumps({
            "result": False,
            "description": "stop mapping timeout"
        }, indent=4), content_type="application/json")
    with base.core.STATUS_LOCK:
        base.core.SYS_STATUS = "Free"
    # 设置默认为前摄像头
    rospy.set_param("/remote_server/camera_id", -1)
    return HttpResponse(json.dumps({
        "result": True,
    }, indent=4), content_type="application/json")


@token_required
@allowed_methods(["GET"])
@allowed_status(["Free"])
def update(req):
    if "map" not in req.GET:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "map is required in update map"
        }, indent=4), content_type="application/json")
    with base.core.STATUS_LOCK:
        base.core.SYS_STATUS = "Busy"
    # 检查地图是否存在
    # 检查目标地图和路径是否存在
    if not os.path.exists(os.path.join(MAP_PATH, req.GET["map"])):
        with base.core.STATUS_LOCK:
            base.core.SYS_STATUS = "Free"
        return HttpResponse(json.dumps({
            "result": False,
            "description": "target map does not exist",
        }, indent=4), content_type="application/json", status=404)
    
    # 设置 camera_id 参数，表示使用哪个摄像头进行建图。0为前摄像头 1为后摄像头，-1为默认摄像头
    if "camera_id" in req.GET:
        rospy.set_param("/remote_server/camera_id", int(req.GET["camera_id"]))
    else:
        rospy.set_param("/remote_server/camera_id", -1)
    # 发送开启更新地图指令
    galileo_cmds = GalileoNativeCmds()
    galileo_cmds.data = 'V' + chr(0x03)
    galileo_cmds.length = len(galileo_cmds.data)
    set_topic_data('/galileo/cmds', GalileoNativeCmds, galileo_cmds)
    timecount = 0
    while timecount < TIMEOUT:
        time.sleep(1)
        timecount += 1
        data = get_topic_data("/galileo/status", GalileoStatus)
        if data is None:
            continue
        if data is not None and "hz" in data and data["hz"] != 0:
            if data['data'].mapStatus == 1:
                break
    if timecount >= TIMEOUT:
        # 发送关闭建图指令
        galileo_cmds.data = 'V' + chr(0x01)
        galileo_cmds.length = len(galileo_cmds.data)
        galileo_cmds.header.stamp = rospy.Time.now()
        set_topic_data('/galileo/cmds', GalileoNativeCmds, galileo_cmds)
        with base.core.STATUS_LOCK:
            base.core.SYS_STATUS = "Free"
        return HttpResponse(json.dumps({
            "result": False,
            "description": "start update map failed",
        }, indent=4), content_type="application/json", status=500)
    with base.core.STATUS_LOCK:
        base.core.SYS_STATUS = "Mapping"
    return HttpResponse(json.dumps({
        "result": True,
    }, indent=4), content_type="application/json")


@token_required
@allowed_methods(["GET"])
@allowed_status(["Mapping"])
def pose(_req):
    """get current robot pose"""
    data = get_topic_data("/galileo/status", GalileoStatus)
    res = {
        "x": 0,
        "y": 0,
        "angle": 0,
    }
    if data is not None:
        res["x"] = data["data"].currentPosX
        res["y"] = data["data"].currentPosY
        res["angle"] = data["data"].currentAngle
    return HttpResponse(json.dumps(
        res, indent=4), content_type="application/json")


@token_required
@allowed_methods(["GET", "POST"])
@allowed_status(["Mapping"])
def get_current_map_png(_req):
    """获取当前正在创建的地图图片"""
    map_data = get_topic_data("/galileo/map/current_map", OccupancyGrid)
    if map_data is None:
        response = None
        with BytesIO() as output:
            image = Image.new("RGB", (255, 255))
            image.save(output, format="PNG")
            response = HttpResponse(
                output.getvalue(), content_type="image/png")
            response['Content-Disposition'] = \
                'attachment; filename=current_map.png'
        return response
    image_buffer = []
    map_data = map_data["data"]
    for y in range(0, map_data.info.height):
        for x in range(0, map_data.info.width):
            i = x + (map_data.info.height - y - 1) * map_data.info.width
            if map_data.data[i] >= 0 and map_data.data[i] <= 100:
                image_buffer.append(int((100 - map_data.data[i]) * 255 / 100))
            else:
                image_buffer.append(int(128))
    image = Image.frombuffer("L", (map_data.info.width, map_data.info.height),
                             bytearray(image_buffer), "raw", "L", 0, 1)
    output = BytesIO()
    image.save(output, format="PNG")
    response = HttpResponse(output.getvalue(), content_type="image/png")
    response['Content-Disposition'] = 'attachment; filename=current_map.png'
    return response


@csrf_exempt
@token_required
@allowed_methods(["GET", "POST"])
@allowed_status(["Mapping"])
def current_map(req):
    """获取当前正在创建的地图，包含图片和说明文件"""
    if req.method == "GET":
        map_header_data = get_topic_data("/galileo/map/current_map_meta",
                                         MapMetaData)
        if map_header_data is None:
            return HttpResponse(json.dumps({
                "status": "OK",
                "map_params": {},
                "map_image": "",
            }, indent=4), content_type="application/json")
        map_header_data = map_header_data["data"]
        return HttpResponse(json.dumps({
            "status": "OK",
            "map_params": {
                "resolution": map_header_data.resolution,
                "origin": [
                    map_header_data.origin.position.x,
                    map_header_data.origin.position.y,
                    map_header_data.origin.position.z
                ],
                "width": map_header_data.width,
                "height": map_header_data.height,
            },
            "map_image": "/api/v1/map/current_map_png",

        }, indent=4), content_type="application/json")
    if req.method == "POST":
        with base.core.STATUS_LOCK:
            base.core.SYS_STATUS = "Busy"
        req_data = json.loads(req.body.decode("utf-8"))
        if "name" not in req_data:
            with base.core.STATUS_LOCK:
                base.core.SYS_STATUS = "Mapping"
            return HttpResponse(json.dumps({
                "result": False,
                "description": "map name is required"
            }, indent=4), content_type="application/json", status=400)
        # 检查是否已经重名
        if os.path.exists(os.path.join(MAP_PATH, req_data["name"].encode("utf-8"))):
            # return to map state
            with base.core.STATUS_LOCK:
                base.core.SYS_STATUS = "Mapping"
            return HttpResponse(json.dumps({
                "result": False,
                "descrpition": "map {name} already exist, please set another name"
                .format(name=req_data["name"].encode("utf-8"))
            }, indent=4), content_type="application/json", status=400)
        if os.path.exists(os.path.join(MAP_WORKSPACE_PATH, "save_complete")):
            os.remove(os.path.join(MAP_WORKSPACE_PATH, "save_complete"))
        galileo_cmds = GalileoNativeCmds()
        galileo_cmds.data = 'V' + chr(0x02)
        galileo_cmds.header.stamp = rospy.Time.now()
        set_topic_data('/galileo/cmds', GalileoNativeCmds, galileo_cmds)
        timecout = 0
        while timecout < TIMEOUT * 10:
            if os.path.exists(os.path.join(MAP_WORKSPACE_PATH, "save_complete")):
                break
            time.sleep(1)
            timecout += 1
        if timecout >= TIMEOUT * 10:
            # 保存超时, 暂不处理
            with base.core.STATUS_LOCK:
                base.core.SYS_STATUS = "Mapping"
            return HttpResponse(json.dumps({
                "result": False,
                "name": "map {name} save timeout {timeout}s"
                .format(name=req_data["name"].encode("utf-8"), timeout=TIMEOUT * 10)
            }, indent=4), content_type="application/json", status=500)
        # 保存当前地图至saved-slamdb
        res = subprocess.Popen(["mkdir", "-p", os.path.join(MAP_PATH, req_data["name"])],
                               universal_newlines=True,
                               stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        if res.wait() != 0:
            with base.core.STATUS_LOCK:
                base.core.SYS_STATUS = "Mapping"
            rospy.logerr(res.stdout.read())
            return HttpResponse(json.dumps({
                "result": False,
                "name": "create map directory failed"
                .format(name=req_data["name"].encode("utf-8"), timeout=TIMEOUT * 10, status=500)
            }, indent=4), content_type="application/json")
        
        res = subprocess.Popen("echo {name} > {map_name}".format(
                name=req_data["name"].encode("utf-8"),
                map_name=os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")
            ),
            universal_newlines=True,
            shell=True,
            stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        if res.wait() != 0:
            with base.core.STATUS_LOCK:
                base.core.SYS_STATUS = "Mapping"
            return HttpResponse(json.dumps({
                "result": False,
                "name": "store map name failed"
                .format(name=req_data["name"].encode("utf-8"), timeout=TIMEOUT * 10)
            }, indent=4), content_type="application/json", status=500)

        res = subprocess.Popen(' '.join(["cp", "-rp", MAP_WORKSPACE_PATH + "/*", os.path.join(MAP_PATH, req_data["name"].encode("utf-8")) + "/"]),
                               universal_newlines=True,
                               shell=True,
                               stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        if res.wait() != 0:
            with base.core.STATUS_LOCK:
                base.core.SYS_STATUS = "Mapping"
            rospy.logerr(res.stdout.read())
            return HttpResponse(json.dumps({
                "result": False,
                "name": "copy map directory failed"
                .format(name=req_data["name"].encode("utf-8"), timeout=TIMEOUT * 10)
            }, indent=4), content_type="application/json", status=500)

        # return to map state
        with base.core.STATUS_LOCK:
            base.core.SYS_STATUS = "Mapping"

        return HttpResponse(json.dumps({
            "result": True,
            "name": req_data["name"].encode("utf-8")
        }, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def get_map_tracks(_req):
    data = None
    with open(os.path.join(MAP_WORKSPACE_PATH, "RobotTrajectory.txt")) as traj_file:
        data = traj_file.read()
    if data is None:
        return HttpResponse(json.dumps({
            "trajectory": []
        }, indent=4), content_type="application/json")
    data = data.split('\n')
    points_list = [
        {
            "x": float(line.split(' ')[1]),
            "y": float(line.split(' ')[2]),
            "z": float(line.split(' ')[3])
        } for line in data if len(line.split(' ')) >= 8]
    return HttpResponse(json.dumps({
        "trajectory": points_list,
    }, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET", "PUT"])
def map_images(req):
    if req.method == "GET":
        if "name" in req.GET:
            if not os.path.exists(os.path.join(MAP_PATH, req.GET["name"], 'map.pgm')):
                return HttpResponse(json.dumps({
                    "result": False,
                    "description": "target map not found"
                }, indent=4), content_type="application/json",
                    status=404)
            with open(os.path.join(MAP_PATH, req.GET["name"], 'map.pgm')) as map_file:
                response = HttpResponse(
                    map_file.read(), content_type="image/pgm")
                response['Content-Disposition'] = 'attachment; filename=map.pgm'
                return response
        else:
            # list all maps
            return HttpResponse(json.dumps(os.listdir(MAP_PATH), indent=4),
                                content_type="application/json")
    if req.method == "PUT":
        # rename map images
        req_data = {}
        try:
            req_data = json.loads(req.body.decode("utf-8"))
        except Exception:
            req_data = []
        if "name" not in req_data or "new_name" not in req_data:
            return HttpResponse(json.dumps({
                "result": False,
                "description": "name and new_name are required",
            }, indent=4), content_type="application/json")
        res = subprocess.Popen(["mv", os.path.join(MAP_PATH, req_data["name"]), os.path.join(MAP_PATH, req_data["new_name"])],
                               universal_newlines=True,
                               stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        if res.wait() != 0:
            return HttpResponse(json.dumps({
                "result": False,
                "description": "rename directory failed",
            }, indent=4), content_type="application/json")

        return HttpResponse(json.dumps({
            "result": True,
            "name": req_data["new_name"],
        }, indent=4),
            content_type="application/json")


@csrf_exempt
@token_required
@allowed_status(["Mapping"])
@allowed_methods(["GET"])
def stream(req):
    topic = rospy.get_param("/galileo/galileo_mapping_topic")
    topic_list = rospy.get_published_topics()
    if len([topic_info for topic_info in topic_list
            if topic_info[0] == topic]) == 0:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "target topic not found"
        }, indent=4), content_type="application/json", status=404)
    return proxy_view(req,
                      "http://127.0.0.1:8080/stream?topic={topic}"
                      .format(topic=topic))
