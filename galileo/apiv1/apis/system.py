#!/usr/bin/env python
# encoding=utf-8

"""
controller for system
"""
import os
import time
import uuid
import json
import hashlib
import subprocess
import requests
import rospy
import rosparam
import rosservice
import dynamic_reconfigure.client
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.test import Client
from std_msgs.msg import Float64, Bool, String, Header, Int32
from nav_msgs.msg import Odometry
try:
    from xqserial_server.srv import Shutdown, ShutdownRequest, ShutdownResponse
except Exception:
    pass
from geometry_msgs.msg import Pose2D, Twist
from bwbot_check.check import get_system_status
from sensor_msgs.msg import Image, CameraInfo
from apiv1.decorator import token_required, allowed_status
from apiv1.models import TOKEN_DB, CONFIG_DB
from utils.decorator import required_keys, allowed_methods
from utils.utils import get_my_id, verify, byte_to_hex, read_yaml, write_yaml, get_config_from_param_server
from utils.views import proxy_view
from galileo.settings import BASE_DIR, TEST, BATTERY_LOW, BATTERY_HIGH, GALILEO_SETTING_PATH, MAP_WORKSPACE_PATH, \
    UPDATE_CHECK_CMD, UPDATE_CMD, UPDATE_LOG_PATH, UPDATE_FLAG_PATH, USER_CONFIG, VERSION, CODE_NAME, SERVICE_PORT
from base.ros_connector import get_topic_data, set_topic_data
from base.core import SCHEDULE_MANAGER, SCHEDULE_MANAGER_LOCK
from base.signals import navigation_started
import base.core
from django.dispatch import receiver
from bson import json_util
from base.management.commands import SHUTDOWN_SINGAL
from utils.utils import get_my_id
from getmac import get_mac_address


@csrf_exempt
@required_keys(["username", "password"])
def token(req):
    """get token"""
    req_data = req.GET
    # 检查用户名和密码
    if USER_CONFIG["username"] != req_data["username"] or \
            USER_CONFIG["password"] != req_data["password"]:
        return HttpResponse(json.dumps({
            "result": False,
            "description": "username or password error",
        }, indent=4), content_type="application/json", status=403)
    new_token_str = uuid.uuid1().hex
    if TEST:
        TOKEN_DB.insert({
            "token": new_token_str,
            "username": req_data["username"],
            "password": req_data["password"],
            "device_id": req_data["device_id"],
        })
    else:
        TOKEN_DB.insert({
            "token": new_token_str,
            "username": req_data["username"],
            "password": req_data["password"],
        })
    return HttpResponse(json.dumps({
        "result": True,
        "token": new_token_str,
    }, indent=4), content_type="application/json")


@csrf_exempt
@token_required
def status(_req):
    """get system status"""
    with base.core.STATUS_LOCK:
        return HttpResponse(json.dumps({
            "status": base.core.SYS_STATUS,
        }, indent=4), content_type="application/json")


@csrf_exempt
@token_required
def info(_req):
    """get system info"""
    res = {}
    data = get_topic_data("/galileo/battery/power", Float64)
    if data is not None:
        res["battery"] = int((data["data"].data - BATTERY_LOW)
                             / (BATTERY_HIGH - BATTERY_LOW) * 100 / 0.8 ) # 上移0.8, 防止充电后电压显示下降
        if res["battery"] > 100:
            res["battery"] = 100
        if res["battery"] < 0:
            res["battery"] = 0
    else:
        res["battery"] = 0
    data = get_topic_data("/galileo/camera/camera_info", CameraInfo)
    if data is not None:
        res["camera_rgb"] = data["hz"]
    else:
        res["camera_rgb"] = 0
    data = get_topic_data("/galileo/camera/depth/info", Int32)
    if data is not None:
        res["camera_depth"] = data["hz"]
    else:
        res["camera_depth"] = 0
    data = get_topic_data("/galileo/imu/odom", Odometry)
    if data is not None:
        res["odom"] = data["hz"]
    else:
        res["odom"] = 0
    data = get_topic_data("/galileo/imu/pose_2d", Pose2D)
    if data is not None:
        res["imu"] = data["hz"]
    else:
        res["imu"] = 0
    data = get_topic_data("/galileo/camera/processed/info", Int32)
    if data is not None:
        res["camera_processed"] = data["hz"]
    else:
        res["camera_processed"] = 0
    data = get_topic_data("/xqserial_server/StatusFlag", Int32)
    if data is not None:
        res["driver_port"] = (data["data"].data == 2)
    else:
        res["driver_port"] = False
    res["info"] = {
        "version": VERSION,
        "code_name": CODE_NAME,
        "id": get_my_id(),
        "mac": get_mac_address(),
        "port": SERVICE_PORT
    }
    return HttpResponse(json.dumps(res, indent=4),
                        content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET", "PUT"])
def speed(req):
    """currnet speed"""
    if req.method == "GET":
        data = get_topic_data("/galileo/imu/odom", Odometry)
        if data is None:
            return HttpResponse(json.dumps({
                "speed_x": 0,
                "speed_y": 0,
                "speed_angle": 0,
            }, indent=4), content_type="application/json")
        else:
            odom = data["data"]
            return HttpResponse(json.dumps({
                "speed_x": odom.twist.twist.linear.x,
                "speed_y": odom.twist.twist.linear.y,
                "speed_angle": odom.twist.twist.angular.z,
            }, indent=4), content_type="application/json")
    if req.method == "PUT":
        req_data = {}
        try:
            req_data = json.loads(req.body.decode("utf-8"))
        except Exception as e:
            rospy.logerr(e)
        if "speed_x" not in req_data or "speed_y" not in req_data\
                or "speed_angle" not in req_data:
            return HttpResponse(json.dumps({
                "result": False,
                "description": "speed_x, speed_y, speed_angle are required keys"
            }, indent=4), content_type="application/json", status=400)
        cmd = Twist()
        cmd.linear.x = req_data["speed_x"]
        cmd.linear.y = req_data["speed_y"]
        cmd.angular.z = req_data["speed_angle"]
        set_topic_data("/cmd_vel", Twist, cmd)
        return HttpResponse(json.dumps({
            "result": True,
        }, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def camera_rgb(req):
    topic = rospy.get_param("/galileo/galileo_image_topic")
    topic_list = rospy.get_published_topics()
    if len([topic_info for topic_info in topic_list
            if topic_info[0] == topic]) == 0:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "target topic not found"
        }, indent=4), content_type="application/json", status=404)
    return proxy_view(req,
                      "http://127.0.0.1:8080/stream?topic={topic}&type=vp8"
                      .format(topic=topic))


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def camera_rgb_processed(req):
    topic = rospy.get_param("/galileo/galileo_image_topic_processed")
    topic_list = rospy.get_published_topics()
    if len([topic_info for topic_info in topic_list
            if topic_info[0] == topic]) == 0:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "target topic not found"
        }, indent=4), content_type="application/json", status=404)
    return proxy_view(req,
                      "http://127.0.0.1:8080/stream?topic={topic}&type=vp8"
                      .format(topic=topic))


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def log(req):
    # the last 10 lines from log file
    res = subprocess.Popen(
        ['tail', os.path.join(BASE_DIR, 'galileo.log')],
        universal_newlines=True,
        stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    res.wait()
    return HttpResponse(json.dumps({
        'result': 'OK',
        'log': res.stdout.read(),
    }, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def restart(req):
    # SHUTDOWN_SINGAL.send('system')
    # time.sleep(2)
    # print("#################1")
    # res = subprocess.Popen(
    #     # ['sleep', '5', '&&', 'echo', '###################'],
    #     "sleep 10; roslaunch galileo start.launch 2> a.log",
    #     universal_newlines=True,
    #     shell=True,
    #     stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
    # )
    # print("#################2")
    # os._exit(0)
    # print("#################3")
    return HttpResponse(json.dumps({
        "result": True,
    }, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def shutdown(req):
    audio_msg = String()
    audio_msg.data = "请等待一分钟后，再切断总电源，谢谢！"
    set_topic_data("/xiaoqiang_tts/text", String, audio_msg)
    # 等待语音播放完毕
    time.sleep(5)
    if rosservice.get_service_node("/motor_driver/shutdown") is not None:
        # call shutdown service
        rospy.wait_for_service('/motor_driver/shutdown')
        shutdown_rpc = rospy.ServiceProxy("/motor_driver/shutdown", Shutdown)
        req = ShutdownRequest()
        req.flag = True
        shutdown_rpc(req)
    subprocess.Popen('sudo shutdown -h now', shell=True)
    return HttpResponse(json.dumps({
        "result": True,
    }, indent=4), content_type="application/json")

@csrf_exempt
@token_required
@allowed_methods(["GET"])
def calibrate_imu(req):
    cmd = Bool()
    cmd.data = True
    set_topic_data("/galileo/calibate_imu", Bool, cmd)
    return HttpResponse(json.dumps({
        "result": "OK",
    }, indent=4), content_type="application/json")


CALIB_STATUS = "FREE"
CURRENT_CAMERA_ID = -1

@csrf_exempt
@token_required
@allowed_methods(["GET"])
@allowed_status(["Free"])
def calibrate_camera_start(req):
    global CALIB_STATUS, CURRENT_CAMERA_ID
    if CALIB_STATUS != "FREE":
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "status not free"
        }, indent=4), content_type="application/json", status=400)
    CALIB_STATUS = "CALIBRATING"
    if "camera_id" in req.GET and int(req.GET["camera_id"]) >= 0:
        CURRENT_CAMERA_ID = int(req.GET["camera_id"])
        if CURRENT_CAMERA_ID > 1:
            CALIB_STATUS = "FREE"
            CURRENT_CAMERA_ID = -1
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "only support two cameras max"
            }, indent=4), content_type="application/json", status=400)
        # 检查是否有两个摄像头
        if not os.path.exists(os.path.join(GALILEO_SETTING_PATH, "setting4_front.yaml")) or \
            not os.path.exists(os.path.join(GALILEO_SETTING_PATH, "setting4_back.yaml")):
            CALIB_STATUS = "FREE"
            CURRENT_CAMERA_ID = -1
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "no camera config file found"
            }, indent=4), content_type="application/json", status=400)
        if CURRENT_CAMERA_ID == 0:
            # 标定前摄像头
            slam_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4_front.yaml"))
            slam_config["EnableCalib"] = 1
            write_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4_front.yaml"), slam_config)
        if CURRENT_CAMERA_ID == 1:
            # 标定后摄像头
            slam_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4_back.yaml"))
            slam_config["EnableCalib"] = 1
            write_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4_back.yaml"), slam_config)
    else:
        # 标定默认单摄像头
        CURRENT_CAMERA_ID = -1
        slam_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4.yaml"))
        slam_config["EnableCalib"] = 1
        write_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4.yaml"), slam_config)
    # 删除旧TbaMatrix
    if os.path.exists(os.path.join(MAP_WORKSPACE_PATH, "TbaMatrix.yaml")):
        os.remove(os.path.join(MAP_WORKSPACE_PATH, "TbaMatrix.yaml"))
    # 进入开启建图程序
    client = Client()
    token = ""
    if "token" in req.GET:
        token = req.GET["token"]
    if CURRENT_CAMERA_ID == -1:
        res = client.get("/api/v1/map/start?token=" + token)
    else:
        res = client.get("/api/v1/map/start?camera_id={camera_id}&token={token}".format(
            camera_id=CURRENT_CAMERA_ID,
            token=token
        ))
    if res.status_code != 200:
        rospy.logerr(res.content.decode("utf-8"))
        CALIB_STATUS = "FREE"
        if CURRENT_CAMERA_ID == -1:
            slam_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4.yaml"))
            slam_config["EnableCalib"] = 0
            write_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4.yaml"), slam_config)
        elif CURRENT_CAMERA_ID == 0:
            slam_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4_front.yaml"))
            slam_config["EnableCalib"] = 0
            write_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4_front.yaml"), slam_config)
        elif CURRENT_CAMERA_ID == 1:
            slam_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4_back.yaml"))
            slam_config["EnableCalib"] = 0
            write_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4_back.yaml"), slam_config)

        CURRENT_CAMERA_ID = -1
        
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "start map failed"
        }, indent=4), content_type="application/json", status=400)
    return HttpResponse(json.dumps({
        "status": "ok",
        "description": "start calibration success"
    }, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def calibrate_camera_complete(req):
    global CALIB_STATUS, CURRENT_CAMERA_ID
    if CALIB_STATUS != "CALIBRATED":
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "status not in calibrated"
        }, indent=4), content_type="application/json", status=400)
    # 获取标定参数
    calib_data = read_yaml(os.path.join(MAP_WORKSPACE_PATH, "TbaMatrix.yaml"))
    # 将标定参数写入配置文件
    # setting4为建图配置文件，setting5为导航配置文件, setting-update为更新地图配置文件
    if CURRENT_CAMERA_ID == -1:
        # 设置 setting4
        slam_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4.yaml"))
        slam_config["TbaMatrix"] = calib_data["tba"]
        slam_config["EnableCalib"] = 0
        write_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4.yaml"), slam_config)
        # 设置 setting5
        slam_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting5.yaml"))
        slam_config["TbaMatrix"] = calib_data["tba"]
        write_yaml(os.path.join(GALILEO_SETTING_PATH, "setting5.yaml"), slam_config)
    if CURRENT_CAMERA_ID == 0:
        # 设置 setting4
        slam_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4.yaml"))
        slam_config["TbaMatrix"] = calib_data["tba"]
        slam_config["EnableCalib"] = 0
        write_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4.yaml"), slam_config)
        # 设置 setting 5
        slam_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting5.yaml"))
        slam_config["TbaMatrix"] = calib_data["tba"]
        write_yaml(os.path.join(GALILEO_SETTING_PATH, "setting5.yaml"), slam_config)
        # 设置 setting4_front
        slam_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4_front.yaml"))
        slam_config["TbaMatrix"] = calib_data["tba"]
        slam_config["EnableCalib"] = 0
        write_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4_front.yaml"), slam_config)
        # 设置 setting5_multi
        slam_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting5_multi.yaml"))
        slam_config["TbaMatrix"] = calib_data["tba"]
        write_yaml(os.path.join(GALILEO_SETTING_PATH, "setting5_multi.yaml"), slam_config)
    if CURRENT_CAMERA_ID == 1:
        # 设置 setting4_back
        slam_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4_back.yaml"))
        slam_config["TbaMatrix"] = calib_data["tba"]
        slam_config["EnableCalib"] = 0
        write_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4_back.yaml"), slam_config)
        # 设置 setting5_multi
        slam_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting5_multi.yaml"))
        slam_config["Tbc2Matrix"] = calib_data["tba"]
        write_yaml(os.path.join(GALILEO_SETTING_PATH, "setting5_multi.yaml"), slam_config)
    slam_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting-update.yaml"))
    slam_config["TbaMatrix"] = calib_data["tba"]
    if CURRENT_CAMERA_ID != -1:
        # 从多摄像头配置中读取前摄像头
        multi_camera_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting5_multi.yaml"))
        slam_config["TbaMatrix"] = multi_camera_config["TbaMatrix"]
    write_yaml(os.path.join(GALILEO_SETTING_PATH, "setting-update.yaml"), slam_config)
    # 停止建图
    client = Client()
    token = ""
    if "token" in req.GET:
        token = req.GET["token"]
    res = client.get("/api/v1/map/stop?token=" + token)
    if res.status_code != 200:
        rospy.logerr(res.content.decode("utf-8"))
        CALIB_STATUS = "FREE"
        CURRENT_CAMERA_ID = -1
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "stop mapping failed"
        }, indent=4), content_type="application/json", status=400)
    CALIB_STATUS = "FREE"
    CURRENT_CAMERA_ID = -1
    return HttpResponse(json.dumps({
        "status": "ok",
        "description": "stop calibration success"
    }, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def calibrate_camera_cancel(req):
    global CALIB_STATUS, CURRENT_CAMERA_ID
    if CALIB_STATUS != "CALIBRATING" and CALIB_STATUS != "CALIBRATED":
        return HttpResponse(json.dumps({
            "status": "error",
            "desription": "status not in calibrating or calibrated state"
        }, indent=4), content_type="application/json", status=400)
    # 设置关闭导航参数
    if CURRENT_CAMERA_ID == -1:
        slam_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4.yaml"))
        slam_config["EnableCalib"] = 0
        write_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4.yaml"), slam_config)
    elif CURRENT_CAMERA_ID == 0:
        slam_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4_front.yaml"))
        slam_config["EnableCalib"] = 0
        write_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4_front.yaml"), slam_config)
    elif CURRENT_CAMERA_ID == 1:
        slam_config = read_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4_back.yaml"))
        slam_config["EnableCalib"] = 0
        write_yaml(os.path.join(GALILEO_SETTING_PATH, "setting4_back.yaml"), slam_config)
    # 停止建图
    client = Client()
    token = ""
    if "token" in req.GET:
        token = req.GET["token"]
    res = client.get("/api/v1/map/stop?token=" + token)
    if res.status_code != 200:
        rospy.logerr(res.content.decode("utf-8"))
        CALIB_STATUS = "FREE"
        CURRENT_CAMERA_ID = -1
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "stop mapping failed"
        }, indent=4), content_type="application/json", status=400)
    CALIB_STATUS = "FREE"
    CURRENT_CAMERA_ID = -1
    return HttpResponse(json.dumps({
        "status": "ok",
        "description": "cancel calibrating success"
    }, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def calibrate_camera_status(req):
    global CALIB_STATUS
    if CALIB_STATUS == "CALIBRATING":
        if os.path.exists(os.path.join(MAP_WORKSPACE_PATH, "TbaMatrix.yaml")):
            CALIB_STATUS = "CALIBRATED"
    if CALIB_STATUS == "CALIBRATED":
        calib_data = read_yaml(os.path.join(MAP_WORKSPACE_PATH, "TbaMatrix.yaml"))
        return HttpResponse(json.dumps({
            "status": CALIB_STATUS,
            "accuracy": 100 - abs((calib_data["visual_distance"] - calib_data["odom_distance"]) / calib_data["odom_distance"] * 100)
        }, indent=4), content_type="application/json")
    return HttpResponse(json.dumps({
        "status": CALIB_STATUS
    }, indent=4), content_type="application/json")

UPDATE_PROCESS = None

@csrf_exempt
@token_required
@allowed_methods(["GET"])
def update_check(req):
    if not USER_CONFIG["allow_update"]:
        return HttpResponse(json.dumps({
            "status": "ok",
            "need_update": False,
        }, indent=4), content_type="application/json")
    global UPDATE_PROCESS
    if UPDATE_PROCESS is not None and UPDATE_PROCESS.poll() is None:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "update process is running, please try latter"
        }, indent=4), content_type="application/json", status=400)
    UPDATE_PROCESS = subprocess.Popen(UPDATE_CHECK_CMD, universal_newlines=True, shell=True,
        stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    UPDATE_PROCESS.wait()
    if UPDATE_PROCESS.returncode != 0:
        UPDATE_PROCESS = None
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "check update failed"
        }, indent=4), content_type="application/json", status=400)
    UPDATE_PROCESS = None
    if os.path.exists(UPDATE_FLAG_PATH):
        return HttpResponse(json.dumps({
            "status": "ok",
            "need_update": True,
        }, indent=4), content_type="application/json")
    else:
        return HttpResponse(json.dumps({
            "status": "ok",
            "need_update": False,
        }, indent=4), content_type="application/json")
    


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def update_start(req):
    if not USER_CONFIG["allow_update"]:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "update is not allowed by user config"
        }, indent=4), content_type="application/json", status=400)
    global UPDATE_PROCESS
    if UPDATE_PROCESS is not None and UPDATE_PROCESS.poll() is None:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "update process is running, please try latter"
        }, indent=4), content_type="application/json", status=400)
    UPDATE_PROCESS = subprocess.Popen("unbuffer " + UPDATE_CMD + " > " + UPDATE_LOG_PATH, universal_newlines=True, shell=True,
        stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    return HttpResponse(json.dumps({
        "status": "ok",
        "description": "update process running in background"
    }, indent=4), content_type="application/json")

@csrf_exempt
@token_required
@allowed_methods(["GET"])
def update_cancel(req):
    if not USER_CONFIG["allow_update"]:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "update is not allowed by user config"
        }, indent=4), content_type="application/json", status=400)
    global UPDATE_PROCESS
    if UPDATE_PROCESS is None or UPDATE_PROCESS.poll() is not None:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "update process not running, please try latter"
        }, indent=4), content_type="application/json", status=400)
    UPDATE_PROCESS.kill()
    UPDATE_PROCESS.wait()
    UPDATE_PROCESS = None
    return HttpResponse(json.dumps({
        "status": "ok"
    }, indent=4), content_type="application/json")

@csrf_exempt
@token_required
@allowed_methods(["GET"])
def update_log(req):
    if not USER_CONFIG["allow_update"]:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "update is not allowed by user config"
        }, indent=4), content_type="application/json", status=400)
    global UPDATE_PROCESS
    if not os.path.exists(UPDATE_LOG_PATH):
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "update log not found"
        }, indent=4), content_type="application/json", status=404)
    with open(UPDATE_LOG_PATH) as update_log_file:
        update_log = update_log_file.read()
        if UPDATE_PROCESS is None or UPDATE_PROCESS.poll() is not None:
            # update complete
            if UPDATE_PROCESS is not None and UPDATE_PROCESS.poll() is not None:
                UPDATE_PROCESS = None
            return HttpResponse(json.dumps({
                "status": "ok",
                "log": update_log,
                "is_complete": True
            }, indent=4), content_type="application/json")
        else:
            return HttpResponse(json.dumps({
                "status": "ok",
                "log": update_log,
                "is_complete": False
            }, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def schedule_manager(req):
    with SCHEDULE_MANAGER_LOCK:
        return HttpResponse(json.dumps(SCHEDULE_MANAGER, indent=4), content_type="application/json")

@csrf_exempt
@token_required
@allowed_methods(["GET"])
def self_test(req):
    res = get_system_status()
    return HttpResponse(json.dumps(res, indent=4), content_type="application/json")

@csrf_exempt
@token_required
@allowed_methods(["GET", "POST"])
def io(req):
    if req.method == "GET":
        try:
            out1 = rosparam.get_param("/xqserial_server/params/out1")
        except:
            out1 = -1
        try:
            out2 = rosparam.get_param("/xqserial_server/params/out2")
        except:
            out2 = -1
        try:
            out3 = rosparam.get_param("/xqserial_server/params/out3")
        except:
            out3 = -1
        return HttpResponse(json.dumps({
            "status": "ok",
            "out1": str(out1),
            "out2": str(out2),
            "out3": str(out3)
        }, indent=4), content_type="application/json")
    if req.method == "POST":
        req_data = {}
        try:
            req_data = json.loads(req.body.decode("utf-8"))
        except:
            pass
        if "level" not in req_data or "port" not in req_data:
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "level is required"
            }, indent=4), content_type="application/json", status=400)
        rosparam.set_param("/xqserial_server/params/out" + req_data["port"], req_data["level"])
        return HttpResponse(json.dumps({
            "status": "ok",
        }, indent=4), content_type="application/json")
    
@receiver(navigation_started)
def load_config(sender, **kwargs):
    config_data = CONFIG_DB.find_one({})
    if config_data is None:
        return
    update_config(config_data)

@csrf_exempt
@token_required
@allowed_methods(["GET", "POST", "DELETE"])
def galileo_config(req):
    """
    更改机器人配置，可以暴露给用户的机器人参数配置都放在这里
    """
    config_data = CONFIG_DB.find_one({})
    if config_data == None:
        config_data = get_config_from_param_server()
    if req.method == "GET":
        if "max_control_speed" in config_data and config_data["max_control_speed"] == 0:
            config_data["max_control_speed"] = None
        return HttpResponse(json.dumps(config_data, indent=4, default=json_util.default), content_type="application/json")
    if req.method == "POST":
        req_data = {}
        try:
            req_data = json.loads(req.body.decode("utf-8"))
        except Exception:
            pass
        if "navigation_speed" in req_data:
            nav_speed = None
            try:
                nav_speed = float(req_data["navigation_speed"])
            except Exception:
                pass
            if nav_speed == None:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "invalid navigation speed"
                }, indent=4), content_type="application/json", status=400)
            config_data["navigation_speed"] = nav_speed
        if "max_control_speed" in req_data:
            max_control_speed = None
            try:
                max_control_speed = float(req_data["max_control_speed"])
            except Exception:
                pass
            if max_control_speed == None:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "invalid max control speed"
                }, indent=4), content_type="application/json", status=400)
            config_data["max_control_speed"] = max_control_speed
        if "max_control_angle_speed" in req_data:
            max_angle_speed = None
            try:
                max_angle_speed = float(req_data["max_control_angle_speed"])
            except Exception:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "invalid max control angle speed"
                }, indent=4), content_type="application/json", status=400)
            config_data["max_control_angle_speed"] = max_angle_speed
        if "k2" in req_data:
            k2 = None
            try:
                k2 = float(req_data["k2"])
            except Exception:
                pass
            if k2 == None:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "invalid k2"
                }, indent=4), content_type="application/json", status=400)
            config_data["k2"] = k2
        
        if "kp" in req_data:
            kp = None
            try:
                kp = float(req_data["kp"])
            except Exception:
                pass
            if kp == None:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "invalid kp"
                }, indent=4), content_type="application/json", status=400)
            config_data["kp"] = kp

        if "ki" in req_data:
            ki = None
            try:
                ki = float(req_data["ki"])
            except Exception:
                pass
            if ki == None:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "invalid ki"
                }, indent=4), content_type="application/json", status=400)
            config_data["ki"] = ki

        if "kd" in req_data:
            kd = None
            try:
                kd = float(req_data["kd"])
            except Exception:
                pass
            if kd == None:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "invalid kd"
                }, indent=4), content_type="application/json", status=400)
            config_data["kd"] = kd
        
        if "look_ahead_dist" in req_data:
            look_ahead_dist = None
            try:
                look_ahead_dist = float(req_data["look_ahead_dist"])
            except Exception:
                pass
            if look_ahead_dist == None:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "invalid look_ahead_dist"
                }, indent=4), content_type="application/json", status=400)
            config_data["look_ahead_dist"] = look_ahead_dist

        if "theta_max" in req_data:
            theta_max = None
            try:
                theta_max = float(req_data["theta_max"])
            except Exception:
                pass
            if theta_max == None:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "invalid theta max"
                }, indent=4), content_type="application/json", status=400)
            config_data["theta_max"] = theta_max
        
        if "plan_width" in req_data:
            plan_width = None
            try:
                plan_width = float(req_data["plan_width"])
            except Exception:
                pass
            if plan_width == None:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "invalid plan_width"
                }, indent=4), content_type="application/json", status=400)
            config_data["plan_width"] = plan_width

        if "path_change" in req_data:
            path_change = None
            try:
                path_change = bool(req_data["path_change"])
            except Exception:
                pass
            if path_change == None:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "invalid path_change"
                }, indent=4), content_type="application/json", status=400)
            config_data["path_change"] = path_change

        if "forward_width" in req_data:
            forward_width = None
            try:
                forward_width = float(req_data["forward_width"])
            except Exception:
                pass
            if forward_width == None:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "invalid forward_width"
                }, indent=4), content_type="application/json", status=400)
            config_data["forward_width"] = forward_width

        if "rot_width" in req_data:
            rot_width = None
            try:
                rot_width = float(req_data["rot_width"])
            except Exception:
                pass
            if rot_width == None:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "invalid rot_width"
                }, indent=4), content_type="application/json", status=400)
            config_data["rot_width"] = rot_width

        if "backtime" in req_data:
            backtime = None
            try:
                backtime = float(req_data["backtime"])
            except Exception:
                pass
            if backtime == None:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "invalid backtime"
                }, indent=4), content_type="application/json", status=400)
            config_data["backtime"] = backtime

        if "bar_distance_min" in req_data:
            bar_distance_min = None
            try:
                bar_distance_min = float(req_data["bar_distance_min"])
            except Exception:
                pass
            if bar_distance_min == None:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "invalid bar_distance_min"
                }, indent=4), content_type="application/json", status=400)
            config_data["bar_distance_min"] = bar_distance_min
        
        if "deliver_wait_time" in req_data:
            deliver_wait_time = None
            try:
                deliver_wait_time = float(req_data["deliver_wait_time"])
            except Exception:
                pass
            if deliver_wait_time == None:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "invalid deliver wait time"
                }, indent=4), content_type="application/json", status=400)
            config_data["deliver_wait_time"] = deliver_wait_time
        

        # 保存至数据库
        CONFIG_DB.drop()
        CONFIG_DB.insert(config_data)
        # 设置参数至参数服务器
        try:
            update_config(config_data)
        except Exception:
            pass
        return HttpResponse(json.dumps(config_data, indent=4, default=json_util.default), content_type="application/json")
    if req.method == "DELETE":
        if "key" in req.GET:
            config_data = CONFIG_DB.find_one({})
            if config_data == None:
                config_data = get_config_from_param_server()
            if req.GET["key"] in config_data:
                config_data[req.GET["key"]] = None
                try:
                    rospy.delete_param(req.GET["key"])
                except Exception:
                    pass
            # 保存至数据库
            CONFIG_DB.drop()
            CONFIG_DB.insert(config_data)
            # 设置参数至参数服务器
            try:
                update_config(config_data)
            except Exception:
                pass
        return HttpResponse(json.dumps({
            "status": "ok",
            "description": "reset to default succeed"
        }, indent=4), content_type="application/json")

        

def update_config(config_data):
    # 设置参数至参数服务器
    # navigation_speed
    if "navigation_speed" in config_data and config_data["navigation_speed"] > 0:
        rospy.set_param("/move_base/params/linear_v", config_data["navigation_speed"])
    if "k2" in config_data and config_data["k2"] != None:
        rospy.set_param("/move_base/params/k2", config_data["k2"])
    if "kp" in config_data and config_data["kp"] != None:
        rospy.set_param("/move_base/params/kp", config_data["kp"])
    if "ki" in config_data and config_data["ki"] != None:
        rospy.set_param("/move_base/params/ki", config_data["ki"])
    if "kd" in config_data and config_data["kd"] != None:
        rospy.set_param("/move_base/params/kd", config_data["kd"])
    if "look_ahead_dist" in config_data and config_data["look_ahead_dist"] != None:
        rospy.set_param("/move_base/params/look_ahead_dist", config_data["look_ahead_dist"])
    if "theta_max" in config_data and config_data["theta_max"] != None:
        rospy.set_param("/move_base/params/theta_max", config_data["theta_max"])
    if "plan_width" in config_data and config_data["plan_width"] != None:
        rospy.set_param("/move_base/params/plan_width", config_data["plan_width"])
    if "path_change" in config_data and config_data["path_change"] != None:
        rospy.set_param("/move_base/params/path_change", config_data["path_change"])
    if "forward_width" in config_data and config_data["forward_width"] != None:
        rospy.set_param("/move_base/params/forward_width", config_data["forward_width"])
    if "rot_width" in config_data and config_data["rot_width"] != None:
        rospy.set_param("/move_base/params/rot_width", config_data["rot_width"])
    if "backtime" in config_data and config_data["backtime"] != None:
        rospy.set_param("/move_base/params/backtime", config_data["backtime"])
    if "bar_distance_min" in config_data and config_data["bar_distance_min"] != None:
        rospy.set_param("/move_base/params/bar_distance_min", config_data["bar_distance_min"])
    if "deliver_wait_time" in config_data and config_data["deliver_wait_time"] is not None:
        rospy.set_param("/xqserial_server/params/wait_time", config_data["deliver_wait_time"])
    
    # max_control_speed
    # avoid_distance
    # get_around
    # auto_switch_map
    # dynamic_reconfigure.client.Client("/bwMono", timeout=3).update_configuration({"auto_switch_map": config_data["auto_switch_map"]})

@csrf_exempt
@token_required
@allowed_methods(["GET"])
def tts(req):
    if "text" not in req.GET:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "text is required"
        }, indent=4), content_type="application/json", status=400)
    audio_msg = String()
    audio_msg.data = req.GET["text"]
    set_topic_data("/xiaoqiang_tts/text", String, audio_msg)
    return HttpResponse(json.dumps({
        "status": "OK",
    }, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def check_cert(req):
    cert_process = subprocess.Popen("/home/xiaoqiang/Documents/.galileo/verify_tool", universal_newlines=True, shell=True,
        stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    cert_process.wait()
    if cert_process.returncode != 0:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "invalid cert"
        }, indent=4), content_type="application/json", status=400)
    return HttpResponse(json.dumps({
        "status": "ok"
    }, indent=4), content_type="application/json")