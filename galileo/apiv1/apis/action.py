#encoding=utf-8
import json
import time
import threading
from django.http import HttpResponse
from base.actions.Action import Action
from base.actions.action_utils import ActionUtils
from base.actions.NavAction import NavAction
from base.actions.CallbackAction import CallbackAction
from base.models import ACTION_DB, TASK_DB
from base.tasks.Task import Task
from bson import json_util
from django.views.decorators.csrf import csrf_exempt
from utils.decorator import allowed_methods, required_keys
from apiv1.decorator import token_required, allowed_status


@csrf_exempt
@token_required
@allowed_methods(["GET", "POST", "PUT", "DELETE"])
def action_controller(req):
    if req.method == "GET":
        if "id" in req.GET:
            action = ACTION_DB.find_one({"id": req.GET["id"]})
            if action is None:
                action = ActionUtils.load_by_id(req.GET["id"])
                if action is not None:
                    action = action.to_dict()
            if action is None:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "result": False,
                    "description": "target action {id} not found".format(id=req.GET["id"])
                }, indent=4), content_type="application/json", status=404)
            else:
                return HttpResponse(json.dumps(action, indent=4, default=json_util.default), content_type="application/json")
        else:
            actions = list(ACTION_DB.find())
            return HttpResponse(json.dumps(actions, indent=4, default=json_util.default), content_type="application/json")

    if req.method == "POST":
        req_data = {}
        try:
            req_data = json.loads(req.body.decode("utf-8"))
        except Exception:
            req_data = {}
        if not set(["type"]).issubset(req_data):
            return HttpResponse(json.dumps({
                "result": False,
                "description": "type is required",
            }, indent=4), content_type="application/json")
        task = None
        if "task_id" not in req_data:
            # 自动创建一个新的任务, 且不保存，作为临时task
            task = Task.new_task({
                "name": "auto task"
            })
        else:
            task = Task.load_by_id(req_data["task_id"])
        if task is None:
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "target task not exist",
            }, indent=4), content_type="application", status=400)
        new_action = ActionUtils.new_action(req_data)
        if new_action is None:
            return HttpResponse(json.dumps({
                    "status": "error",
                    "result": False,
                    "description": "invalid action data",
                }, indent=4), content_type="application/json", status=400)
        if "task_id" in req_data:
            # 只保存永久任务，临时任务不保存
            new_action.save()
        task.sub_tasks.append(new_action)
        if "task_id" in req_data:
            # 只保存永久任务，临时任务不保存
            task.save()
        if "task_id" not in req_data:
            # 对于新创建的task，进行返回，方便操作
            return HttpResponse(json.dumps(task.to_dict(), indent=4), content_type="application/json")
        return HttpResponse(
                json.dumps(new_action.to_dict(), indent=4),
                content_type="application/json")

    req_data = {}
    try:
        req_data = json.loads(req.body.decode("utf-8"))
    except Exception:
        req_data = {}
    if req.method == "DELETE":
        req_data = req.GET
    if "id" not in req_data:
        return HttpResponse(json.dumps({
            "status": "error",
            "result": False,
            "description": "id is required"
        }, indent=4), content_type="application", status=400)
    action = ACTION_DB.find_one({"id": req_data["id"]})
    if action is None:
        return HttpResponse(json.dumps({
            "status": "error",
            "result": False,
            "description": "target action not found",
        }, indent=4), content_type="application/json", status=404)

    if req.method == "PUT":
        target_action = ActionUtils.load_by_id(action["id"])
        target_action.update_action(req_data)
        return HttpResponse(json.dumps(target_action.to_dict(), indent=4), content_type="application/json")

    if req.method == "DELETE":
        ActionUtils.delete(req_data["id"])
        return HttpResponse(json.dumps({
            "status": "OK",
            "result": True,
        }, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET", "POST", "PUT", "DELETE"])
def task_controller(req):
    if req.method == "GET":
        if "id" not in req.GET:
            tasks_from_db = list(TASK_DB.find())
            tasks = []
            with Task.data_lock:
                for task in Task.instances:
                    tasks.append(task.to_dict())
            for task in tasks_from_db:
                if len(filter(lambda x: x["id"] == task["id"], tasks)) == 0:
                    # 内存中未包含此数据
                    tasks.append(task)
            return HttpResponse(json.dumps(tasks, indent=4, default=json_util.default), content_type="application/json")
        else:
            # 获取当前的导航任务
            if req.GET["id"] == "current_nav_action":
                if NavAction.currentAction is not None:
                    return HttpResponse(json.dumps(NavAction.currentAction.to_dict(), indent=4), content_type="application/json")
                else:
                    return HttpResponse(json.dumps({
                        "status": "error",
                        "description": "no current navigation task found"
                    }, indent=4), content_type="application/json", status=404)
            target_task = Task.load_by_id(req.GET["id"])
            if target_task is not None:
                return HttpResponse(json.dumps(target_task.to_dict(), indent=4), content_type="application/json")
            else:
                return HttpResponse(json.dumps({
                    "status": "error",
                    "description": "task not found"
                }, indent=4), content_type="application/json", status=404)
    if req.method == "POST":
        req_data = {}
        try:
            req_data = json.loads(req.body.decode("utf-8"))
        except Exception as e:
            print(e)
        if not set(["name", "sub_tasks"]).issubset(req_data):
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "name and sub_tasks are required"
            }, indent=4), content_type="application/json", status=400)
        new_task = Task.new_task(req_data)
        if new_task is None:
            return HttpResponse(json.dumps({
                "status": "error",
                "description": str(e)
            }, indent=4), content_type="application/json", status=400)
        return HttpResponse(json.dumps(new_task.to_dict(), indent=4), content_type="application/json")
    if req.method == "PUT":
        req_data = None
        try:
            req_data = json.loads(req.body.decode("utf-8"))
        except Exception as e:
            print(e)
        task = Task.load_by_id(req_data["id"])
        if task is None:
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "task not found"
            }, indent=4), content_type="application/json", status=404)
        task.update(req_data)
        task.save()
        return HttpResponse(json.dumps(task.to_dict(), indent=4), content_type="application/json")
    if req.method == "DELETE":
        if not set(["id"]).issubset(req.GET):
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "map_name, path_name, id are required"
            }, indent=4), content_type="application/json", status=400)
        task = Task.load_by_id(req.GET["id"])
        if task is not None:
            task.delete()
        return HttpResponse(json.dumps({
            "status": "OK"
        }, indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
def task_start(req):
    if "id" not in req.GET:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "id is required"
        }, indent=4), content_type="application/json", status=400)
    target_task = Task.load_by_id(req.GET["id"])
    if target_task is None:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "target task not found",
        }, indent=4), content_type="application/json", status=404)
    target_task.stop()
    while target_task.state != Task._states.CANCELLED:
        time.sleep(0.1)
    threading.Thread(target=target_task.start).start()
    return HttpResponse(json.dumps(target_task.to_dict(), indent=4), content_type="application/json")

@csrf_exempt
@token_required
@allowed_methods(["GET"])
def task_pause(req):
    if "id" not in req.GET:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "id is required"
        }, indent=4), content_type="application/json", status=400)
    target_task = Task.load_by_id(req.GET["id"])
    if target_task is None:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "target task not found",
        }, indent=4), content_type="application/json", status=404)

    target_task.pause()
    return HttpResponse(json.dumps(target_task.to_dict(), indent=4), content_type="application/json")

@csrf_exempt
@token_required
@allowed_methods(["GET"])
def task_resume(req):
    if "id" not in req.GET:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "id is required"
        }, indent=4), content_type="application/json", status=400)
    target_task = Task.load_by_id(req.GET["id"])
    if target_task is None:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "target task not found",
        }, indent=4), content_type="application/json", status=404)

    target_task.resume()
    return HttpResponse(json.dumps(target_task.to_dict(), indent=4), content_type="application/json")



@csrf_exempt
@token_required
@allowed_methods(["GET"])
def task_stop(req):
    if "id" not in req.GET:
        # 停止所有 task
        with Task.data_lock:
            for task in Task.instances:
                task.stop()
        return HttpResponse(json.dumps({
            "status": "ok",
        }, indent=4), content_type="application/json", status=400)
    target_task = Task.load_by_id(req.GET["id"])
    if target_task is None:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "target task not found",
        }, indent=4), content_type="application/json", status=404)

    target_task.stop()
    return HttpResponse(json.dumps(target_task.to_dict(), indent=4), content_type="application/json")


@csrf_exempt
@token_required
@allowed_methods(["GET"])
@required_keys(["loop_flag"])
def task_loop(req):
    if "id" not in req.GET:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "id is required"
        }, indent=4), content_type="application/json", status=400)
    target_task = Task.load_by_id(req.GET["id"])
    if target_task is None:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "target task not found",
        }, indent=4), content_type="application/json", status=404)
    target_task.loop_flag = req.GET["loop_flag"].lower() == "true"
    return HttpResponse(json.dumps(target_task.to_dict(), indent=4), content_type="application/json")

@csrf_exempt
@token_required
@allowed_methods(["GET"])
def update_wait_req_action(req):
    if "id" not in req.GET:
        # 没有id的情况下更新所有的 WaitReqAction
        set_wait_flag = False
        with ActionUtils.data_lock:
            for instance in ActionUtils.instances:
                if instance.to_dict()["type"] == "wait_req_action" and instance.state == Action._states.WORKING:
                    instance.complete()
                    set_wait_flag = True
        if not set_wait_flag:
            # 没有找到需要设置的 wait_req_action
            return HttpResponse(json.dumps({
                "status": "error",
                "description": "not wait action found"
            }, indent=4), content_type="application/json", status=400)
        return HttpResponse(json.dumps({
            "status": "OK",
        }, indent=4), content_type="application/json")
    target_action = ActionUtils.load_by_id(req.GET["id"])
    if target_action is None:
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "target action not found"
        }, indent=4), content_type="application/json", status=404)

    if target_action.to_dict()["type"] != "wait_req_action":
        return HttpResponse(json.dumps({
            "status": "error",
            "description": "target action is not wait req action"
        }, indent=4), content_type="application/json", status=400)
    target_action.complete()
    return HttpResponse(json.dumps(target_action.to_dict(), indent=4),
        content_type="application/json")

@csrf_exempt
@token_required
@allowed_methods(["GET"])
def running_tasks(req):
    """
    获取当前正在执行的任务
    """
    all_tasks = []
    with Task.data_lock:
        all_tasks = [instance for instance in Task.instances if instance.state == Task._states.WORKING or instance.state == Task._states.PAUSED]
    return HttpResponse(json.dumps({
        "tasks": [task.to_dict() for task in all_tasks]
    }, indent=4), content_type="application/json")