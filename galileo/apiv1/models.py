#!/usr/bin/env python
#encoding=utf-8

"""apiv1 models"""
from pymongo import MongoClient
from galileo.settings import DEBUG, TEST, MONGO_DB_NAME
from utils.utils import get_config_from_param_server


CLIENT = MongoClient()
DB = None

if TEST:
    DB = CLIENT[MONGO_DB_NAME + "_test"]
elif DEBUG:
    DB = CLIENT[MONGO_DB_NAME + "_debug"]
else:
    DB = CLIENT[MONGO_DB_NAME]

TOKEN_DB = DB["token"]
RECHARGE_DB = DB["recharge"]
CONFIG_DB = DB["config"]
if CONFIG_DB.find_one({}) is None:
    CONFIG_DB.insert_one(get_config_from_param_server())
SLAMDB = CLIENT["slamdb"]
SLAMDB_MAP = CLIENT["slamdb"]["maps"]
SLAMDB_KEYFRAME = CLIENT["slamdb"]["keyframes"]
SLAMDB_KEYFRAMEDB = CLIENT["slamdb"]["keyframedbs"]
SLAMDB_MAPPOINTS = CLIENT["slamdb"]["map_points"]

def delete_map(map_name):
    SLAMDB_MAP.delete_one({"mMapName": map_name})
    SLAMDB_KEYFRAME.delete_one({"mMapName": map_name})
    SLAMDB_KEYFRAMEDB.delete_one({"mMapName": map_name})
    SLAMDB_MAPPOINTS.delete_one({"mMapName": map_name})

def is_valid_token(token):
    if TOKEN_DB.find_one({"token": token}) is None:
        return False
    return True
