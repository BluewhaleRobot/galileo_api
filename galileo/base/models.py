#!/usr/bin/env python
#encoding=utf-8

"""apiv1 models"""
from pymongo import MongoClient
from galileo.settings import DEBUG, TEST, MONGO_DB_NAME


CLIENT = MongoClient()
DB = None

if TEST:
    DB = CLIENT[MONGO_DB_NAME + "_test"]
elif DEBUG:
    DB = CLIENT[MONGO_DB_NAME + "_debug"]
else:
    DB = CLIENT[MONGO_DB_NAME]
TASK_DB = DB["task"]
ACTION_DB = DB["action"]