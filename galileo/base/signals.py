from django.shortcuts import HttpResponse
import time
import django.dispatch
from django.dispatch import receiver

navigation_started = django.dispatch.Signal(providing_args=['map', 'path'])