#!/usr/bin/env python
# encoding=utf-8

from __future__ import print_function
import logging
import threading
import time
import rostopic
import rospy
import json
from libs.websocket_server import WebsocketServer as WsServer
from libs.rospy_message_converter.json_message_converter \
    import convert_ros_message_to_json
from galileo.settings import WS_SERVICE_PORT, USER_CONFIG
from apiv1.models import is_valid_token

class WebsocketServer(threading.Thread):

    def __init__(self):
        super(WebsocketServer, self).__init__()
        self.is_running = True
        self.server = WsServer(
            WS_SERVICE_PORT, host='0.0.0.0', loglevel=logging.DEBUG)
        self.server.set_fn_new_client(self.new_client)
        self.server.set_fn_client_left(self.client_left)
        self.topic_map = {}
        self.logger = logging.getLogger(__name__)
        logging.basicConfig()

    def run(self):
        threading.Thread(target=self.server.run_forever).start()
        while self.is_running:
            time.sleep(1)
        self.server.shutdown()
        print("Websocket Server stopped")

    def new_client(self, client, server):
        topic_name = client['headers']['path']
        params = client['headers']['query']
        rospy.logwarn(client['headers']['path'])
        rospy.logwarn(json.dumps(params, indent=4))
        rospy.logwarn("new clinet")
        if not USER_CONFIG["no_token_check"]:
            if "token" not in params:
                rospy.logwarn("no token found")
                client['handler'].close()
                return
            if not is_valid_token(params["token"]):
                rospy.logwarn("token invalid: " + params["token"])
                client['handler'].close()
                return
        self.register_topic(client, topic_name)

    def client_left(self, client, server):
        rospy.logwarn("client offline: " + client['headers']['path'])
        if client is not None:
            self.unregister_topic(client)


    def register_topic(self, client, topic_name):
        topic_class = rostopic.get_topic_class(topic_name)[0]
        if topic_class is None:
            # cannot find target topic
            rospy.logwarn("subscribe to topic {topic} failed".format(topic=topic_name))
            client['handler'].close()
            return
        client['fail_count'] = 0

        def msg_cb(data):
            try:
                client['handler'].send_message(
                    convert_ros_message_to_json(data))
                client['fail_count'] = 0
            except Exception as e:
                rospy.logwarn(e)
                # 发送失败关闭此订阅
                client['fail_count'] += 1
                rospy.logwarn("send message failed")
                if client['fail_count'] > 20:
                    rospy.logwarn("send message failed reached max count, unregister topic")
                    self.topic_map[client['id']].unregister()
                    client['handler'].close()

        sub = rospy.Subscriber(topic_name, topic_class, msg_cb)
        self.topic_map[client['id']] = sub

    def unregister_topic(self, client):
        if client['id'] in self.topic_map:
            self.topic_map[client['id']].unregister()

    def stop(self):
        self.is_running = False
        self.server.server_close()
