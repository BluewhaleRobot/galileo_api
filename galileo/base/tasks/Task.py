#!/usr/bin/env python
# encoding=utf-8

import json
import os
from subprocess import Popen
from uuid import uuid4
from utils.utils import Enum
from shutil import rmtree
from django.dispatch import receiver
from base.management.commands import SHUTDOWN_SINGAL
from base.models import TASK_DB, ACTION_DB
from base.actions.Action import Action
from base.actions.action_utils import ActionUtils
from base.actions.SleepAction import SleepAction
import threading


class Task(object):

    _states = Enum(["WAITTING", "WORKING", "PAUSED", "CANCELLED", "ERROR", "COMPLETE"])
    instances = []
    data_lock = threading.RLock()

    def __init__(self, name=""):
        self.id = str(uuid4())
        self.sub_tasks = []
        self.stopped = False
        self.loop_flag = False
        self.current_task = None
        self.name = name
        self.state = Task._states.WAITTING
        self.loop_count = 0

    def start(self):
        self.state = Task._states.WORKING
        self.stopped = False
        first_loop = True
        while self.loop_flag or first_loop:
            for task in self.sub_tasks:
                self.current_task = task
                try:
                    task.start()
                except Exception:
                    task.state = Task._states.ERROR
                if task.state == Task._states.ERROR:
                    self.state = Task._states.ERROR
                    return
                if task.state == Task._states.CANCELLED:
                    self.stopped = True
                    self.state = Task._states.CANCELLED
                if self.stopped:
                    return
            first_loop = False
            self.loop_count += 1
        self.state = Task._states.COMPLETE

    def stop(self):
        self.stopped = True
        if self.current_task is not None:
            self.current_task.stop()
        self.state = Task._states.CANCELLED

    def pause(self):
        if self.current_task is not None:
            self.current_task.pause()
        self.state = Task._states.PAUSED

    def resume(self):
        if self.current_task is not None:
            self.current_task.resume()
        self.state = Task._states.WORKING

    def __getitem__(self, key):
        return self.sub_tasks[key]

    def __setitem__(self, key, item):
        self.sub_tasks[key] = item

    def __len__(self):
        return len(self.sub_tasks)

    def append(self, value):
        self.sub_tasks.append(value)

    def loop(self, loop_flag):
        self.loop_flag = loop_flag
        self.loop_count = 0

    @staticmethod
    def __loads(data):
        task = Task.new_task(data)
        task.id = data["id"]
        return task

    def progress(self):
        progress = 0
        if self.state == Task._states.COMPLETE:
            progress = 1
        if self.state == Task._states.WORKING:
            if self.current_task is not None:
                index = self.sub_tasks.index(self.current_task)
                progress = (index + self.current_task.progress())* 1.0 / len(self.sub_tasks)
        return progress


    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "loop_flag": self.loop_flag,
            "current_task": None if self.current_task is None else self.current_task.to_dict(),
            "state": self.state,
            "sub_tasks": [None if task is None else task.to_dict() for task in self.sub_tasks],
            "progress": self.progress(),
            "loop_count": self.loop_count
        }

    def save(self, overwrite=False):
        # check if record exist in database
        if TASK_DB.find_one({"id": self.id}) is not None:
            TASK_DB.find_one_and_update({"id": self.id}, {"$set": self.to_dict()})
        else:
            TASK_DB.insert_one(self.to_dict())
        return True

    def update(self, task_data):
        if "name" in task_data:
            self.name = task_data["name"]
        if "loop_flag" in task_data:
            self.loop_flag = task_data["loop_flag"]
        if "sub_tasks" in task_data:
            self.sub_tasks = []
            for sub_task_dict in task_data["sub_tasks"]:
                if "sub_tasks" in sub_task_dict:
                    # this is a task, check task exists
                    sub_task = Task.load_by_id(sub_task_dict["id"])
                    if sub_task is not None:
                        self.sub_tasks.append(sub_task)
                else:
                    # this is an action
                    sub_action = ActionUtils.load_by_id(sub_task_dict)
                    if sub_action is not None:
                        self.sub_tasks.append(sub_action)

    @staticmethod
    def load_by_id(id):
        target_task = [task for task in Task.instances if task.id == id]
        if len(target_task) == 0:
            task_record = TASK_DB.find_one({"id": id})
            if task_record is None:
                return None
            return Task.__loads(task_record)
        return target_task[0]

    @staticmethod
    def new_task(data):
        task = Task()
        if "name" in data:
            task.name = data["name"]
        if "loop_flag" in data:
            task.loop_flag = data["loop_flag"]
        if "sub_tasks" in data:
            for sub_task_dict in data["sub_tasks"]:
                if "sub_tasks" in sub_task_dict:
                    # this is a task, check task exists
                    sub_task = None
                    if "id" not in sub_task_dict or sub_task_dict["id"] == "":
                        # 当前不存在的任务，需要自己创建一个新任务
                        sub_task = Task.new_task(sub_task_dict)
                    else:
                        sub_task = Task.load_by_id(sub_task_dict["id"])
                    if sub_task is not None:
                        task.sub_tasks.append(sub_task)
                else:
                    # this is an action
                    sub_action = None
                    if "id" not in sub_task_dict or sub_task_dict["id"] == "":
                        # 当前不存在的动作，需要自己创建一个新动作
                        sub_action = ActionUtils.new_action(sub_task_dict)
                    else:
                        sub_action = ActionUtils.load_by_id(sub_task_dict["id"])
                    if sub_action is not None:
                        task.sub_tasks.append(sub_action)
        with Task.data_lock:
            Task.instances.append(task)
        return task

    def delete(self):
        self.stop()
        with Task.data_lock:
            Task.instances.remove(self)
        TASK_DB.delete_one({"id": self.id})

    def is_action(self):
        False

@receiver(SHUTDOWN_SINGAL)
def stop_all_task(sender, **kwargs):
    with Task.data_lock:
        for task in Task.instances:
            task.stop()