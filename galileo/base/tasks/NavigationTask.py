#!/usr/bin/env python
#encoding=utf-8

import time
import threading
from actionlib_msgs.msg import GoalStatus
from base.actions.NavigationTarget import NavigationTarget


class NavigationTask(threading.Thread):

    def __init__(self):
        super(NavigationTask, self).__init__()
        self.target_points = []
        self.target_point_index = 0
        self.target_points_lock = threading.RLock()
        self.current_target = NavigationTarget()
        self.is_running = False
        self.pause_flag = False

    def run(self):
        self.is_running = True
        while self.is_running:
            current_point = None
            with self.target_points_lock:
                if len(self.target_points) == self.target_point_index:
                    self.target_point_index = 0
                if len(self.target_points) == 0 or self.pause_flag:
                    time.sleep(0.1)
                    continue
                current_point = self.target_points[self.target_point_index]
            self.current_target.send_goal(
                current_point["x"],
                current_point["y"],
                current_point["angle"],
            )
            self.target_point_index += 1

        print("navigation task exited")

    def send_goal(self, pos_x, pos_y, angle):
        if self.current_target.get_state() == GoalStatus.ACTIVE:
            self.current_target.cancel_goal()
        self.pause()
        self.current_target.send_goal_async(pos_x, pos_y, angle)


    def stop(self):
        self.is_running = False

    def pause(self):
        if self.current_target.get_state() == GoalStatus.ACTIVE:
            self.current_target.cancel_goal()
        self.pause_flag = True

    def resume(self):
        self.pause_flag = False

    def set_target_points(self, target_points):
        with self.target_points_lock:
            self.target_points = target_points
            self.target_point_index = 0
            self.resume()

    def get_target_points(self):
        with self.target_points_lock:
            return self.target_points
