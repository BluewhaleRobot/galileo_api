from base.tasks.Task import Task
from base.actions.NavAction import NavAction
from base.actions.SleepAction import SleepAction
import json

class NavTask(Task):

    def __init__(self, map, path, wait_time=5):
        super(NavTask, self).__init__()
        self.wait_time = wait_time
        self.map = map
        self.pause = path
    
    @staticmethod
    def loads(data):
        data = json.load(data)
        task = NavTask(data["map"], data["path"], data["wait_time"])
        task.loop_flag = data["loop_flag"]
        for action in data["actions"]:
            task.append(NavAction.loads(action))
            task.append(SleepAction(task.wait_time))
        return task

    @staticmethod
    def load(file):
        with open(file) as json_file:
            data = json_file.read()
            return NavTask.loads(data)

    @staticmethod
    def dumps(task):
        task_json = {
            "map": task.map,
            "path": task.path,
            "wait_time": task.wait_time,
            "loop_flag": task.loop_flag,
            "actions": []
        }
        for action in task.actions:
            task_json["actions"].append(action.to_dict())
        return json.dumps(task_json, indent=4)

    @staticmethod
    def dump(file, task):
        with open(file, "w+") as json_file:
            json_file.write(NavTask.dumps(task))
    