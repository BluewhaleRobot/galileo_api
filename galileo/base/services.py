#/usr/bin/env python
#encoding=utf-8

"""
在后台长期运行的线程
"""
import os
import time
import json
import threading
import requests
import rospy
from django.dispatch import receiver
from django.test import Client
from galileo.settings import USER_CONFIG, BATTERY_LOW, MAP_WORKSPACE_PATH, MAP_PATH
from base.management.commands import SHUTDOWN_SINGAL
from base.core import SCHEDULE_MANAGER, SCHEDULE_MANAGER_LOCK, status_updater
from base.ros_connector import get_topic_data, set_topic_data
from galileo_serial_server.msg import GalileoStatus
from utils.utils import get_my_id
from apiv1.apis.system import update_config
from apiv1.models import CONFIG_DB
from utils.utils import md5sum
from std_msgs.msg import String


def schedule_manager_reporter():
    global SCHEDULE_MANAGER, SCHEDULE_MANAGER_LOCK

    schedule_manager_reporter.running = True
    while schedule_manager_reporter.running:
        time.sleep(1)
        robot_status = get_topic_data("/galileo/status", GalileoStatus)
        if robot_status is not None:
            status_updater(robot_status["data"])
    print("schedule manager reporter stoped")

threading.Thread(target=schedule_manager_reporter, args=()).start()

# 状态检测线程
def status_monitor():
    print("status monitor started")
    status_monitor.running = True
    while status_monitor.running and not rospy.is_shutdown():
        time.sleep(5)
        robot_status = get_topic_data("/galileo/status", GalileoStatus)
        if robot_status is not None and robot_status["data"].navStatus == 1:
            # 处于导航状态每5秒设置一下状态
            config_db = CONFIG_DB.find_one({})
            if config_db is not None:
                update_config(config_db)
    print("status monitor stopped")

threading.Thread(target=status_monitor, args=()).start()

# 自动充电低电量检测线程
def charge_monitor():
    print("charge monitor started")
    charge_monitor.running = True
    power_records = []
    last_charge_time = 0
    last_nav_time = 0
    while charge_monitor.running and not rospy.is_shutdown():
        time.sleep(1)
        # 检测docker station文件是否发生变化
        dock_station_path = os.path.join(MAP_WORKSPACE_PATH, "dock_station.txt")
        if os.path.exists(dock_station_path):
            # 获取当前地图名称
            map_name = ""
            if os.path.exists("/home/xiaoqiang/slamdb/map_name.txt"):
                with open("/home/xiaoqiang/slamdb/map_name.txt") as map_name_file:
                    map_name = map_name_file.read().strip()
            
            if map_name != "":
                save_dock_station_path = os.path.join(MAP_PATH, map_name, "dock_station.txt")
                if not os.path.exists(save_dock_station_path) or \
                    md5sum(dock_station_path) != md5sum(save_dock_station_path):
                    # 更新充电桩位置文件
                    os.system("cp -rfp {station_filename} {dist}".format(
                        station_filename=dock_station_path,
                        dist=save_dock_station_path
                    ))

        if not USER_CONFIG["auto_charge"]:
            continue
        try:
            now = int(time.time() * 1000)
            if now - last_charge_time < 5 * 60 * 1000:
                return
            c = Client()
            res = c.get("/api/v1/system/info")
            if res.status_code != 200:
                continue
            info = json.loads(res.content.decode("utf-8"))
            if len(power_records) < 120:
                power_records.append(info["battery"])
                continue
            else:
                power_records.pop(0)
                power_records.append(info["battery"])
            status = get_topic_data("/galileo/status", GalileoStatus)
            if status == None:
                continue
            if status["data"].chargeStatus == 1 or status["data"].chargeStatus == 2:
                # 正在充电或已经充满
                continue
            if status["data"].busyStatus == 1:
                continue
            if sum(power_records) * 1.0 / len(power_records) > USER_CONFIG["battery_low"] or \
                status["data"].power < (BATTERY_LOW / 2 + 0.2):
                # 电量足或者电压获取错误
                continue
            if status["data"].navStatus != 1:
                if now - last_nav_time < 5 * 60 * 1000:
                    #电量低的情况下需要强制开启导航服务，每5分钟尝试一次
                    continue
                c.get("/api/v1/system/tts?text={text}".format(text="电量低，即将开启导航服务，开启服务过程中机器人可能会原地旋转，请注意安全"))
                time.sleep(3)
                # 启动导航
                r  = c.get("/api/v1/navigation/start")
                if r.status_code != 200:
                    rospy.logwarn(r.content.decode("utf-8"))
                    continue
                last_nav_time = now
                continue
            if status["data"].visualStatus < 1:
                continue
            # 开始充电指令
            time.sleep(5)
            c.get("/api/v1/system/tts?text=" + "电量低，开始自动返回充电")
            r = c.get("/api/v1/navigation/go_charge")
            if r.status_code == 200:
                last_charge_time = now
        except Exception as e:
            rospy.logwarn(e)
            
threading.Thread(target=charge_monitor, args=()).start()

# 电量警告线程
def audio_reporter():
    print("audio reporter started")
    audio_reporter.running = True
    c = Client()
    warn_wait_count = 0
    while charge_monitor.running and not rospy.is_shutdown():
        time.sleep(1)
        res = c.get("/api/v1/system/info")
        if res.status_code != 200:
            return
        res = json.loads(res.content.decode("utf-8"))
        if "battery" not in res:
            return
        status = get_topic_data("/galileo/status", GalileoStatus)
        if status is None:
            return
        # 20%时提示电量低
        if res["battery"] <= 20 and res["battery"] > 5 and status["data"].targetStatus <= 0:
            warn_wait_count += 1
            if warn_wait_count >= 5:
                warn_wait_count = -30
                data = String()
                data.data = "电量小于百分之二十，请及时充电"
                set_topic_data("/xiaoqiang_tts/text", String, data)
            return 
        # 5%时提示电量很低
        if res["battery"] < 5 and res["battery"] > 0:
            warn_wait_count += 1
            if warn_wait_count > 5:
                warn_wait_count = -5
                data = String()
                data.data = "电量小于百分之五，请及时充电"
                set_topic_data("/xiaoqiang_tts/text", String, data)
            return
        # 0%时自动关机，由system_monitor完成warn_wait_count += 1
threading.Thread(target=audio_reporter, args=()).start()

@receiver(SHUTDOWN_SINGAL)
def stop_udp(sender, **kwargs):
    schedule_manager_reporter.running = False
    status_monitor.running = False
    charge_monitor.running = False
    audio_reporter.running = False