#!/usr/bin/env python
#encoding=utf-8

import signal
import sys
import time
import django

from django.core.management.commands.runserver import Command as BaseRunserverCommand
from base.management.commands import SHUTDOWN_SINGAL

# pylint: disable=C0413, w0611
import base.ros_connector
# pylint: enable=C0413, w0611

class Command(BaseRunserverCommand):
    def __init__(self, *args, **kwargs):
        signal.signal(signal.SIGINT, Command.handle_exit)
        time.sleep(2)
        BaseRunserverCommand.__init__(self, *args, **kwargs)

    def create_parser(self, prog_name, subcommand):
        """
        Create and return the ``ArgumentParser`` which will be used to
        parse the arguments to this command.
        """
        parser = super(Command, self).create_parser(prog_name, subcommand)
        parser.parse_args = Command.get_parser(parser)
        return parser

    @classmethod
    def get_parser(cls, parser):
        def parse_known(*args, **kwargs):
            arg, dummy = parser.parse_known_args(*args, **kwargs)
            return arg
        return parse_known

    @classmethod
    def handle_exit(cls, _signal, _frame):
        SHUTDOWN_SINGAL.send('system')
        print("\nWait 2s to exit")
        countdown = 0
        while countdown < 2:
            countdown += 1
            time.sleep(1)
            print(str(2 - countdown) + "...")
        sys.exit(0)
