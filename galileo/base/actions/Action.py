#!/usr/bin/env python
# encoding=utf-8

from utils.utils import Enum
from uuid import uuid4
from base.models import ACTION_DB

class Action(object):

    """
    动作对象。一个动作就是需要完成的一个目标，比如移动到某目标点。等待1秒，触发某个io电平
    动作可以开始，暂停，继续，取消
    动作有特定的状态，如等待开始，暂停，继续，完成
    多个动作可以构成一个任务(Task)
    """

    _states = Enum(
            ["WAITTING", "WORKING", "PAUSED", "CANCELLED", "COMPLETE", "ERROR"])
    def __init__(self):
        self.id = str(uuid4())

    def start(self):
        raise NotImplementedError("start not implement")

    def pause(self):
        raise NotImplementedError("pause not implement")

    def resume(self):
        raise NotImplementedError("resume not implement")

    def stop(self):
        raise NotImplementedError("stop not implement")

    def progress(self):
        """
        返回动作执行的进度百分比
        """
        raise NotImplementedError("progress not implement")

    def to_dict(self):
        raise NotImplementedError("todict not implement")

    def is_action(self):
        return True

    def save(self):
        if ACTION_DB.find_one({"id": self.id}) is None:
            ACTION_DB.insert_one(self.to_dict())
        else:
            ACTION_DB.find_one_and_update({"id", self.id}, {"$set": self.to_dict()})
