#!/usr/bin/env python
# encoding=utf-8

import json
import time
from base.actions.Action import Action
import rospy
from bw_auto_dock.srv import Charge, StopCharge, ChargeRequest, StopChargeRequest
from base.ros_connector import get_topic_data
from galileo_serial_server.msg import GalileoStatus

class ChargeAction(Action):

    def __init__(self, x, y, theta):
        super(ChargeAction, self).__init__()
        self.x = x
        self.y = y
        self.theta = theta
        self.state = Action._states.WAITTING
        self.paused = False
        self.stopped = False
        self.result = ""
        rospy.wait_for_service('/bw_auto_dock/charge')
        rospy.wait_for_service('/bw_auto_dock/stop_charge')
        self.start_charge = rospy.ServiceProxy('/bw_auto_dock/charge', Charge)
        self.stop_charge = rospy.ServiceProxy('/bw_auto_dock/stop_charge', StopCharge)

    def start(self):
        self.state = Action._states.WORKING
        self.paused = False
        self.stopped = False
        rospy.wait_for_service('/bw_auto_dock/charge')
        charge_req = ChargeRequest()
        charge_req.x = self.x
        charge_req.y = self.y
        charge_req.theta = self.theta
        charge_res = self.start_charge(charge_req)
        if not charge_res:
            # 发送充电请求失败
            self.state = Action._states.ERROR
            return
        while not self.stopped:
            time.sleep(1)
            # 通过充电状态判断是否充电完成
            galileo_status = get_topic_data("/galileo/status", GalileoStatus)
            if galileo_status is not None and (galileo_status["data"].chargeStatus == 1 or galileo_status["data"].chargeStatus == 2):
                self.state = Action._states.COMPLETE
                break

        if self.stopped:
            self.state = Action._states.CANCELLED
        else:
            self.state = Action._states.COMPLETE
        
    def pause(self):
        raise ValueError("Charge action cannot be paused")

    def resume(self):
        raise ValueError("Charge action cannot be resumed")
    
    def stop(self):
        # 先发送取消充电任务，等待退出充电状态
        self.stopped = True
        rospy.wait_for_service('/bw_auto_dock/stop_charge')
        stop_charge_req = StopChargeRequest()
        self.stop_charge(stop_charge_req)
        self.state = Action._states.CANCELLED

    def progress(self):
        if self.state != Action._states.COMPLETE:
            return 0
        else:
            return 1
    
    def set_action_data(self, action_data):
        self.x = action_data["x"]
        self.y = action_data["y"]
        self.theta = action_data["theta"]
    
    @staticmethod
    def loads(data):
        data = json.loads(data)
        charge_action = ChargeAction(data["x"], data["y"], data["theta"])
        charge_action.id = data["id"]

    def to_dict(self):
        return {
            "type": "charge_action",
            "id": self.id,
            "x": self.x,
            "y": self.y,
            "theta": self.theta,
            "state": self.state,
            "result": self.result,
            "progress": self.progress(),
        }
    