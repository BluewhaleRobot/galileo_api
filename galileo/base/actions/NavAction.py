#!/usr/bin/env python
# encoding=utf-8

import rospy
import rosservice
import actionlib
import time
import threading
import math
import json
import os
import tf
from tf.transformations import euler_from_quaternion, quaternion_from_euler, quaternion_conjugate
from geometry_msgs.msg import Pose, Point, Quaternion, PoseStamped, Twist
from nav_msgs.msg import Odometry
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from actionlib_msgs.msg import GoalStatus
from base.actions.Action import Action
from base.ros_connector import trans_pose
from base.ros_connector import ROS_CONNECTOR
from base.core import get_status
from galileo.settings import MAP_WORKSPACE_PATH
from scipy.spatial.distance import cdist
from bw_auto_dock.srv import StopCharge, StopChargeRequest

class NavAction(Action):

    move_base = actionlib.SimpleActionClient(
        "/move_base", MoveBaseAction)
    last_speed = None
    nav_cmd_vel_sub = None
    cmd_vel_pub = rospy.Publisher('cmd_vel', Twist, queue_size=0)
    currentAction = None
    current_pose_stamped = None
    current_pose_stamped_map = None
    status_lock = threading.Lock()
    odom_sub = None

    def __init__(self, x, y, theta=0):
        super(NavAction, self).__init__()
        if NavAction.nav_cmd_vel_sub is None:
            NavAction.nav_cmd_vel_sub = rospy.Subscriber(
                "/cmd_vel_nav", Twist, NavAction.send_cmd_vel)
        
        if NavAction.odom_sub is None:
            NavAction.odom_sub = rospy.Subscriber("xqserial_server/Odom", Odometry, NavAction.get_odom)

        self.x = x
        self.y = y
        self.theta = theta
        self.state = self._states.WAITTING
        q_angle = quaternion_from_euler(0, 0, theta, axes='sxyz')
        q = Quaternion(*q_angle)
        self.goal = PoseStamped()
        self.goal.header.frame_id = "map"
        self.goal.header.stamp = rospy.Time(0)
        self.goal.pose = Pose(Point(x, y, 0.0), q)
        self.stopped = False
        self.paused = False
        self.total_distance = -1
        self.current_distance = -1
        self.result = ""
        self.new_nav_points_file = os.path.join(MAP_WORKSPACE_PATH, "new_nav.csv")
        self.index = -1
        # 从nav文件中载入导航点
        if os.path.exists(self.new_nav_points_file):
            with open(self.new_nav_points_file, "r") as new_nav_data_file:
                new_nav_data_str = new_nav_data_file.readline()
                self.target_points = []
                while len(new_nav_data_str) != 0:
                    pos_x = float(new_nav_data_str.split(" ")[0])
                    pos_y = float(new_nav_data_str.split(" ")[1])
                    self.target_points.append([pos_x, pos_y])
                    new_nav_data_str = new_nav_data_file.readline()
            # 找到其中距离目标的最近的点
            index = cdist([[self.x, self.y]], self.target_points).argmin()
            nearest_point = self.target_points[index]
            # 检查是否小于2cm
            if math.sqrt(math.pow(nearest_point[0] - self.x, 2) + math.pow(nearest_point[1] - self.y, 2)) < 0.02:
                self.index = int(index)
        if rosservice.get_service_node("/bw_auto_dock/stop_charge") is not None:
            rospy.wait_for_service('/bw_auto_dock/stop_charge')
            self.stop_charge = rospy.ServiceProxy('/bw_auto_dock/stop_charge', StopCharge)

    @staticmethod
    def send_cmd_vel(msg):
        NavAction.last_speed = msg
        with NavAction.status_lock:
            if NavAction.currentAction is None:
                return
            if NavAction.currentAction.state == NavAction._states.PAUSED:
                return
        NavAction.cmd_vel_pub.publish(msg)

    @staticmethod
    def get_odom(odom):
        with NavAction.status_lock:
            NavAction.current_pose_stamped = PoseStamped()
            NavAction.current_pose_stamped.pose = odom.pose.pose
            NavAction.current_pose_stamped.header = odom.header
            

    def start(self):
        if not NavAction.move_base.wait_for_server(rospy.Duration(1)):
            self.state = NavAction._states.ERROR
            self.result = "wait for move base server timeout"
            return
        # 可能move base 出错
        if self.state == NavAction._states.ERROR:
            return
        self.stopped = False
        self.paused = False
        # 检查是否处于充电状态，如果在充电先停止充电
        if rosservice.get_service_node("/bw_auto_dock/stop_charge") is not None:
            rospy.wait_for_service('/bw_auto_dock/stop_charge')
            stop_charge_req = StopChargeRequest()
            self.stop_charge(stop_charge_req)
        print("################# start nav action: " + self.id)
        with NavAction.status_lock:
            if NavAction.currentAction is not None and not NavAction.currentAction.stopped:
                rospy.logerr(
                    "a nav action is already running and not stopped,"
                    " stop previous action")
                NavAction.currentAction.stop()
            NavAction.currentAction = self
        self.state = NavAction._states.WORKING

        move_base_goal = MoveBaseGoal()
        move_base_goal.target_pose = self.goal

        def done_cb(status, result):
            if status == GoalStatus.SUCCEEDED:
                self.state = Action._states.COMPLETE

        NavAction.move_base.send_goal(move_base_goal, done_cb=done_cb)

        while not self.stopped:
            time.sleep(0.1)
            if self.total_distance < 0:
                self.total_distance = self.current_goal_distance()
            if self.state == Action._states.COMPLETE:
                with NavAction.status_lock:
                    NavAction.currentAction = None
                return
            if self.paused:
                self.state = NavAction._states.PAUSED
            if NavAction.move_base.get_state() == GoalStatus.ABORTED or ( get_status() != "Navigating" and get_status() != "Busy"):
                self.state = NavAction._states.ERROR
                with NavAction.status_lock:
                    NavAction.currentAction = None
                return

        # 任务被停止
        self.state = Action._states.CANCELLED
        with NavAction.status_lock:
            if NavAction.currentAction == self:
                NavAction.move_base.cancel_goal()
                NavAction.currentAction = None
                # 发送速度0
                speed = Twist()
                NavAction.cmd_vel_pub.publish(speed)

    def stop(self):
        print("################## stop nav action: " + self.id)
        self.stopped = True

    def pause(self):
        self.paused = True
        if NavAction.last_speed is not None:
            current_speed = NavAction.last_speed.linear.x
            for i in range(0, 100):
                speed = Twist()
                speed.linear.x = current_speed * (100 - i) / 100.0
                NavAction.cmd_vel_pub.publish(speed)
                time.sleep(0.01)
        NavAction.move_base.cancel_goal()

    def resume(self):
        self.paused = False
        move_base_goal = MoveBaseGoal()
        move_base_goal.target_pose = self.goal

        def done_cb(status, result):
            if status == GoalStatus.SUCCEEDED:
                self.state = Action._states.COMPLETE

        if not NavAction.move_base.wait_for_server(rospy.Duration(1)):
            self.state = Action._states.ERROR
            self.result = "wait for movebase timeout"
            with NavAction.status_lock:
                NavAction.currentAction = None
            return
        self.state = Action._states.WORKING
        NavAction.move_base.send_goal(move_base_goal, done_cb=done_cb)

    def progress(self):
        if self.total_distance < 0:
            return 0
        self.current_distance = self.current_goal_distance()
        current_progress = 1 - self.current_distance / self.total_distance
        if current_progress < 0:
            return 0
        return current_progress

    def pose_distance(self, pose1, pose2):
        return math.sqrt(math.pow((pose1.position.x - pose2.position.x), 2)
                         + math.pow((pose1.position.y - pose2.position.y), 2)
                         + math.pow((pose1.position.z - pose2.position.z), 2)
                         )

    def current_goal_distance(self):
        # get current robot position
        if NavAction.current_pose_stamped == None:
            return -1

        NavAction.current_pose_stamped.header.stamp = rospy.Time(0)
        NavAction.current_pose_stamped.header.frame_id = "odom"
        try:
            NavAction.current_pose_stamped_map = trans_pose(
                "map", self.current_pose_stamped)
        except (tf.LookupException, tf.ConnectivityException, 
                tf.ExtrapolationException, tf.Exception):
            return -1
        return self.pose_distance(self.goal.pose, self.current_pose_stamped_map.pose)

    @staticmethod
    def loads(data):
        data = json.loads(data)
        nav_action = NavAction(data["x"], data["y"], data["theta"])
        nav_action.id = data["id"]
        return nav_action

    @staticmethod
    def stop_all():
        with NavAction.status_lock:
            if NavAction.currentAction is not None and not NavAction.currentAction.stopped:
                NavAction.currentAction.stop()
    @staticmethod
    def pause_all():
        with NavAction.status_lock:
            if NavAction.currentAction is not None and NavAction.currentAction._states.WORKING:
                NavAction.currentAction.pause()
    @staticmethod
    def resume_all():
        with NavAction.status_lock:
            if NavAction.currentAction is not None and NavAction.currentAction._states.PAUSED:
                NavAction.currentAction.resume()
    

    def to_dict(self):
        current_angle = -1
        if NavAction.current_pose_stamped_map is not None:
            current_oritation = NavAction.current_pose_stamped_map.pose.orientation
            current_pose_q = [current_oritation.x, current_oritation.y,
                            current_oritation.z, current_oritation.w]
            current_angle = euler_from_quaternion(current_pose_q)[2]
        return {
            "type": "nav_action",
            "id": self.id,
            "x": self.x,
            "y": self.y,
            "theta": self.theta,
            "state": self.state,
            "result": self.result,
            "index": self.index,
            "progress": self.progress(),
            "current_location": {
                "x": -1 if NavAction.current_pose_stamped_map is None else NavAction.current_pose_stamped_map.pose.position.x,
                "y": -1 if NavAction.current_pose_stamped_map is None else NavAction.current_pose_stamped_map.pose.position.y,
                "theta": current_angle
            },
            "current_distance": self.current_distance
        }
