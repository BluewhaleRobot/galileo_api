#encoding=utf-8

import json
import os
from base.actions.SleepAction import SleepAction
from base.actions.CallbackAction import CallbackAction
from base.actions.UploadMapAction import UploadMapAction
from base.actions.DownloadMapAction import DownloadMapAction
from base.actions.NavAction import NavAction
from base.actions.ChargeAction import ChargeAction
from base.actions.LocalMoveAction import LocalMove
from base.actions.WaitReqAction import WaitReqAction
import threading
from base.models import ACTION_DB


class ActionUtils(object):

    instances = []
    data_lock = threading.RLock()

    def __init__(self):
        super(ActionUtils).__init__()

    @staticmethod
    def new_action(action_data):
        action = None
        if action_data["type"] == "callback_action":
            if not set(["url", "method", "data"]).issubset(action_data):
                return None
            action = CallbackAction(
                action_data["url"], action_data["method"], action_data["data"])
        if action_data["type"] == "sleep_action":
            if "wait_time" not in action_data:
                return None
            action = SleepAction(action_data["wait_time"])
        if action_data["type"] == "upload_map_action":
            if "server_id" not in action_data:
                return None
            action = UploadMapAction(action_data["server_id"])
        if action_data["type"] == "download_map_action":
            if not set(["server_id", "map_id"]).issubset(action_data):
                return None
            action = DownloadMapAction(action_data["server_id"], action_data["map_id"])
        if action_data["type"] == "nav_action":
            if not set(["x", "y", "theta"]).issubset(action_data):
                return None
            action = NavAction(action_data["x"], action_data["y"], action_data["theta"])
        if action_data["type"] == "charge_action":
            if not set(["x", "y", "theta"]).issubset(action_data):
                return None
            action = ChargeAction(action_data["x"], action_data["y"], action_data["theta"])
        if action_data["type"] == "local_move_action":
            if not set(["distance", "angle", "method"]).issubset(action_data):
                return None
            action = LocalMove(action_data["distance"], action_data["angle"], action_data["method"])
        if action_data["type"] == "wait_req_action":
            action = WaitReqAction()
            if "timeout" in action_data:
                action.timeout = action_data["timeout"]
        with ActionUtils.data_lock:
            ActionUtils.instances.append(action)
        return action

    @staticmethod
    def __loads(data):
        action = ActionUtils.new_action(data)
        action.id = data["id"]
        return action

    @staticmethod
    def load_by_id(id):
        target_action = [action for action in ActionUtils.instances if action.id == id]
        if len(target_action) == 0:
            action_record = ACTION_DB.find_one({"id": id})
            if action_record is None:
                return None
            return ActionUtils.__loads(action_record)
        return target_action[0]

    @staticmethod
    def delete(id):
        with ActionUtils.data_lock:
            target_action = ActionUtils.load_by_id(id)
            if target_action is None:
                return
            target_action.stop()
            ActionUtils.instances.remove(target_action)
            ACTION_DB.delete_one({"id": id})


