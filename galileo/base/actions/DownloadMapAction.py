# encoding=utf-8

import os
import json
import requests
from requests_toolbelt import MultipartEncoder, MultipartEncoderMonitor
from base.actions.Action import Action
from galileo.settings import MAP_WORKSPACE_PATH
from base.core import SCHEDULE_MANAGER, SCHEDULE_MANAGER_LOCK

class DownloadMapAction(Action):

    """
    从调度中心下载地图至本地
    """

    def __init__(self, server_id, map_id):
        super(DownloadMapAction, self).__init__()
        self.server_id = server_id
        self.map_id = map_id
        self.state = Action._states.WAITTING
        self.download_size = 0
        self.total_size = 0
        self.result = ""

    def start(self):
        self.state = Action._states.WORKING
        

        # 获取调度中心服务端口
        schedule_manager_port = 0
        schedule_manager_ip = ""
        with SCHEDULE_MANAGER_LOCK:
            schedule_manager = [manager for manager in SCHEDULE_MANAGER if manager["id"] == self.server_id]
            if len(schedule_manager) is None:
                self.state = Action._states.ERROR
                self.result = "target schedule manager not found"
                return
            schedule_manager_port = schedule_manager[0]["port"]
            schedule_manager_ip = schedule_manager[0]["ip"]

        # 从调度中心获取地图
        
        res = requests.get('http://{ip}:{port}/api/v1/layout/map?id={id}'.format(
            ip=schedule_manager_ip, port=schedule_manager_port,id=self.map_id))
        if res.status_code != 200:
            self.state = Action._states.ERROR
            self.result = res.content.decode("utf-8")
            return
        map_info = json.loads(res.content.decode("utf-8"))
        
        download_files = [
            {
                "local": os.path.join(MAP_WORKSPACE_PATH, "map.yaml"),
                "remote": map_info["map_yaml"]
            },
            {
                "local": os.path.join(MAP_WORKSPACE_PATH, "map.pgm"),
                "remote": map_info["image_pgm"]
            },
            {
                "local": os.path.join(MAP_WORKSPACE_PATH, "keyframedb.bson"),
                "remote": map_info["keyframedb_bson"]
            },
            {
                "local": os.path.join(MAP_WORKSPACE_PATH, "keyframes.bson"),
                "remote": map_info["keyframes_bson"]
            },
            {
                "local": os.path.join(MAP_WORKSPACE_PATH, "map.bson"),
                "remote": map_info["map_bson"]
            },
            {
                "local": os.path.join(MAP_WORKSPACE_PATH, "mappoints.bson"),
                "remote": map_info["mappoints_bson"]
            },
            {
                "local": os.path.join(MAP_WORKSPACE_PATH, "RobotTrajectory.txt"),
                "remote": map_info["robot_trajectory"]
            }
        ]
        # 计算总大小
        for download_file in download_files:
            r = requests.get("http://{ip}:{port}{url}".format(
                ip=schedule_manager_ip, port=schedule_manager_port, url=download_file["remote"].encode("utf-8")
            ), stream=True)
            total_length = r.headers.get('content-length')
            if total_length is not None:
                self.total_size += int(total_length)
            r.close()

        # 开始下载
        for download_file in download_files:
            r = requests.get("http://{ip}:{port}{url}".format(
                ip=schedule_manager_ip, port=schedule_manager_port, url=download_file["remote"].encode("utf-8")
            ), stream=True)
            with open(download_file["local"], "wb") as f:
                for data in r.iter_content(chunk_size=4096):
                    self.download_size += len(data)
                    f.write(data)
            if r.status_code != 200:
                self.state = Action._states.ERROR
                self.result = "Download map data failed"
                return
        with open(os.path.join(MAP_WORKSPACE_PATH, "map_name.txt"), "wb") as f:
            f.write(map_info["name"])
        self.state = Action._states.COMPLETE
        self.result = json.dumps(map_info, indent=4)
        
    def pause(self):
        print("Pause is not supported")

    def resume(self):
        print("Resume is not supported")

    def stop(self):
        print("Stop is not supported")

    def progress(self):
        if self.state == Action._states.COMPLETE:
            return 1
        if self.download_size == 0:
            return 0
        return self.download_size * 1.0 / self.total_size

    @staticmethod
    def loads(data):
        data = json.loads(data)
        action = DownloadMapAction(data["server_id"], data["port"])
        action.id = data["id"]
    
    def to_dict(self):
        return {
            "type": "download_map_action",
            "id": self.id,
            "server_id": self.server_id,
            "map_id": self.map_id,
            "state": self.state,
            "result": self.result,
            "progress": self.progress(),
        }

