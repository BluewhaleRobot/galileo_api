#encoding=utf-8

from base.actions.Action import Action
import requests
import json
import time
from base.models import ACTION_DB


class WaitReqAction(Action):

    def __init__(self, timeout=0):
        super(WaitReqAction, self).__init__()
        self.paused = False
        self.stopped = False
        self.state = Action._states.WAITTING
        self.timeout = timeout

    def start(self):
        self.state = Action._states.WORKING
        timecount = 0
        while self.state != Action._states.COMPLETE and self.state != Action._states.CANCELLED:
            time.sleep(0.1)
            timecount += 0.1
            if self.timeout != 0 and timecount > self.timeout:
                self.state = Action._states.COMPLETE
                break
    
    def pause(self):
        self.paused = True

    def resume(self):
        self.paused = False

    def stop(self):
        self.stopped = True
        self.state = Action._states.CANCELLED

    def complete(self):
        self.state = Action._states.COMPLETE

    def progress(self):
        if self.state != Action._states.COMPLETE:
            return 0
        return 1
    
    def set_action_data(self, action_data):
        pass
    
    @staticmethod
    def loads(data):
        data = json.loads(data)
        action = WaitReqAction()
        action.id = data["id"]
        if "timeout" in data:
            action.timeout = data["timeout"]

    def to_dict(self):
        return {
            'type': 'wait_req_action',
            'id': self.id,
            'state': self.state,
            'timeout': self.timeout
        }






    

    
