#!/usr/bin/env python
# encoding=utf-8

import time
import json
from base.actions.Action import Action


class SleepAction(Action):

    """
    等待特定时间的动作
    """

    def __init__(self, wait_time=1):
        super(SleepAction, self).__init__()
        self.wait_time = wait_time
        self.paused = False
        self.stopped = False
        self.state = Action._states.WAITTING
        self.timecount = 0

    def start(self):
        print("Start sleep action")
        self.state = Action._states.WORKING
        self.paused = False
        self.stopped = False
        self.timecount = 0
        
        while not self.stopped:
            time.sleep(0.01)
            self.timecount += 0.01
            if self.timecount >= self.wait_time:
                break
            while self.paused and not self.stopped:
                self.state = Action._states.PAUSED
                time.sleep(1)
        if self.timecount >= self.wait_time:
            self.state = Action._states.COMPLETE
        else:
            self.state = Action._states.CANCELLED

    def pause(self):
        self.paused = True

    def resume(self):
        self.paused = False

    def stop(self):
        self.stopped = True

    def progress(self):
        return self.timecount / self.wait_time

    def set_action_data(self, action_data):
        if "wait_time" in action_data:
            self.wait_time = action_data["wait_time"]

    @staticmethod
    def loads(data):
        action = SleepAction(data['wait_time'])
        action.id = data['id']

    def to_dict(self):
        return {
            'type': 'sleep_action',
            'wait_time': self.wait_time,
            'id': self.id,
            'state': self.state
        }
