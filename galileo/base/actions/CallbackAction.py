#encoding=utf-8

from base.actions.Action import Action
import requests
import json
from base.models import ACTION_DB


class CallbackAction(Action):

    def __init__(self, url, method, data):
        super(CallbackAction, self).__init__()
        self.url = url
        self.method = method
        self.data = data
        self.paused = False
        self.stopped = False
        self.state = Action._states.WAITTING

    def start(self):
        self.state = Action._states.WORKING
        try:
            if self.method.lower() == "get":
                requests.get(self.url, data=self.data)
            if self.method.lower() == "post":
                requests.post(self.url, json=self.data)
            if self.method.lower() == "put":
                requests.post(self.url, json=self.data)
            if self.method.lower() == "delete":
                requests.delete(self.url, data=self.data)
            self.state = Action._states.COMPLETE
        except Exception:
            self.state = Action._states.ERROR
    
    def pause(self):
        self.paused = True

    def resume(self):
        self.paused = False

    def stop(self):
        self.stopped = True

    def progress(self):
        if self.state == Action._states.WAITTING:
            return 0
        return 1
    
    def set_action_data(self, action_data):
        if "url" in action_data:
            self.url = action_data["url"]
        if "method" in action_data:
            self.method = action_data["method"]
        if "data" in action_data:
            self.data = action_data["data"]
    
    @staticmethod
    def loads(data):
        data = json.loads(data)
        action = CallbackAction(data["url"], data["method"], data["data"])
        action.id = data["id"]

    def to_dict(self):
        return {
            'type': 'callback_action',
            'url': self.url,
            'method': self.method,
            'data': self.data,
            'id': self.id,
            'state': self.state
        }






    

    
