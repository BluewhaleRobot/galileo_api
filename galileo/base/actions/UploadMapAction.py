# encoding=utf-8

import json
import uuid
import requests
from requests_toolbelt import MultipartEncoder, MultipartEncoderMonitor
import os
import yaml
from base.actions.Action import Action
from galileo.settings import MAP_WORKSPACE_PATH
from base.core import SCHEDULE_MANAGER, SCHEDULE_MANAGER_LOCK


class UploadMapAction(Action):

    """
    上传地图至调度中心任务
    """

    def __init__(self, server_id):
        super(UploadMapAction, self).__init__()
        self.server_id = server_id
        self.paused = False
        self.stopped = False
        self.state = Action._states.WAITTING
        self.upload_size = 0
        self.total_size = 0
        self.result = ""

    def start(self):
        self.state = Action._states.WORKING
        upload_files = [
            os.path.join(MAP_WORKSPACE_PATH, "map.pgm"),
            os.path.join(MAP_WORKSPACE_PATH, "keyframedb.bson"),
            os.path.join(MAP_WORKSPACE_PATH, "keyframes.bson"),
            os.path.join(MAP_WORKSPACE_PATH, "map.bson"),
            os.path.join(MAP_WORKSPACE_PATH, "mappoints.bson"),
            os.path.join(MAP_WORKSPACE_PATH, "RobotTrajectory.txt"),
        ]
        needed_files = upload_files + [
            os.path.join(MAP_WORKSPACE_PATH, "map_name.txt"),
            os.path.join(MAP_WORKSPACE_PATH, "map.yaml"),
        ]
        map_name_path = os.path.join(MAP_WORKSPACE_PATH, "map_name.txt")
        map_name = ""
        for needed_file in needed_files:
            if not os.path.exists(needed_file):
                self.state = Action._states.ERROR
                self.result = "required file {filename} not found".format(
                    filename=needed_file)
                return

        with open(map_name_path) as map_name_file:
            map_name = map_name_file.read().strip()

        schedule_manager_port = 0
        schedule_manager_ip = ""
        with SCHEDULE_MANAGER_LOCK:
            schedule_manager = [manager for manager in SCHEDULE_MANAGER if manager["id"] == self.server_id]
            if len(schedule_manager) is None:
                self.state = Action._states.ERROR
                self.result = "target schedule manager not found"
                return
            schedule_manager_port = schedule_manager[0]["port"]
            schedule_manager_ip = schedule_manager[0]["ip"]

        # 检查是否存在重名地图, 若存在则自动重命名
        map_res = requests.get("http://{ip}:{port}/api/v1/layout/map".format(
            ip=schedule_manager_ip, port=schedule_manager_port
        ))
        if map_res.status_code != 200:
            self.state = Action._states.ERROR
            self.result = map_res.content.decode("utf-8")
            return
        map_records = json.loads(map_res.content.decode("utf-8"))
        target_map = [map_record for map_record in map_records if map_record["name"] == map_name]
        if len(target_map) != 0:
            # 存在重名地图, 重命名，添加4个随机字符
            map_name += str(uuid.uuid4())[:4]

        content = ""
        with open(os.path.join(MAP_WORKSPACE_PATH, "map.yaml")) as map_info_file:
            content = map_info_file.read()
        map_info = yaml.load(content, Loader=yaml.FullLoader)

        def progress_cb(monitor):
            self.upload_size = monitor.bytes_read

        e = MultipartEncoder({
            "name": map_name,
            "origin_x": str(map_info["origin"][0]),
            "origin_y": str(map_info["origin"][1]),
            "resolution": str(map_info["resolution"]),
            "image_pgm": ("map.pgm", open(upload_files[0], "rb")),
            "keyframedb_bson": ("keyframedb.bson", open(upload_files[1], "rb")),
            "keyframes_bson": ("keyframes.bson", open(upload_files[2], "rb")),
            "map_bson": ("map.bson", open(upload_files[3], "rb")),
            "mappoints_bson": ("mappoints.bson", open(upload_files[4], "rb")),
            "robot_trajectory": ("RobotTrajectory.txt", open(upload_files[5], "rb"))
        })
        m = MultipartEncoderMonitor(e, progress_cb)
        self.total_size = m.len

        r = requests.post('http://{ip}:{port}/api/v1/layout/map'.format(
            ip=schedule_manager_ip, port=schedule_manager_port
        ), data=m, headers={'Content-Type': m.content_type})

        if r.status_code != 200:
            self.state = Action._states.ERROR
            self.result = r.content.decode("utf-8")
        else:
            self.state = Action._states.COMPLETE
            self.result = r.content.decode("utf-8")

    def pause(self):
        print("Pause is not supported")

    def resume(self):
        print("Resume is not supported")

    def stop(self):
        print("Stop is not supported")

    def progress(self):
        if self.state == Action._states.COMPLETE:
            return 1
        if self.upload_size == 0:
            return 0
        return self.upload_size * 1.0 / self.total_size

    @staticmethod
    def loads(data):
        data = json.loads(data)
        action = UploadMapAction(data["server_id"])
        action.id = data["id"]

    def to_dict(self):
        return {
            "type": "upload_map_action",
            "server_id": self.server_id,
            "id": self.id,
            "state": self.state,
            "result": self.result,
            "progress": self.progress()
        }
