#encoding=utf-8

from galileo_msg.msg import LocalMoveGoal, LocalMoveAction
from base.actions.Action import Action
import rospy
import actionlib
import json
from actionlib_msgs.msg import GoalStatus

class LocalMove(Action):

    def __init__(self, distance, angle=0, method=0):
        super(LocalMove, self).__init__()
        self.distance = distance
        self.angle = angle
        self.method = method
        self.state = Action._states.WAITTING
        self.result = ""

        self.local_move = actionlib.SimpleActionClient(
            "local_move", LocalMoveAction)
        rospy.loginfo("Waiting for local move action server...")
        self.local_move.wait_for_server(rospy.Duration(1))
        rospy.loginfo("Connected to local move server")
        self.stopped = False



    def start(self):
        self.state = Action._states.WORKING
        self.local_move.wait_for_server(rospy.Duration(1))
        # self.local_move.cancel_all_goals()
        forward_goal = LocalMoveGoal()
        forward_goal.distance = self.distance
        forward_goal.angle = self.angle
        forward_goal.method = self.method
        self.local_move.send_goal(forward_goal)
        res = self.local_move.wait_for_result()
        if not res:
            # 任务执行失败
            if self.state != Action._states.CANCELLED:
                self.state = Action._states.ERROR
        else:
            self.state = Action._states.COMPLETE


    def pause(self):
        raise ValueError("local move action cannot be paused")

    def resume(self):
        raise ValueError("local move action cannot be resumed")

    def stop(self):
        self.state = Action._states.CANCELLED
        self.local_move.wait_for_server(rospy.Duration(1))
        self.local_move.cancel_all_goals()

    def progress(self):
        if self.state != Action._states.COMPLETE:
            return 0
        else:
            return 1

    def set_action_data(self, action_data):
        self.distance = action_data["distance"]
        self.angle = action_data["angle"]
        self.method = action_data["method"]
    
    @staticmethod
    def loads(data):
        data = json.loads(data)
        local_move_action = LocalMove(data["distance"], data["angle"], data["method"])
        local_move_action.id = data["id"]

    def to_dict(self):
        return {
            "type": "local_move_action",
            "id": self.id,
            "distance": self.distance,
            "angle": self.angle,
            "method": self.method,
            "state": self.state,
            "result": self.result,
            "progress": self.progress(),
        }