#!/usr/bin/env python
#encoding=utf-8


import rospy
import actionlib
from actionlib_msgs.msg import GoalStatus
from galileo_msg.msg import NavigationAction, NavigationActionGoal
from tf.transformations import quaternion_from_euler
from geometry_msgs.msg import Pose, Point, Quaternion

class NavigationTarget:

    def __init__(self):
        self.state = "PAUSED"
        self.move_action = actionlib.SimpleActionClient(
            "/galileo/navigation/target_point", NavigationAction)
        self.pos_x = 0
        self.pos_y = 0
        self.angle = 0
        self.distance = 0


    def send_goal_async(self, pos_x, pos_y, angle,
            done_cb=None, active_cb=None, feedback_cb=None):
        q_angle = quaternion_from_euler(0, 0, angle, axes='sxyz')
        q = Quaternion(*q_angle)
        # send to action server
        target_point = Pose(Point(pos_x, pos_y, 0.0), q)
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.angle = angle
        goal = NavigationActionGoal().goal
        goal.target_pose.header.frame_id = 'map'
        goal.target_pose.header.stamp = rospy.Time.now()
        goal.target_pose.pose = target_point
        self.move_action.send_goal(goal,
            feedback_cb=feedback_cb, done_cb=done_cb, active_cb=active_cb)

    def send_goal(self, pos_x, pos_y, angle):
        q_angle = quaternion_from_euler(0, 0, angle, axes='sxyz')
        q = Quaternion(*q_angle)
        # send to action server
        target_point = Pose(Point(pos_x, pos_y, 0.0), q)
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.angle = angle
        goal = NavigationActionGoal().goal
        goal.target_pose.header.frame_id = 'map'
        goal.target_pose.header.stamp = rospy.Time.now()
        goal.target_pose.pose = target_point
        self.move_action.send_goal(goal,
            feedback_cb=self.set_distance)
        self.move_action.wait_for_result()


    def get_state(self):
        if not self.move_action.gh:
            return GoalStatus.LOST
        return self.move_action.get_state()

    def get_state_str(self):
        state = GoalStatus.LOST
        if self.move_action.gh:
            state = self.move_action.get_state()
        return GoalStatus.to_string(state)

    def cancel_goal(self):
        self.move_action.cancel_goal()

    def set_distance(self, feedback):
        self.distance = feedback.distance

    def get_distance(self):
        return self.distance

    def wait_for_distance(self, goal):
        if self.move_action.get_state() == GoalStatus.ACTIVE:
            self.move_action.cancel_goal()
        def feedback_cb(feedback):
            self.distance = feedback.distance
            self.move_action.cancel_goal()
        self.send_goal_async(goal["x"], goal["y"], goal["angle"],
            feedback_cb=feedback_cb)
        self.move_action.wait_for_result()
        return self.distance
