#!/usr/bin/env python
# encoding=utf-8

"""
负责和ROS进行通信的线程
"""

import threading
import time
import os
import requests
import rospy
import json
from tf.listener import TransformListener
from django.dispatch import receiver
from base.management.commands import SHUTDOWN_SINGAL
from base.tasks.WebsocketServer import WebsocketServer
from galileo_msg.srv import PullRoadStatus, PullRobotStatus, PushRoadStatus,\
    PullRoadStatusResponse, PullRobotStatusResponse, PushRoadStatusResponse
from base.core import SCHEDULE_MANAGER, SCHEDULE_MANAGER_LOCK


class RosConnector(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.stop_flag = threading.Event()
        self.data = {}
        self.data_lock = threading.RLock()
        self.service_lock = threading.RLock()
        self.last_update = {}
        self.freq = {}
        self.freqcount = {}
        self.publisher = {}
        self.tf_trans = None
        self.lock_id = ""

    def run(self):
        rospy.init_node("galileo_system", disable_signals=True)
        rospy.Service("/galileo/service/pull_road_status",
                      PullRoadStatus, self.pull_road_status_handler)
        rospy.Service("/galileo/service/pull_robot_status",
                      PullRobotStatus, self.pull_robot_status_handler)
        rospy.Service("/galileo/service/push_road_status",
                      PushRoadStatus, self.push_road_status_handler)

        self.tf_trans = TransformListener()
        while not self.stopped():
            if rospy.is_shutdown():
                SHUTDOWN_SINGAL.send("system")
                time.sleep(1)
                os._exit(0)
            time.sleep(0.1)
        print("stopping ros node")
        rospy.signal_shutdown("stop node")

    def pull_road_status_handler(self, req):
        robot_id = req.robot_id
        lock_flag = req.lock_flag
        schedule_manager = None
        with SCHEDULE_MANAGER_LOCK:
            if len(SCHEDULE_MANAGER) != 1:
                return PullRoadStatusResponse(10001, "no schedule manager found or multi schedule manager found", "")
            schedule_manager = SCHEDULE_MANAGER[0]
        res = None
        try:
            res = requests.get("http://{ip}:{port}/api/v1/layout".format(
                ip=schedule_manager["ip"],
                port=schedule_manager["port"]
            ), params={
                "robot_id": robot_id,
                "lock": lock_flag
            }, timeout=5)
        except Exception:
            pass
        # rospy.logwarn("##############")
        # rospy.logwarn(json.dumps({
        #     "robot_id": robot_id,
        #     "lock": lock_flag
        # }, indent=4))
        # rospy.logwarn("pull_road_status_handler")
        # rospy.logwarn(res.status_code)
        # # rospy.logwarn(res.content.decode("utf-8"))
        if res is None:
            return PullRoadStatusResponse(10003, "network error", "")
        if res.status_code != 200:
            return PullRoadStatusResponse(10002, json.loads(res.content.decode("utf-8"))["description"], "")
        if lock_flag:
            with self.service_lock:
                self.lock_id = json.loads(res.content.decode("utf-8"))["lock_id"]
        layout_json = json.loads(res.content.decode("utf-8"))["layout"]
        return PullRoadStatusResponse(0, "ok", json.dumps(layout_json, indent=4))

    def pull_robot_status_handler(self, req):
        robot_id = req.robot_id
        schedule_manager = None
        with SCHEDULE_MANAGER_LOCK:
            if len(SCHEDULE_MANAGER) != 1:
                return PullRobotStatusResponse(10001, "not schedule manager found or multi schedule manager found", "")
            schedule_manager = SCHEDULE_MANAGER[0]
        res = None
        if robot_id == "":
            res = None
            try:
                res = requests.get("http://{ip}:{port}/api/v1/robot/status".format(
                    ip=schedule_manager["ip"],
                    port=schedule_manager["port"]
                ), timeout=5)
            except Exception:
                pass
            # rospy.logwarn("#################")
            # rospy.logwarn("pull_robot_status_handler")
            # rospy.logwarn(res.status_code)
            # rospy.logwarn(res.content.decode("utf-8"))
            if res is None:
                return PullRobotStatusResponse(10003, "network error", "")
            if res.status_code != 200:
                return PullRobotStatusResponse(10002, json.loads(res.content.decode("utf-8"))["description"], "")
            robot_json = json.loads(res.content.decode("utf-8"))["robots"]
            # 只返回激活且在线的机器人
            robot_json = [robot_record for robot_record in robot_json if robot_record["is_enabled"] and robot_record["is_online"]]
            return PullRobotStatusResponse(0, "ok", json.dumps(robot_json, indent=4))
        else:
            res = None
            try:
                res = requests.get("http://{ip}:{port}/api/v1/robot/status".format(
                    ip=schedule_manager["ip"],
                    port=schedule_manager["port"]
                ), params={
                    "robot_id": robot_id
                }, timeout=5)
            except Exception:
                pass
            # rospy.logwarn("#################")
            # rospy.logwarn("pull_robot_status_handler")
            # rospy.logwarn(res.status_code)
            # rospy.logwarn(res.content.decode("utf-8"))
            if res is None:
                return PullRobotStatusResponse(10003, "network error", "")
            if res.status_code != 200:
                return PullRobotStatusResponse(10002, json.loads(res.content.decode("utf-8"))["description"], "")
            robot_json = json.loads(res.content.decode("utf-8"))["robot"]
            return PullRobotStatusResponse(0, "ok", json.dumps(robot_json, indent=4))

    def push_road_status_handler(self, req):
        schedule_manager = None
        with SCHEDULE_MANAGER_LOCK:
            if len(SCHEDULE_MANAGER) != 1:
                return PushRoadStatusResponse(10001, "not schedule manager found or multi schedule manager found")
            schedule_manager = SCHEDULE_MANAGER[0]
        with self.service_lock:
            # 在做push期间不允许有改动lock_id的行为
            if self.lock_id == "":
                return PushRoadStatusResponse(10001, "you need to lock layout first")
            robot_id = req.robot_id
            new_layout = json.loads(req.road_layout)
            res = None
            try:
                res = requests.put("http://{ip}:{port}/api/v1/layout".format(
                    ip=schedule_manager["ip"],
                    port=schedule_manager["port"]
                ), json={
                    "robot_id": robot_id,
                    "lock_id": self.lock_id,
                    "layout": new_layout,
                    "unlock": True
                }, timeout=5)
            except Exception:
                pass
            # rospy.logwarn("################")
            # rospy.logwarn("push_road_status_handler")
            # rospy.logwarn(json.dumps({
            #     "robot_id": robot_id,
            #     "lock_id": self.lock_id,
            #     # "layout": new_layout,
            #     "unlock": True
            # }, indent=4))
            # rospy.logwarn(res.status_code)
            # # rospy.logwarn(res.content.decode("utf-8"))
            if res is None:
                return PushRoadStatusResponse(10002, "network error")
            if res.status_code != 200:
                return PushRoadStatusResponse(10002, json.loads(res.content.decode("utf-8"))["description"])
            self.lock_id = ""
        return PushRoadStatusResponse(0, "ok")

    def stop(self):
        self.stop_flag.set()

    def stopped(self):
        return self.stop_flag.is_set()

    def register_topic(self, topic, datatype):
        def call_back(cb_data):
            with self.data_lock:
                self.data[topic] = cb_data
                if topic in self.last_update:
                    if time.time() - self.last_update[topic] < 1:
                        self.freqcount[topic] += 1
                    else:
                        self.last_update[topic] = time.time()
                        self.freq[topic] = self.freqcount[topic]
                        self.freqcount[topic] = 0
                else:
                    self.freq[topic] = 0
                    self.freqcount[topic] = 0
                    self.last_update[topic] = time.time()
        rospy.Subscriber(topic, datatype, call_back)

    def get_topic_data(self, topic, datatype):
        with self.data_lock:
            if topic in self.data \
                    and self.data[topic] is not None:
                return {
                    "data": self.data[topic],
                    "hz": self.freq[topic]
                }
            if topic not in self.data:
                self.register_topic(topic, datatype)
        self.data[topic] = None
        self.freq[topic] = 0
        return None

    def set_topic_data(self, topic, datatype, data):
        with self.data_lock:
            if topic not in self.publisher:
                self.publisher[topic] = \
                    rospy.Publisher(topic,
                                    datatype,
                                    queue_size=0)
                time.sleep(1)
            self.publisher[topic].publish(data)

    def trans_pose(self, target_frame, pose):
        with self.data_lock:
            now = rospy.Time()
            self.tf_trans.waitForTransform(
                target_frame, pose.header.frame_id, now, rospy.Duration(4.0))
            return self.tf_trans.transformPose(target_frame, pose)


ROS_CONNECTOR = RosConnector()
ROS_CONNECTOR.start()

WS_SERVER = WebsocketServer()
WS_SERVER.start()


@receiver(SHUTDOWN_SINGAL)
def stop_node(sender, **kwargs):
    ROS_CONNECTOR.stop()
    WS_SERVER.stop()


def get_topic_data(topic, datatype):
    return ROS_CONNECTOR.get_topic_data(topic, datatype)


def set_topic_data(topic, datatype, data):
    ROS_CONNECTOR.set_topic_data(topic, datatype, data)


def trans_pose(target_frame, pose):
    return ROS_CONNECTOR.trans_pose(target_frame, pose)
