# encoding=utf8
# @Author: xiaoqiang
# @Date:   2017-Jun-20
# @Email:  randoms@bwbot.org
# @Project: galileo
# @Last modified by:   randoms
# @Last modified time: 2017-07-28
# @Copyright: © 2015-2017 Blue Whale Technology all rights reserved


"""
系统核心状态相关程序，和API版本无关
"""

from __future__ import print_function
import threading
import json
import time
import requests
import uuid
from getmac import get_mac_address
from socket import socket, AF_INET, SOCK_DGRAM, SOL_SOCKET, SO_BROADCAST
from django.dispatch import receiver
from base.management.commands import SHUTDOWN_SINGAL
from galileo.settings import BROADCAST_PORT, SERVICE_PORT, VERSION, SCHEDULE_MANAGER_PORT, SCHEDULE_MANAGER_IP, SCHEDULE_MANAGER_SERVICE_PORT
from utils.utils import get_my_id
from galileo_serial_server.msg import GalileoStatus

STATUS_LOCK = threading.RLock()
SYS_STATUS = "Free"

# 服务器信息广播
HOST = '<broadcast>'
BUFSIZE = 1024
ADDR = (HOST, BROADCAST_PORT)

UDP_SOCKET = socket(AF_INET, SOCK_DGRAM)
UDP_SOCKET.bind(('', 0))
UDP_SOCKET.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)


def status_updater(status):
    global SYS_STATUS
    with STATUS_LOCK:
        if status is None:
            SYS_STATUS = "Error"
            return
        # 开启导航过程中
        if status.navStatus == 1 and status.visualStatus < 0:
            SYS_STATUS = "Busy"
            return
        # 开启创建地图过程中
        if status.mapStatus == 1 and status.visualStatus < 0:
            SYS_STATUS = "Busy"
            return
        # 关闭导航过程中, 关闭创建地图过程中
        if status.navStatus == 0 and status.mapStatus == 0 and status.visualStatus > 0:
            SYS_STATUS = "Busy"
            return
        if status.navStatus == 1:
            SYS_STATUS = "Navigating"
            return
        if status.navStatus == 0 and status.visualStatus != -1:
            SYS_STATUS = "Mapping"
            return
        if status.navStatus == 0 and status.mapStatus == 0 and status.visualStatus == -1:
            SYS_STATUS = "Free"
            return

def get_status():
    with STATUS_LOCK:
        return SYS_STATUS


def broadcast_thread():
    broadcast_thread.running = True
    print("UDP server started at " + str(BROADCAST_PORT))
    schedule_udp = None
    if SCHEDULE_MANAGER_IP != "":
        schedule_udp = socket(AF_INET, SOCK_DGRAM)
    while broadcast_thread.running:
        data = json.dumps({
            "id": get_my_id(),
            "port": SERVICE_PORT,
            "mac": get_mac_address(),
            "version": VERSION,
        })
        try:
            UDP_SOCKET.sendto(data.encode("utf-8"), ADDR)
            if schedule_udp is not None:
                # 直接发送udp数据至调度中心
                schedule_udp.sendto(data.encode("utf-8"),
                                    (SCHEDULE_MANAGER_IP, BROADCAST_PORT))
        except Exception:
            print("UDP broadcast network error")
        time.sleep(0.1)
    print("Stopping udp server")
    UDP_SOCKET.close()
    print("udp server stoped")


threading.Thread(target=broadcast_thread, args=()).start()

# 调度中心发现
# 机器人状态上传调度中心
SCHEDULE_MANAGER = []
SCHEDULE_MANAGER_LOCK = threading.RLock()


def find_schedule_manager_thread():
    global SCHEDULE_MANAGER, SCHEDULE_MANAGER_LOCK

    find_schedule_manager_thread.running = True
    print("Schedule reporter started at " + str(SCHEDULE_MANAGER_PORT))
    sock = socket(AF_INET, SOCK_DGRAM)
    server_address = ('0.0.0.0', SCHEDULE_MANAGER_PORT)
    sock.bind(server_address)
    sock.settimeout(2)
    if SCHEDULE_MANAGER_IP != "":
        SCHEDULE_MANAGER.append({
            "ip": SCHEDULE_MANAGER_IP,
            "id": str(uuid.uuid4()),
            "port": SCHEDULE_MANAGER_SERVICE_PORT,
            "version": "1.0.0",
            "update_time": int(time.time() * 1000)
        })
    while find_schedule_manager_thread.running:
        data = None
        try:
            data, address = sock.recvfrom(4096)
            data = json.loads(data.decode("utf-8"))
        except Exception:
            continue
        if not set(["port", "version"]).issubset(data):
            continue
        with SCHEDULE_MANAGER_LOCK:
            if SCHEDULE_MANAGER_IP != "":
                if address[0] != SCHEDULE_MANAGER_IP:
                    continue
                SCHEDULE_MANAGER[0]["ip"] = address[0]
                SCHEDULE_MANAGER[0]["port"] = data["port"]
                SCHEDULE_MANAGER[0]["version"] = data["version"]
                SCHEDULE_MANAGER[0]["id"] = data["id"]
                SCHEDULE_MANAGER[0]["update_time"] = int(time.time() * 1000)
                continue

            record = [
                record for record in SCHEDULE_MANAGER if record["ip"] == address[0]]
            if len(record) == 0:
                SCHEDULE_MANAGER.append({
                    "ip": address[0],
                    "id": data["id"],
                    "port": data["port"],
                    "version": data["version"],
                    "update_time": int(time.time() * 1000)
                })
            else:
                record = record[0]
                record["ip"] = address[0]
                record["port"] = data["port"]
                record["version"] = data["version"]
                record["id"] = data["id"]
                record["update_time"] = int(time.time() * 1000)
            # remove offline manager
            now = int(time.time() * 1000)
            # 超过10s认为下线
            SCHEDULE_MANAGER = [
                record for record in SCHEDULE_MANAGER if now - record["update_time"] < 10 * 1000]
    sock.close()
    print("find schedule manager thread stoped")


threading.Thread(target=find_schedule_manager_thread, args=()).start()


@receiver(SHUTDOWN_SINGAL)
def stop_udp(sender, **kwargs):
    broadcast_thread.running = False
    find_schedule_manager_thread.running = False
