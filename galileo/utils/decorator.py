#!/usr/bin/env python
#encoding=utf-8

import json
from django.http import HttpResponse


def allowed_methods(methods):
    def out_wrap(function):
        def wrap(request, *args, **kwargs):
            if request.method not in methods:
                return HttpResponse(json.dumps({
                    "result": False,
                    "status": "error",
                    "description": "not allowed methods"
                }, indent=4), content_type="json", status=400)
            else:
                return function(request, *args, **kwargs)
        return wrap
    return out_wrap


def required_keys(keys):
    def outer_wrap(function):
        def wrap(request, *args, **kwargs):
            try:
                req_data = json.loads(request.body.decode("utf-8"))
            except Exception:
                req_data = {}
            if request.method == "GET" or request.method == "DELETE":
                req_data = request.GET
            for mkey in keys:
                if mkey not in req_data:
                    return HttpResponse(json.dumps({
                        "result": False,
                        "status": "error",
                        "description": mkey + " is required",
                    }, indent=4), content_type="application/json", status=400)
            return function(request, *args, **kwargs)
        return wrap
    return outer_wrap
