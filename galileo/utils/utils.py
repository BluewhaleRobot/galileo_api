#!/usr/bin/env python
#encoding=utf-8

import re
import os
import time
import signal
import threading
import math
import hashlib
import rospy
from binascii import a2b_base64
from Crypto.Util.asn1 import DerSequence
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA
import numpy as np
import yaml
from galileo.settings import SHARPLINK_LOG_FILE, BASE_DIR, TEST, TIMEOUT


def get_my_id(myid=None):
    if TEST:
        return myid
    log_file = open(SHARPLINK_LOG_FILE)
    contents = log_file.read()
    log_file.close()
    mid_search = re.search(r"[0-9]+,\sID:\s(?P<id>[0-9A-F]{76})", contents)
    mid = mid_search.group("id")
    return mid


def verify(sign, data):
    cert_file = open(os.path.join(BASE_DIR, 'private', 'pubkey.pem'))
    cert = import_cert(cert_file.read())
    cert_file.close()
    verifier = PKCS1_v1_5.new(cert)
    hashed = SHA.new(data.encode("utf-8"))
    return verifier.verify(hashed, "".join(map(chr, sign)))


def import_cert(key_str):
    lines = key_str.replace(" ", '').split()
    der = a2b_base64(''.join(lines[1:-1]))
    cert = DerSequence()
    cert.decode(der)
    tbs_certificate = DerSequence()
    tbs_certificate.decode(cert[0])
    subject_publickey_info = tbs_certificate[6]
    # Initialize RSA key
    rsa_key = RSA.importKey(subject_publickey_info)
    return rsa_key


def byte_to_hex(data):
    hex_str = ""
    for byte in data:
        byte_hex = hex(ord(byte))[2:]
        if len(byte_hex) < 2:
            byte_hex = '0' + byte_hex
        hex_str += byte_hex
    return hex_str


def stop_process(target_process):
    if target_process.poll() is None:
        target_process.send_signal(signal.SIGINT)
        thread1 = threading.Thread(target=target_process.wait, args=())
        thread1.start()

    timecount = 0
    while timecount < TIMEOUT:
        timecount += 1
        time.sleep(1)
        try:
            os.killpg(target_process.pid, 0)
        except Exception:
            break

    if timecount >= TIMEOUT and target_process.poll() is None:
        os.killpg(target_process.pid, signal.SIGKILL)
        target_process.terminate()
        target_process.wait()

def num2deg(xtile, ytile, zoom):
    n_0 = 2.0 ** zoom
    lon_deg = xtile / n_0 * 360.0 - 180.0
    lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * ytile / n_0)))
    lat_deg = math.degrees(lat_rad)
    return (lat_deg, lon_deg)

def deg2num(lat_deg, lon_deg, zoom):
    lat_rad = math.radians(lat_deg)
    n = 2.0 ** zoom
    xtile = int((lon_deg + 180.0) / 360.0 * n)
    ytile = int((1.0 - math.log(math.tan(lat_rad)
        + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n)
    return (xtile, ytile)


def position_to_degree(pos_x, pos_y, xtile, ytile,
    map_width, map_height, map_origin, map_scale):
    p_x = (pos_x - map_origin["x"]) / map_scale
    p_y = (pos_y - map_origin["y"]) / map_scale
    d_x = p_x / (2 * max(map_width, map_height))
    d_y = p_y / (2 * max(map_width, map_height))
    t_x = d_x + xtile
    t_y = d_y + ytile
    lat, lon = num2deg(t_x, t_y, 14)
    return {"lat": lat, "lon": lon}

def degree_to_position(lon, lat, xtile, ytile,
    map_width, map_height, map_origin, map_scale):
    lat_rad = math.radians(lat)
    n = 2.0 ** 14
    t_x = (lon + 180.0) / 360.0 * n
    t_y = (1.0 - math.log(math.tan(lat_rad)
        + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n
    if int(t_x) != xtile:
        t_x = xtile
    if int(t_y) != ytile:
        t_y = ytile
    p_x = (t_x - int(t_x)) * 2 * max(map_width, map_height)
    p_y = (t_y - int(t_y)) * 2 * max(map_width, map_height)
    pos_x = p_x * map_scale + map_origin["x"]
    pos_y = p_y * map_scale + map_origin["y"]
    return [pos_x, pos_y]



class Enum(set):
    def __getattr__(self, name):
        if name in self:
            return name
        raise AttributeError

def opencv_matrix_constructor(loader, node):
        mapping = loader.construct_mapping(node, deep=True)
        mat = np.array(mapping["data"])
        mat.resize(mapping["rows"], mapping["cols"])
        return mat

yaml.add_constructor(u"tag:yaml.org,2002:opencv-matrix", opencv_matrix_constructor, Loader=yaml.FullLoader)

def opencv_matrix_representer(dumper, mat):
        mapping = {'rows': mat.shape[0], 'cols': mat.shape[1], 'dt': 'f', 'data': mat.reshape(-1).tolist()}
        return dumper.represent_mapping(u"tag:yaml.org,2002:opencv-matrix", mapping)

yaml.add_representer(np.ndarray, opencv_matrix_representer)

def write_yaml(file_path, content):
    with open(file_path, 'w') as f:
        f.write("%YAML:1.0\n")
        res = yaml.dump(content, default_flow_style=False)
        res = res.replace('- ', '  - ')
        f.write(res)
 
def read_yaml(file_path):
    with open(file_path, 'r') as f:
        content = f.read()
        content = content.replace('%YAML:1.0\n', '')
        return yaml.load(content, Loader=yaml.FullLoader)

def md5sum(filename, blocksize=65536):
    hash = hashlib.md5()
    with open(filename, "rb") as f:
        for block in iter(lambda: f.read(blocksize), b""):
            hash.update(block)
    return hash.hexdigest()


def get_config_from_param_server():
    config = {
        "default_map": None,
        "maps": {},
        "navigation_speed": rospy.get_param("/move_base/ADDWAPlannerROS/max_vel_x", None),
        "max_control_speed": None,
        "max_control_angle_speed": None,
        "k2": rospy.get_param("/move_base/ADDWAPlannerROS/k2", None),
        "kp": rospy.get_param("/move_base/ADDWAPlannerROS/kp", None),
        "ki": rospy.get_param("/move_base/ADDWAPlannerROS/ki", None),
        "kd": rospy.get_param("/move_base/ADDWAPlannerROS/kd", None),
        "theta_max": rospy.get_param("/move_base/ADDWAPlannerROS/theta_max", None),
        "plan_width": rospy.get_param("/move_base/ADDWAPlannerROS/plan_width", None),
        "path_change": rospy.get_param("/move_base/ADDWAPlannerROS/path_change", None),
        "forward_width": rospy.get_param("/move_base/ADDWAPlannerROS/forward_width", None),
        "rot_width": rospy.get_param("/move_base/ADDWAPlannerROS/rot_width", None),
        "backtime": rospy.get_param("/move_base/ADDWAPlannerROS/backtime", None),
        "bar_distance_min": rospy.get_param("/move_base/ADDWAPlannerROS/bar_distance_min", None)

    }
    return config
