# Galileo 导航系统
伽利略导航系统服务器程序
## 功能简介
实现伽利略系统API。对系统的各种资源进行管理。
## 使用方法
按照一般的ROS包的使用方式，首先使用 ```catkin_make``` 编译软件包
然后
```
roslaunch galileo start.launch

## 文件结构
整体采用Django开发。不同版本的API分入不同的app里。对于和API无关的核心功能代码放入base里面。

## 使用docker进行虚拟测试

### 创建虚拟网络

docker network create -d macvlan --subnet=192.168.0.0/24 --gateway=192.168.0.1 -o parent=enp2s0 xiaoqiang_fake_net

### 启动docker

docker run -it --network=xiaoqiang_fake_net xiaoqiang_fake:1.0 roslaunch startup fake.launch

### 更新说明

#### 4.5.0

增加路径和导航点相关api
增加对galileo_proxy的支持

#### 4.4.1

修复循环错误问题

修复绘制路径时无法开启导航的问题，增加只载入地图定位的功能

#### 4.4.0

更改网络链接方式，默认采用http连接

增加创建地图时地图实时预览

#### 4.3.3

修改电量计算算法

Nav action增加当前距目标距离数据

#### 4.3.2

增加机器人使用情况统计

#### 4.3.1

增加更多的机器人参数设置

#### 4.3.0

添加机器人设置API，可以通过HTTP API设置机器人的导航速度，避障行为，默认地图等行为。

#### 4.2.0

增加 Task loop_count 用于记录循环次数

#### 4.1.0

增加WaitReqAction,等待用户请求动作

修改停止task api在没有id参数时的行为。当没有id参数时停止所有task

增加动态创建 task 的支持，创建task时如果sub_task内部的action和task没有设置id或者id为空字符，则自动创建对应的子任务和action