#!/bin/bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
launch_file="$DIR/../launch/start.launch"
launch_file_template="$DIR/../launch/start-template.launch"
if [ ! -e $launch_file ]; then
    echo "copying launch file"
    cp $launch_file_template $launch_file
fi


